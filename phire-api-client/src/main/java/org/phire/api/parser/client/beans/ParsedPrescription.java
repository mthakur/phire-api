package org.phire.api.parser.client.beans;

import org.phire.api.parser.client.dto.ParsedDrugInfo;

public class ParsedPrescription {

    String prescription;
    String jsonResponse;
    ParsedDrugInfo parsedDrugInfo;

    public ParsedDrugInfo getParsedDrugInfo() {
        return parsedDrugInfo;
    }

    public void setParsedDrugInfo(ParsedDrugInfo parsedDrugInfo) {
        this.parsedDrugInfo = parsedDrugInfo;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getJsonResponse() {
        return jsonResponse;
    }

    public void setJsonResponse(String jsonResponse) {
        this.jsonResponse = jsonResponse;
    }

    @Override
    public String toString() {
        return "ParsedPrescription{" +
                "prescription='" + prescription + '\'' +
                ", jsonResponse='" + jsonResponse + '\'' +
                '}';
    }
}
