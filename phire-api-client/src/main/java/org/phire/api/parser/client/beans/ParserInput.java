package org.phire.api.parser.client.beans;

import org.phire.api.parser.client.dto.ParsedDrugInfo;

public class ParserInput {


    String serviceUr;
    String httpMethod;
    String din;
    String drugName;
    String instruction;
    String prescriptions;
    boolean prescribedDrug;
    ParsedDrugInfo parsedDrugInfo;
    String jsonOutput;

    public String getServiceUr() {
        return serviceUr;
    }

    public void setServiceUr(String serviceUr) {
        this.serviceUr = serviceUr;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(String prescriptions) {
        this.prescriptions = prescriptions;
    }

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getJsonOutput() {
        return jsonOutput;
    }

    public void setJsonOutput(String jsonOutput) {
        this.jsonOutput = jsonOutput;
    }

    public ParsedDrugInfo getParsedDrugInfo() {
        return parsedDrugInfo;
    }

    public void setParsedDrugInfo(ParsedDrugInfo parsedDrugInfo) {
        this.parsedDrugInfo = parsedDrugInfo;
    }

    public boolean isPrescribedDrug() {
        return prescribedDrug;
    }

    public void setPrescribedDrug(boolean prescribedDrug) {
        this.prescribedDrug = prescribedDrug;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

}
