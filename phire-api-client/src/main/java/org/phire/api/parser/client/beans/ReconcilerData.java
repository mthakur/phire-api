package org.phire.api.parser.client.beans;

import org.phire.api.parser.client.model.MatchResult;

import java.util.List;

/**
 * Created by hxiao on 24/02/2017.
 */
public class ReconcilerData {
    private String omnimedJson;
    private String dsqJson;
//    private List<MatchResult> resultReconcilation;
    private List<MatchResult> resultReconcilation;
    public String getOmnimedJson() {
        return omnimedJson;
    }

    public void setOmnimedJson(String omnimedJson) {
        this.omnimedJson = omnimedJson;
    }

    public String getDsqJson() {
        return dsqJson;
    }

    public void setDsqJson(String dsqJson) {
        this.dsqJson = dsqJson;
    }

    public List<MatchResult> getResultReconcilation() {
        return resultReconcilation;
    }

    public void setResultReconcilation(List<MatchResult> resultReconcilation) {
        this.resultReconcilation = resultReconcilation;
    }
}
