package org.phire.api.parser.client.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ClientControler {


    private static final String LIST_RX = "parser/rx";


    @RequestMapping(value = "/rx", method = RequestMethod.GET)
    public String welcome(Model model) {
//        model.addAttribute("prescriptionList", prescriptionList);
        return LIST_RX;
    }
}
