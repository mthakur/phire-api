package org.phire.api.parser.client.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.phire.api.parser.client.beans.ParsedPrescription;
import org.phire.api.parser.client.beans.ParserInput;
import org.phire.api.parser.client.beans.ReconcilerData;
import org.phire.api.parser.client.dto.ParsedDrugInfo;
import org.phire.api.parser.client.dto.PrescriptionData;
import org.phire.api.parser.client.model.MatchResult;
import org.phire.api.parser.client.service.PhireParserService;
import org.phire.api.parser.client.service.ReconcilerService;
import org.phire.api.parser.client.service.SentenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.*;


@Controller
public class ParserController {

    @Autowired
    PhireParserService phireParserService;

    @Autowired
    SentenceService sentenceService;

//    @Autowired
//    FileService fileService;

    @Autowired
    ReconcilerService reconcilerService;

    private static int counter = 0;
    private static final String VIEW_INDEX = "index";
    private static final String PARSER_INPUT = "parser/input";
    private static final String VIGILANCE = "parser/vigilance";
    private static final String DSQ = "parser/dsq";
    private static final String RIGHTRX = "parser/rightrx";
    private static final String RECONCILER = "parser/reconciler";
    private static final String TEST_PRESCRIPTION = "parser/testrrx";
    final static GsonBuilder builder = new GsonBuilder();
    final static Gson gson = builder.create();

    @ModelAttribute("parserInput")
    public ParserInput createFormBean() {
        return new ParserInput();
    }

//    @ModelAttribute("reconcilerData")
//    public ReconcilerData createFormData() {
//        ReconcilerData reconcilerData = new ReconcilerData();
//        reconcilerData.setDsqJson(fileService.getFile(FileService.FileType.DSQ));
//        reconcilerData.setOmnimedJson(fileService.getFile(FileService.FileType.Omnimed));
//        return reconcilerData;
//    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String welcome(Model model) {
        System.err.println("-- /welcome --");
        model.addAttribute("message", "Welcome");
        model.addAttribute("counter", ++counter);
        return VIEW_INDEX;
    }

    @RequestMapping(value = "/parse", method = RequestMethod.GET)
    public String parse(Model model) {
        System.err.println("-- /parse --");
        System.out.println("/parse");
        model.addAttribute("parserInput", new ParserInput());
        return PARSER_INPUT;
    }

    @RequestMapping(value = "/parse/{source}", method = RequestMethod.GET)
    public String parseTestData(@PathVariable(value = "source") String source, Model model) {

        String rxSource = "";
        long startTime = System.currentTimeMillis();
        System.err.println("-- /parse/" + source + " --");
        List<PrescriptionData> prescriptionDataList = new ArrayList<>();

        Map<String, List<PrescriptionData>> map = new HashMap<>();

        if ("rrx".equals(source)) {
            rxSource = "RightRx";
            //337999
            map = sentenceService.fetchTestPrescriptons(337999);
            //prescriptionDataList = sentenceService.fetchTestPrescriptons(337999);

        } else if ("chart".equals(source)) {
            rxSource = "Chart Abstractions";
            prescriptionDataList = sentenceService.fetchChartAbstractionsRx(1000);
        } else if ("rrx-free".equals(source)) {
            rxSource = "RighRx free";
            prescriptionDataList = sentenceService.fetchRRxFreeText();
        } else if ("dsq".equals(source)) {
            rxSource = "DSQ";
            prescriptionDataList = sentenceService.readDsqOrder(0, 50);
        } else {
            prescriptionDataList = sentenceService.readDsqOrder(0, 8000);
        }

        if ("rrx".equals(source)) {
            Iterator<String> keyStr = map.keySet().iterator();
            String key = "";

            while (keyStr.hasNext()) {
                key = keyStr.next();
                prescriptionDataList = map.get(key);
                System.err.println("\t" + key + "\t :: " + prescriptionDataList.size());
                List<PrescriptionData> dataList = parsePrescriptions(prescriptionDataList, source);
                long totalTime = System.currentTimeMillis() - startTime;
                model.addAttribute("SuccessRate ", key + " :: " + dataList.size() + " / " + prescriptionDataList.size() + " = " + ((float) (prescriptionDataList.size() - dataList.size()) / prescriptionDataList.size()) * 100 + " %");
                System.err.println("successRate" + key + " :: " + dataList.size() + " / " + prescriptionDataList.size() + " = " + ((float) (prescriptionDataList.size() - dataList.size()) / prescriptionDataList.size()) * 100 + " %");
                model.addAttribute("prescriptionDataList", dataList);
                model.addAttribute("source", rxSource);

                if (prescriptionDataList.size() != 0) {
                    model.addAttribute("averageProcessingTime", (totalTime / prescriptionDataList.size()) + "ms");
                }
            }
        } else {

            System.err.println("Total Record to parse : " + prescriptionDataList.size());
            List<PrescriptionData> dataList = parsePrescriptions(prescriptionDataList, source);
            long totalTime = System.currentTimeMillis() - startTime;
            model.addAttribute("successRate", dataList.size() + " / " + prescriptionDataList.size() + " = " + ((float) (prescriptionDataList.size() - dataList.size()) / prescriptionDataList.size()) * 100 + " %");
            System.err.println("successRate :: " + dataList.size() + " / " + prescriptionDataList.size() + " = " + ((float) (prescriptionDataList.size() - dataList.size()) / prescriptionDataList.size()) * 100 + " %");
            model.addAttribute("prescriptionDataList", dataList);
            model.addAttribute("source", rxSource);
            model.addAttribute("averageProcessingTime", (totalTime / prescriptionDataList.size()) + "ms");
        }
        return TEST_PRESCRIPTION;
    }


    private List<PrescriptionData> parsePrescriptions(List<PrescriptionData> prescriptionDataList, String source) {

        List<PrescriptionData> dataList = new ArrayList<PrescriptionData>();
        int count = 0;
        int prescriptionCount = 0;
        int failedPrescriptionTest = 0;
        String preFormat = null;
        int firstRow = 0;

        for (PrescriptionData prescriptionData : prescriptionDataList) {

            ParsedDrugInfo parsedDrugInfo = phireParserService.parserPrescriptionHttpClient(prescriptionData.getDin(),
                    prescriptionData.getDrugName(), prescriptionData.getInstructions(), source);
            prescriptionData.setParsedDrugInfo(parsedDrugInfo);

            if (!prescriptionData.success()) {
                dataList.add(prescriptionData);
                failedPrescriptionTest++;

                if (failedPrescriptionTest > 100) {
                }
            }
            preFormat = prescriptionData.getDrugInfo().getFormat();
            prescriptionCount++;
            count++;
        }
        return dataList;
    }

    @RequestMapping(value = "/parse", method = RequestMethod.POST)
    public String parsePrescription(ParserInput parserInput, Model model) {

        System.out.println("/parse");
        List<ParsedPrescription> parsedPrescriptions = new ArrayList<ParsedPrescription>();
        List<ParserInput> parserInputs = parserInputlist(parserInput.getPrescriptions());

        try {
            for (ParserInput input : parserInputs) {
                input.setParsedDrugInfo(phireParserService.parserPrescriptionHttpClient(input.getDin(), input.getDrugName(), input.getPrescriptions(), "INPUT"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ParsedPrescription parsedPrescription = new ParsedPrescription();
        parsedPrescription.setPrescription(parserInput.getPrescriptions());
        model.addAttribute("parserInputs", parserInputs);
        model.addAttribute("parsedPrescriptions", parsedPrescriptions);
        return PARSER_INPUT;
    }

//    @RequestMapping(value = "/reconciler", method = RequestMethod.GET)
//    public String reconciler(Model model) {
//        model.addAttribute("reconcilerData", createFormData());
//        return RECONCILER;
//    }

    @RequestMapping(value = "/reconciler", method = RequestMethod.POST)
    public String reconcile(ReconcilerData reconcilerData, Model model) throws IOException {
        String omnimedJson = reconcilerData.getOmnimedJson();
        String dsqJson = reconcilerData.getDsqJson();
        String result = reconcilerService.reconcile(omnimedJson, dsqJson);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());
        List<MatchResult> results = objectMapper.readValue(result, List.class);
        reconcilerData.setResultReconcilation(results);
        model.addAttribute("reconcilerData", reconcilerData);
        return RECONCILER;
    }

    private List<ParserInput> parserInputlist(String prescriptions) {

        List<ParserInput> parserInputs = new ArrayList<ParserInput>();

        String inputPrescriptionInfo;
        String[] dinAndPrescription;
        String[] prescriptionsArray = prescriptions.split(System.getProperty("line.separator"));

        for (int count = 0; count < prescriptionsArray.length; count++) {
            inputPrescriptionInfo = prescriptionsArray[count];

            if (inputPrescriptionInfo != null && !inputPrescriptionInfo.equals("")) {
                dinAndPrescription = inputPrescriptionInfo.split("\\|");
                ParserInput parserInput = new ParserInput();

                if (dinAndPrescription.length == 3) {
                    parserInput.setDin(dinAndPrescription[0]);
                    parserInput.setPrescriptions(dinAndPrescription[1]);
                    parserInput.setPrescribedDrug(new Boolean(dinAndPrescription[2]));

                } else if (dinAndPrescription.length == 2) {
                    parserInput.setDin(dinAndPrescription[0]);
                    parserInput.setPrescriptions(dinAndPrescription[1]);

                } else if (dinAndPrescription.length == 1) {
                    parserInput.setPrescriptions(dinAndPrescription[0]);

                } else {
                    continue;
                }
                parserInputs.add(parserInput);
            }
        }
        return parserInputs;
    }
}
