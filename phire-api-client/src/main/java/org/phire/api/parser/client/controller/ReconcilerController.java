package org.phire.api.parser.client.controller;


import org.phire.api.parser.client.beans.ReconcilerData;
import org.phire.api.parser.client.service.FileService;
import org.phire.api.parser.client.service.ReconcilerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by hxiao on 23/02/2017.
 */

@Controller
public class ReconcilerController {
    private static final String RECONCILER="conciliator/reconcile";

//    @Autowired
//    private FileService fileService;

    @Autowired
    private ReconcilerService reconcilerService;

    @ModelAttribute("reconcilerData")
    public ReconcilerData createFormData(){return new ReconcilerData();}


    @PostMapping("reconcile")
    @ResponseBody
    public String reconcile(@RequestParam( value = "omniMedProfile", required=true) String profile, @RequestParam(value="dsqResponse",required = true) String dsqResponse)
    {

        String result=reconcilerService.reconcile(profile, dsqResponse);
//        return result;
        return "reconcile";
    }


    @RequestMapping(value = "/reconcile", method = RequestMethod.GET)
    public String reconcile( Model model) {
        ReconcilerData reconcilerData=createFormData();
        model.addAttribute("reconcileDate", reconcilerData);
        return RECONCILER;
    }
//
//    @GetMapping(value = "/dsq",produces = "text/html; charset=ISO-8859-1")
////    @javax.ws.rs.Produces(MediaType.TEXT_HTML_VALUE)
//    @ResponseBody
//    public String getDsqJsonFile(){
//        return fileService.getFile(FileService.FileType.DSQ);
//    }
//
//    @GetMapping(value="/omnimed", produces = "text/html; charset=ISO-8859-1" )
//    @ResponseBody
//    public String getOmnimedJsonFile(){
//        return fileService.getFile(FileService.FileType.Omnimed);
//    }


}