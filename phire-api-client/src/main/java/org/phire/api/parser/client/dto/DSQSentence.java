package org.phire.api.parser.client.dto;

import org.phire.api.parser.client.beans.ParsedPrescription;

public class DSQSentence {

    String din;
    String drugName;
    String instructions;
    String jsonParsedRs;
    ParsedPrescription parsedPrescription;

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getJsonParsedRs() {
        return jsonParsedRs;
    }

    public void setJsonParsedRs(String jsonParsedRs) {
        this.jsonParsedRs = jsonParsedRs;
    }

    public ParsedPrescription getParsedPrescription() {
        return parsedPrescription;
    }

    public void setParsedPrescription(ParsedPrescription parsedPrescription) {
        this.parsedPrescription = parsedPrescription;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

}
