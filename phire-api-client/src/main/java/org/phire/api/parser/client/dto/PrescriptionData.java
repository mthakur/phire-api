package org.phire.api.parser.client.dto;

import org.apache.commons.lang3.StringUtils;

public class PrescriptionData {

    private String din;
    private String drugName;
    private String instructions;
    private String source;

    private ParsedDrugInfo drugInfo;
    private ParsedDrugInfo parsedDrugInfo;

    public boolean success() {

        try {

            if (drugInfo == null || parsedDrugInfo == null) {
                //System.err.println("\t" + drugInfo + "|" + parsedDrugInfo);
                return false;
            }

            if (!equalsStrings(drugInfo.getGenericName(), parsedDrugInfo.getGenericName(), "GENERIC_NAME")) {
                //System.err.println("\t" + drugInfo.getGenericName() + "|" + parsedDrugInfo.getGenericName());
                return false;
            }

            if (!equalsStrings(drugInfo.getGenCode(), parsedDrugInfo.getGenCode(), "GENERIC_CODE")) {
                //System.err.println("\t" + drugInfo.getGenCode() + "|" + parsedDrugInfo.getGenCode());
                return false;
            }

            if (!equalsStrings(drugInfo.getFormat(), parsedDrugInfo.getFormat(), "FORMAT")) {
                //System.err.println("\t" + drugInfo.getFormat() + "|" + parsedDrugInfo.getFormat());
                return false;
            }

            if (!isDose(drugInfo.getFormatAtAdmin()) && !equalsStrings(drugInfo.getQuantityPerAdmin(), parsedDrugInfo.getQuantityPerAdmin(), "QTY")) {
                //System.err.println("\t" + drugInfo.getQuantityPerAdmin() + "|" + parsedDrugInfo.getQuantityPerAdmin());
                return false;
            }

            if (!isDose(drugInfo.getFormatAtAdmin()) && !equalsStrings(drugInfo.getFormatAtAdmin(), parsedDrugInfo.getFormatAtAdmin(), "FMT_ADMIN")) {
                //System.err.println("\t" + drugInfo.getFormatAtAdmin() + "|" + parsedDrugInfo.getFormatAtAdmin());
                return false;
            }

            if (!equalsStrings(drugInfo.getStrength(), parsedDrugInfo.getStrength(), "STRENGTH")) {
                //System.err.println("\t" + drugInfo.getStrength() + "|" + parsedDrugInfo.getStrength());
                return false;
            }

            if (!equalsStrings(drugInfo.getDosePerAdmin(), parsedDrugInfo.getDosePerAdmin(), "DOSE")) {
                if (StringUtils.isBlank(parsedDrugInfo.getDosePerAdmin()) && StringUtils.isNotBlank(parsedDrugInfo.getQuantityPerAdmin())
                        && !equalsStrings(drugInfo.getDosePerAdmin(), parsedDrugInfo.getQuantityPerAdmin(), "QTY")) {
                    //System.err.println("\t" + drugInfo.getDosePerAdmin() + "|" + parsedDrugInfo.getDosePerAdmin());
                    return false;
                }
            }

            if (!equalsStrings(drugInfo.getUnitAtAdmin(), parsedDrugInfo.getUnitAtAdmin(), "DOSE_UNIT")) {
                if (StringUtils.isBlank(parsedDrugInfo.getUnitAtAdmin()) && StringUtils.isNotBlank(parsedDrugInfo.getFormatAtAdmin())
                        && !equalsStrings(drugInfo.getUnitAtAdmin(), parsedDrugInfo.getFormatAtAdmin(), "FORMAT_ADMIN")) {
                    //System.err.println("\t" + drugInfo.getUnitAtAdmin() + "|" + parsedDrugInfo.getFormatAtAdmin());
                    return false;
                }
            }

            if (!equalsStrings(drugInfo.getFrequency(), parsedDrugInfo.getFrequency(), "FREQ")) {
                if (!equalsStrings(drugInfo.getFrequency(), parsedDrugInfo.getFreqCode(), "FREQ_CODE")) {
                    //System.err.println("\t" + drugInfo.getFrequency() + "|" + parsedDrugInfo.getFrequency());
                    return false;
                }
            }

            // if (!equalsStrings(drugInfo.getRoute(), parsedDrugInfo.getRoute(), "RUT")) {
            // return false;
            //System.err.println("\t" + drugInfo.getRoute() + "|" + parsedDrugInfo.getRoute());
            //}

            //if (!equalsStrings(drugInfo.getDuration(), parsedDrugInfo.getDuration(), "DRT")) {
            // return false;
            //}

            if (!equalsStrings(drugInfo.getRepeat(), parsedDrugInfo.getRepeat(), "REPEAT")) {
                //System.err.println("\t" + drugInfo.getRepeat() + "|" + parsedDrugInfo.getRepeat());
                return false;
            }

            if (!equalsStrings(drugInfo.getPrn(), parsedDrugInfo.getPrn(), "PRN")) {
                //System.err.println("\t" + drugInfo.getPrn() + "|" + parsedDrugInfo.getPrn());
                return false;
            }

            if (!equalsStrings(drugInfo.getDirective(), parsedDrugInfo.getDirective(), "DIRECTIVE")) {
                //System.err.println("\t" + drugInfo.getDirective() + "|" + parsedDrugInfo.getDirective());
                return false;
            }

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }


    private boolean isDose(String unit) {
        if (drugInfo.getFormatAtAdmin().equalsIgnoreCase("mg")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("ml")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("gram")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("ui")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("mcg")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("ui")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("g")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("%")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("meq")
                || drugInfo.getFormatAtAdmin().equalsIgnoreCase("mmol")
                ) {
            return true;
        }
        return false;
    }

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public ParsedDrugInfo getParsedDrugInfo() {
        return parsedDrugInfo;
    }

    public ParsedDrugInfo getDrugInfo() {
        return drugInfo;
    }

    public void setDrugInfo(ParsedDrugInfo drugInfo) {
        this.drugInfo = drugInfo;
    }

    public void setParsedDrugInfo(ParsedDrugInfo parsedDrugInfo) {
        this.parsedDrugInfo = parsedDrugInfo;
    }

    private boolean equalsStrings(String strOne, String strTwo, String tag) {
        if (StringUtils.isNotBlank(strOne)) {

            if ("GENERIC_CODE".equalsIgnoreCase(tag)) {
                if (strOne.contains(strTwo) || strTwo.contains(strTwo)) {
                    return true;
                }
            } else if ("RUT".equalsIgnoreCase(tag)) {
                if (strOne.equalsIgnoreCase("vaginal") && strTwo.equalsIgnoreCase("vaginsi")
                        || strOne.equalsIgnoreCase("oral") && strTwo.equalsIgnoreCase("po")
                        || strOne.equalsIgnoreCase("topical") && strTwo.equalsIgnoreCase("skin")
                        ) {
                    return true;
                }
            } else if ("QTY".equalsIgnoreCase(tag)) {
            } else if ("FORMAT".equalsIgnoreCase(tag) && "vaginaltab.".equals(strOne) && "comp.vag.".equals(strTwo)
                    || "FORMAT".equalsIgnoreCase(tag) && "vaginaltab.".equals(strOne) && "comp.vag.".equals(strTwo)) {
                return true;
            } else if ("FREQ_CODE".equalsIgnoreCase(tag) && "daily".equals(strOne) && "die".equals(strTwo)) {
                return true;
            }

            if (StringUtils.isBlank(strTwo) || (strTwo != null && !strOne.trim().replaceAll("\\s+", "").equalsIgnoreCase(strTwo.trim().replaceAll("\\s+", "")))) {
                System.err.println(tag + " : |" + strOne.trim().replaceAll("\\s+", "") + "=" + strTwo.trim().replaceAll("\\s+", "") + "| : " + instructions);
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "PrescriptionData{" +
                "din='" + din + '\'' +
                ", drugName='" + drugName + '\'' +
                ", instructions='" + instructions + '\'' +
                ", source='" + source + '\'' +
                ", drugInfo=" + drugInfo +
                //", parsedDrugInfo=" + parsedDrugInfo +
                "}";
    }
}
