package org.phire.api.parser.client.dto;

import org.apache.commons.lang3.StringUtils;
import org.phire.api.parser.client.beans.ParsedPrescription;

public class RightRxSentence {

    private String rxString;
    private String din;
    private String brandName;
    private String strength;
    private String genericName;
    private String genCode;
    private String ahfsClass;
    private String dose;
    private String doseUnit;
    private String frequency;
    private String route;
    private String directive;
    private String duration;
    private String repeat;
    private String prn;

    private String languageCode;
    private String jsonParsedRs;
    private ParsedPrescription parsedPrescription;

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getRxString() {
        return rxString;
    }

    public void setRxString(String rxString) {
        this.rxString = rxString;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

    public String getAhfsClass() {
        return ahfsClass;
    }

    public void setAhfsClass(String ahfsClass) {
        this.ahfsClass = ahfsClass;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getDirective() {
        return directive;
    }

    public void setDirective(String directive) {
        this.directive = directive;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getJsonParsedRs() {
        return jsonParsedRs;
    }

    public void setJsonParsedRs(String jsonParsedRs) {
        this.jsonParsedRs = jsonParsedRs;
    }

    public ParsedPrescription getParsedPrescription() {
        return parsedPrescription;
    }

    public void setParsedPrescription(ParsedPrescription parsedPrescription) {
        this.parsedPrescription = parsedPrescription;
    }

    public Boolean hasSameParsedInfo() {
        String strength = getParsedPrescription().getParsedDrugInfo().getStrength();
        strength = strength != null ? strength.replaceAll(" ", "") : strength;
        return !((StringUtils.isNoneBlank(this.strength) && !this.strength.equalsIgnoreCase(strength))
                ||
                (StringUtils.isNoneBlank(genericName) && !genericName.equalsIgnoreCase(getParsedPrescription().getParsedDrugInfo().getGenericName()))
                || (StringUtils.isNoneBlank(dose) && !dose.equalsIgnoreCase(getParsedPrescription().getParsedDrugInfo().getDosePerAdmin()))
                || ((StringUtils.isNoneBlank(doseUnit) && StringUtils.isNoneBlank(dose)) && !doseUnit.equalsIgnoreCase(getParsedPrescription().getParsedDrugInfo().getUnitAtAdmin()))
                || (StringUtils.isNoneBlank(frequency) && !frequency.equalsIgnoreCase(getParsedPrescription().getParsedDrugInfo().getFrequency())));
    }
}
