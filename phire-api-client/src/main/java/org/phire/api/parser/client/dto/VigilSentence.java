package org.phire.api.parser.client.dto;

import org.phire.api.parser.client.beans.ParsedPrescription;

public class VigilSentence {

    private String rxString;
    private String genCode;
    private String genericName;
    private String dose;
    private String doseUnit;
    private String frequencyId;
    private String prn;
    private String languageCode;
    private String jsonParsedRs;
    private ParsedPrescription parsedPrescription;

    public String getRxString() {
        return rxString;
    }

    public void setRxString(String rxString) {
        this.rxString = rxString;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    public String getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(String frequencyId) {
        this.frequencyId = frequencyId;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getJsonParsedRs() {
        return jsonParsedRs;
    }

    public void setJsonParsedRs(String jsonParsedRs) {
        this.jsonParsedRs = jsonParsedRs;
    }

    public ParsedPrescription getParsedPrescription() {
        return parsedPrescription;
    }

    public void setParsedPrescription(ParsedPrescription parsedPrescription) {
        this.parsedPrescription = parsedPrescription;
    }

    @Override
    public String toString() {
        return "  genCode='" + genCode + '\'' +
                ", genericName='" + genericName + '\'' +
                ", dose='" + dose + '\'' +
                ", doseUnit='" + doseUnit + '\'' +
                ", frequencyId='" + frequencyId + '\'' +
                ", prn='" + prn + '\'';
    }
}
