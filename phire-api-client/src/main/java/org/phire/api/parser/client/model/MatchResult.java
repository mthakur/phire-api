package org.phire.api.parser.client.model;


import org.phire.api.parser.client.model.dsq.DsqMedication;
import org.phire.api.parser.client.model.omnimed.OmniMedication;

/**
 * Created by hxiao on 17/02/2017.
 */
//@XmlRootElement(name="MatchResult")
public class MatchResult {
    public MatchResult(OmniMedication omnimedMedication, DsqMedication dsqMedication) {
        this.omnimedMedication = omnimedMedication;
        this.dsqMedication = dsqMedication;
    }
    private OmniMedication omnimedMedication;
    private DsqMedication dsqMedication;

    public OmniMedication getOmnimedMedication() {
        return omnimedMedication;
    }

    public void setOmnimedMedication(OmniMedication omnimedMedication) {
        this.omnimedMedication = omnimedMedication;
    }

    public DsqMedication getDsqMedication() {
        return dsqMedication;
    }

    public void setDsqMedication(DsqMedication dsqMedication) {
        this.dsqMedication = dsqMedication;
    }
}
