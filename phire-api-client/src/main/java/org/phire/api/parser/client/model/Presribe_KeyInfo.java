package org.phire.api.parser.client.model;

/**
 * Created by hxiao on 14/02/2017.
 */
public class Presribe_KeyInfo {
    private String licenceNumber;
    private String genCode;
//    private String dinCode;
//    private Calendar prescribe_date;

    public Presribe_KeyInfo(String licenceNumber, String genCode) {
        this.licenceNumber = licenceNumber;
        this.genCode = genCode;
//        this.dinCode=dinCode;
//        this.prescribe_date = prescribe_date;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

//    public Calendar getPrescribe_date() {
//        return prescribe_date;
//    }
//
//    public void setPrescribe_date(Calendar prescribe_date) {
//        this.prescribe_date = prescribe_date;
//    }

//    public String getDinCode() {
//        return dinCode;
//    }
//
//    public void setDinCode(String dinCode) {
//        this.dinCode = dinCode;
//    }

    public String toString(){
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd H:mm:ss z");
//        return licenceNumber+"  : "+genCode + " : "+simpleDateFormat.format(prescribe_date.getTime());
        return licenceNumber+"  : "+genCode  ;
    }

    @Override
    public boolean equals(Object otherObject){
        Presribe_KeyInfo otherPrescription=(Presribe_KeyInfo)otherObject;
        if( this.genCode.compareToIgnoreCase(otherPrescription.getGenCode())==0 && licenceNumber.compareToIgnoreCase(otherPrescription.getLicenceNumber())==0)
            return true;
        else
            return false;
    }
    @Override
    public int hashCode(){
        int hash=17;
        if(licenceNumber!=null)
            hash=hash*31+licenceNumber.hashCode();
        if(genCode!=null)
            hash=hash*31+genCode.hashCode();
        return hash;
    }
}
