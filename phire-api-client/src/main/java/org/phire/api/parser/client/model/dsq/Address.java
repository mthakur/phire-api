package org.phire.api.parser.client.model.dsq;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Address {
    private String city;
    private String postalCode;
    private String street1;
    private Country country;
    private CountrySubdivision countrySubdivision;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet1() {
        return street1;
    }

    public void setStreet1(String street1) {
        this.street1 = street1;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public CountrySubdivision getCountrySubdivision() {
        return countrySubdivision;
    }

    public void setCountrySubdivision(CountrySubdivision countrySubdivision) {
        this.countrySubdivision = countrySubdivision;
    }
}
