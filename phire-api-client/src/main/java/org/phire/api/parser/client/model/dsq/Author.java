package org.phire.api.parser.client.model.dsq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hxiao on 09/02/2017.
 */
public class Author{
    private String firstName;
    private String lastName;
    private String type;
    private List<String> healthcareWorkerProfileList;
    private List<String> workingInstitutionList;
    private String licenceNumber;
    private Map<String, String> profession=new HashMap<String, String>() ;
    private List<String> specializationList=new ArrayList<String>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getHealthcareWorkerProfileList() {
        return healthcareWorkerProfileList;
    }

    public void setHealthcareWorkerProfileList(List<String> healthcareWorkerProfileList) {
        this.healthcareWorkerProfileList = healthcareWorkerProfileList;
    }

    public List<String> getWorkingInstitutionList() {
        return workingInstitutionList;
    }

    public void setWorkingInstitutionList(List<String> workingInstitutionList) {
        this.workingInstitutionList = workingInstitutionList;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public Map<String, String> getProfession() {
        return profession;
    }

    public void setProfession(Map<String, String> profession) {
        this.profession = profession;
    }

    public List<String> getSpecializationList() {
        return specializationList;
    }

    public void setSpecializationList(List<String> specializationList) {
        this.specializationList = specializationList;
    }
}
