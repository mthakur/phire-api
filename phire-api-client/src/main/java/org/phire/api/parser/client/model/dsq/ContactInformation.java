package org.phire.api.parser.client.model.dsq;

import java.util.List;
import java.util.Map;

/**
 * Created by hxiao on 09/02/2017.
 */

public class ContactInformation{
    private List<Address> addressList;
    private List<String> emailAddressList;
    private List<Map<String, String>> phoneNumberList;

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    public List<String> getEmailAddressList() {
        return emailAddressList;
    }

    public void setEmailAddressList(List<String> emailAddressList) {
        this.emailAddressList = emailAddressList;
    }

    public List<Map<String, String>> getPhoneNumberList() {
        return phoneNumberList;
    }

    public void setPhoneNumberList(List<Map<String, String>> phoneNumberList) {
        this.phoneNumberList = phoneNumberList;
    }
}
