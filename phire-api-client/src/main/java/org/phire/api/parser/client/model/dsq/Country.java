package org.phire.api.parser.client.model.dsq;

/**
 * Created by hxiao on 14/02/2017.
 */
public class Country {
    private String englishName;
    private String frenchName;

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getFrenchName() {
        return frenchName;
    }

    public void setFrenchName(String frenchName) {
        this.frenchName = frenchName;
    }
}
