package org.phire.api.parser.client.model.dsq;


import org.joda.time.LocalDate;

/**
 * Created by hxiao on 09/02/2017.
 */

public class DeliveryStatus{
    private int completedDeliveries;
    private LocalDate firstDeliveryDate;
    private Quantity firstDeliveryQuantity;
    private LocalDate lastDeliveryDate;
    private Quantity lastDeliveryQuantity;
    private int remainingDeliveries;
    private Quantity remainingQuantity;
    private Quantity suppliedQuantity;

    public int getCompletedDeliveries() {
        return completedDeliveries;
    }

    public void setCompletedDeliveries(int completedDeliveries) {
        this.completedDeliveries = completedDeliveries;
    }

    public LocalDate getFirstDeliveryDate() {
        return firstDeliveryDate;
    }

    public void setFirstDeliveryDate(LocalDate firstDeliveryDate) {
        this.firstDeliveryDate = firstDeliveryDate;
    }

    public LocalDate getLastDeliveryDate() {
        return lastDeliveryDate;
    }

    public void setLastDeliveryDate(LocalDate lastDeliveryDate) {
        this.lastDeliveryDate = lastDeliveryDate;
    }

    public int getRemainingDeliveries() {
        return remainingDeliveries;
    }

    public void setRemainingDeliveries(int remainingDeliveries) {
        this.remainingDeliveries = remainingDeliveries;
    }

    public Quantity getFirstDeliveryQuantity() {
        return firstDeliveryQuantity;
    }

    public void setFirstDeliveryQuantity(Quantity firstDeliveryQuantity) {
        this.firstDeliveryQuantity = firstDeliveryQuantity;
    }

    public Quantity getLastDeliveryQuantity() {
        return lastDeliveryQuantity;
    }

    public void setLastDeliveryQuantity(Quantity lastDeliveryQuantity) {
        this.lastDeliveryQuantity = lastDeliveryQuantity;
    }

    public Quantity getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(Quantity remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public Quantity getSuppliedQuantity() {
        return suppliedQuantity;
    }

    public void setSuppliedQuantity(Quantity suppliedQuantity) {
        this.suppliedQuantity = suppliedQuantity;
    }
}
