package org.phire.api.parser.client.model.dsq;

/**
 * Created by hxiao on 09/02/2017.
 */
public class DispensingDetail{
    private Pharmacy assignedPharmacy;
        private Quantity quantity;

    public Pharmacy getAssignedPharmacy() {
        return assignedPharmacy;
    }

    public void setAssignedPharmacy(Pharmacy assignedPharmacy) {
        this.assignedPharmacy = assignedPharmacy;
    }

    public Quantity getQuantity() {
        return quantity;
    }

    public void setQuantity(Quantity quantity) {
        this.quantity = quantity;
    }
}