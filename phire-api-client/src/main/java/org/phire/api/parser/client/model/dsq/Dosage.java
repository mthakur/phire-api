package org.phire.api.parser.client.model.dsq;

/**
 * Created by hxiao on 09/02/2017.
 */

public class Dosage{
    private String instructions;

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
