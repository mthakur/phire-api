package org.phire.api.parser.client.model.dsq;

import java.util.UUID;

/**
 * Created by hxiao on 09/02/2017.
 */
public class DsqMedicament {
    private DsqResult dsqResult;
    private UUID transactionUuid;

    public DsqResult getDsqResult() {
        return dsqResult;
    }

    public void setDsqResult(DsqResult dsqResult) {
        this.dsqResult = dsqResult;
    }


    public UUID getTransactionUuid() {
        return transactionUuid;
    }

    public void setTransactionUuid(UUID transactionUuid) {
        this.transactionUuid = transactionUuid;
    }


}
