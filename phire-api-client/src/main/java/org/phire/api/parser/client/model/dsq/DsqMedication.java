package org.phire.api.parser.client.model.dsq;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by hxiao on 09/02/2017.
 */

public class DsqMedication {
    private String id;
    private boolean isAnnotated;
    private boolean isProblematic;
    private String precedence;
    private String status;
    private String treatmentType;
    private Author author;
    private LocalDate createdDate;
    private DeliveryStatus deliveryStatus;
    private DispensingDetail dispensingDetails;
    private List<Dosage> dosageList;
    private Metadata metadata;
    private Product product;
    private List<RefusalList> refusalList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonGetter("isAnnotated")
    public boolean isAnnotated() {
        return isAnnotated;
    }
    @JsonSetter("isAnnotated")
    public void setAnnotated(boolean annotated) {
        isAnnotated = annotated;
    }

    @JsonGetter("isProblematic")
    public boolean isProblematic() {
        return isProblematic;
    }
    @JsonSetter("isProblematic")
    public void setProblematic(boolean problematic) {
        isProblematic = problematic;
    }

    public String getPrecedence() {
        return precedence;
    }

    public void setPrecedence(String precedence) {
        this.precedence = precedence;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTreatmentType() {
        return treatmentType;
    }

    public void setTreatmentType(String treatmentType) {
        this.treatmentType = treatmentType;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public DeliveryStatus getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public DispensingDetail getDispensingDetails() {
        return dispensingDetails;
    }

    public void setDispensingDetails(DispensingDetail dispensingDetails) {
        this.dispensingDetails = dispensingDetails;
    }

    public List<Dosage> getDosageList() {
        return dosageList;
    }

    public void setDosageList(List<Dosage> dosageList) {
        this.dosageList = dosageList;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<RefusalList> getRefusalList() {
        return refusalList;
    }

    public void setRefusalList(List<RefusalList> refusalList) {
        this.refusalList = refusalList;
    }


}
