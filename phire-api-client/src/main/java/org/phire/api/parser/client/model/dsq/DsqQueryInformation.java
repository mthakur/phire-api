package org.phire.api.parser.client.model.dsq;

import java.util.UUID;

/**
 * Created by hxiao on 09/02/2017.
 */

public class DsqQueryInformation{
    private int queryCurrentQuantity;
    private UUID queryId;
    private int queryRemainingQuantity;
    private int queryTotalQuantity;

    public DsqQueryInformation() {
    }

    public int getQueryCurrentQuantity() {
        return queryCurrentQuantity;
    }

    public void setQueryCurrentQuantity(int queryCurrentQuantity) {
        this.queryCurrentQuantity = queryCurrentQuantity;
    }

    public UUID getQueryId() {
        return queryId;
    }

    public void setQueryId(UUID queryId) {
        this.queryId = queryId;
    }

    public int getQueryRemainingQuantity() {
        return queryRemainingQuantity;
    }

    public void setQueryRemainingQuantity(int queryRemainingQuantity) {
        this.queryRemainingQuantity = queryRemainingQuantity;
    }

    public int getQueryTotalQuantity() {
        return queryTotalQuantity;
    }

    public void setQueryTotalQuantity(int queryTotalQuantity) {
        this.queryTotalQuantity = queryTotalQuantity;
    }
}
