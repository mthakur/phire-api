package org.phire.api.parser.client.model.dsq;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hxiao on 09/02/2017.
 */
public class DsqResult{
    private boolean isSuccess;
    private List<String> messageList=new ArrayList<String>();
    private DsqQueryInformation dsqQueryInformation;
    @JsonProperty("medicationList")
    private List<DsqMedication> dsqMedicationList =new ArrayList<DsqMedication>();

    @JsonGetter("isSuccess")
    public boolean isSuccess() {
        return isSuccess;
    }

    @JsonSetter("isSuccess")
    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    public DsqQueryInformation getDsqQueryInformation() {
        return dsqQueryInformation;
    }

    public void setDsqQueryInformation(DsqQueryInformation dsqQueryInformation) {
        this.dsqQueryInformation = dsqQueryInformation;
    }

    public List<DsqMedication> getDsqMedicationList() {
        return dsqMedicationList;
    }

    public void setDsqMedicationList(List<DsqMedication> dsqMedicationList) {
        this.dsqMedicationList = dsqMedicationList;
    }
}