package org.phire.api.parser.client.model.dsq;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hxiao on 09/02/2017.
 */


public class Metadata{
    @JsonProperty("isDevice")
    private boolean isDevice;
    @JsonProperty("isDetailed")
    private boolean isDetailed;
    @JsonProperty("isPrescribed")
    private boolean isPrescribed;
    @JsonProperty("isUnfilled")
    private boolean isUnfilled;

    public boolean isDevice() {
        return isDevice;
    }

    public void setDevice(boolean device) {
        isDevice = device;
    }

    public boolean isDetailed() {
        return isDetailed;
    }

    public void setDetailed(boolean detailed) {
        isDetailed = detailed;
    }

    public boolean isPrescribed() {
        return isPrescribed;
    }

    public void setPrescribed(boolean prescribed) {
        isPrescribed = prescribed;
    }

    public boolean isUnfilled() {
        return isUnfilled;
    }

    public void setUnfilled(boolean unfilled) {
        isUnfilled = unfilled;
    }
}