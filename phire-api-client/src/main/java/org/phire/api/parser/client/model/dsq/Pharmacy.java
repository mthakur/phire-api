package org.phire.api.parser.client.model.dsq;

/**
 * Created by hxiao on 09/02/2017.
 */
public class Pharmacy{
    private ContactInformation contactInformation;
    private String dsqIdentifier;
    private String dsqName;

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    public String getDsqIdentifier() {
        return dsqIdentifier;
    }

    public void setDsqIdentifier(String dsqIdentifier) {
        this.dsqIdentifier = dsqIdentifier;
    }

    public String getDsqName() {
        return dsqName;
    }

    public void setDsqName(String dsqName) {
        this.dsqName = dsqName;
    }
}