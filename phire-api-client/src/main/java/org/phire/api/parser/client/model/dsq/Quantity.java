package org.phire.api.parser.client.model.dsq;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Quantity {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
