package org.phire.api.parser.client.model.dsq;

import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by hxiao on 14/02/2017.
 */
public class RefusalList {
    private Author author;
    private LocalDate createdDate;
    private Pharmacy institution;
    private List<String> reasonList;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public Pharmacy getInstitution() {
        return institution;
    }

    public void setInstitution(Pharmacy institution) {
        this.institution = institution;
    }

    public List<String> getReasonList() {
        return reasonList;
    }

    public void setReasonList(List<String> reasonList) {
        this.reasonList = reasonList;
    }
}
