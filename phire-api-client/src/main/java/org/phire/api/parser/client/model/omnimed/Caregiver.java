package org.phire.api.parser.client.model.omnimed;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Caregiver {
    private DataManipulationDetails dataManipulationDetails;
    private String uuid;
    private int version;
    private String dsqIdentifier;
    private String firstName;
    private String lastName;
    private String licenceNumber;

    public DataManipulationDetails getDataManipulationDetails() {
        return dataManipulationDetails;
    }

    public void setDataManipulationDetails(DataManipulationDetails dataManipulationDetails) {
        this.dataManipulationDetails = dataManipulationDetails;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getDsqIdentifier() {
        return dsqIdentifier;
    }

    public void setDsqIdentifier(String dsqIdentifier) {
        this.dsqIdentifier = dsqIdentifier;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }
}
