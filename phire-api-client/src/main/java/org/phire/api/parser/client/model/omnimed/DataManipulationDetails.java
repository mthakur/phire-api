package org.phire.api.parser.client.model.omnimed;

import org.joda.time.LocalDate;


/**
 * Created by hxiao on 10/02/2017.
 */
public class DataManipulationDetails {
    private LocalDate createdDate;
    private EndPoint createdByEndPoint;
    private EndPoint lastModifiedByEndPoint;
    private User createdByUser;
    private LocalDate lastModifiedDate;
    private User lastModifiedByUser;

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public User getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(User createdByUser) {
        this.createdByUser = createdByUser;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getLastModifiedByUser() {
        return lastModifiedByUser;
    }

    public void setLastModifiedByUser(User lastModifiedByUser) {
        this.lastModifiedByUser = lastModifiedByUser;
    }

    public EndPoint getCreatedByEndPoint() {
        return createdByEndPoint;
    }

    public void setCreatedByEndPoint(EndPoint createdByEndPoint) {
        this.createdByEndPoint = createdByEndPoint;
    }

    public EndPoint getLastModifiedByEndPoint() {
        return lastModifiedByEndPoint;
    }

    public void setLastModifiedByEndPoint(EndPoint lastModifiedByEndPoint) {
        this.lastModifiedByEndPoint = lastModifiedByEndPoint;
    }
}
