package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hxiao on 10/02/2017.
 */
public class DispensingDetail {
    private int dispensingQuantity;
    private DispensingUnit dispensingUnit;
    private int duration;
    private String durationUnit;
    private String instructions;
    @JsonProperty("isDispenseAsWritten")
    private boolean isDispenseAsWritten;
    private int renewals;
    private int treatmentTotalDuration;
    private String treatmentTotalDurationUnit;

    public String getTreatmentTotalDurationUnit() {
        return treatmentTotalDurationUnit;
    }

    public void setTreatmentTotalDurationUnit(String treatmentTotalDurationUnit) {
        this.treatmentTotalDurationUnit = treatmentTotalDurationUnit;
    }

    public int getDispensingQuantity() {
        return dispensingQuantity;
    }

    public void setDispensingQuantity(int dispensingQuantity) {
        this.dispensingQuantity = dispensingQuantity;
    }

    public DispensingUnit getDispensingUnit() {
        return dispensingUnit;
    }

    public void setDispensingUnit(DispensingUnit dispensingUnit) {
        this.dispensingUnit = dispensingUnit;
    }

    public boolean isDispenseAsWritten() {
        return isDispenseAsWritten;
    }

    public void setDispenseAsWritten(boolean dispenseAsWritten) {
        isDispenseAsWritten = dispenseAsWritten;
    }

    public int getRenewals() {
        return renewals;
    }

    public void setRenewals(int renewals) {
        this.renewals = renewals;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public int getTreatmentTotalDuration() {
        return treatmentTotalDuration;
    }

    public void setTreatmentTotalDuration(int treatmentTotalDuration) {
        this.treatmentTotalDuration = treatmentTotalDuration;
    }
}
