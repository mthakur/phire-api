package org.phire.api.parser.client.model.omnimed;

import java.util.Map;

/**
 * Created by hxiao on 10/02/2017.
 */
public class DispensingUnit {
    private String alias;
    private Map<String, String> abbreviation;
    private Map<String, String> name;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Map<String, String> getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(Map<String, String> abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Map<String, String> getName() {
        return name;
    }

    public void setName(Map<String, String> name) {
        this.name = name;
    }
}
