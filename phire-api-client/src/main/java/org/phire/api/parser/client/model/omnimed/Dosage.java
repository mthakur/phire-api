package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Dosage {
    private DataManipulationDetails dataManipulationDetails;
    @JsonProperty("isTakeWhenNeeded")
    private boolean isTakeWhenNeeded;
    private Object textualContent;
    private DosageFrequency dosageFrequency;
    private DosageUnit dosageUnit;
    private int minDose;
    private int maxDose;
    public DataManipulationDetails getDataManipulationDetails() {
        return dataManipulationDetails;
    }

    public void setDataManipulationDetails(DataManipulationDetails dataManipulationDetails) {
        this.dataManipulationDetails = dataManipulationDetails;
    }

    public boolean isTakeWhenNeeded() {
        return isTakeWhenNeeded;
    }

    public void setTakeWhenNeeded(boolean takeWhenNeeded) {
        isTakeWhenNeeded = takeWhenNeeded;
    }

    public Object getTextualContent() {
        return textualContent;
    }

    public void setTextualContent(Object textualContent) {
        this.textualContent = textualContent;
    }

    public DosageFrequency getDosageFrequency() {
        return dosageFrequency;
    }

    public void setDosageFrequency(DosageFrequency dosageFrequency) {
        this.dosageFrequency = dosageFrequency;
    }

    public DosageUnit getDosageUnit() {
        return dosageUnit;
    }

    public void setDosageUnit(DosageUnit dosageUnit) {
        this.dosageUnit = dosageUnit;
    }

    public int getMinDose() {
        return minDose;
    }

    public void setMinDose(int minDose) {
        this.minDose = minDose;
    }

    public int getMaxDose() {
        return maxDose;
    }

    public void setMaxDose(int maxDose) {
        this.maxDose = maxDose;
    }
}
