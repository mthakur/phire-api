package org.phire.api.parser.client.model.omnimed;

import java.util.Map;

/**
 * Created by hxiao on 14/02/2017.
 */
public class DosageFrequency {
    private String alias;
    private String abbreviation;
    private String dosageFrequencyType;
    private String dosageInterval;
    private String dosagePeriod;
    private String Period;
    private int maxFrequency;
    private int minFrequency;
    private Map<String, String> name;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getDosageFrequencyType() {
        return dosageFrequencyType;
    }

    public void setDosageFrequencyType(String dosageFrequencyType) {
        this.dosageFrequencyType = dosageFrequencyType;
    }

    public String getDosageInterval() {
        return dosageInterval;
    }

    public void setDosageInterval(String dosageInterval) {
        this.dosageInterval = dosageInterval;
    }

    public String getPeriod() {
        return Period;
    }

    public void setPeriod(String period) {
        Period = period;
    }

    public int getMaxFrequency() {
        return maxFrequency;
    }

    public void setMaxFrequency(int maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    public int getMinFrequency() {
        return minFrequency;
    }

    public void setMinFrequency(int minFrequency) {
        this.minFrequency = minFrequency;
    }

    public Map<String, String> getName() {
        return name;
    }

    public void setName(Map<String, String> name) {
        this.name = name;
    }

    public String getDosagePeriod() {
        return dosagePeriod;
    }

    public void setDosagePeriod(String dosagePeriod) {
        this.dosagePeriod = dosagePeriod;
    }
}
