package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.UUID;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Drug {
    private String type;
    private UUID uuid;
    private String englishTerm;
    private String frenchTerm;
    private String defaultRoute;
    private String dinCode;
    private String genericCode;
    private Map<String, String> form;
    @JsonProperty("isChronic")
    private boolean isChronic;
    @JsonProperty("isNarcotic")
    private boolean isNarcotic;
    private Map<String, String> manufacturer;
    private ProductType productType;
    private Map<String, String> strength;
    private float unitCost;
    private int usualDuration;
    private int usualQuantity;
    private VigilanceGeneric vigilanceGeneric;
    private String[] exceptionalMedicationCodesList;
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getEnglishTerm() {
        return englishTerm;
    }

    public void setEnglishTerm(String englishTerm) {
        this.englishTerm = englishTerm;
    }

    public String getFrenchTerm() {
        return frenchTerm;
    }

    public void setFrenchTerm(String frenchTerm) {
        this.frenchTerm = frenchTerm;
    }

    public String getDefaultRoute() {
        return defaultRoute;
    }

    public void setDefaultRoute(String defaultRoute) {
        this.defaultRoute = defaultRoute;
    }

    public String getDinCode() {
        return dinCode;
    }

    public void setDinCode(String dinCode) {
        this.dinCode = dinCode;
    }

    public String getGenericCode() {
        return genericCode;
    }

    public void setGenericCode(String genericCode) {
        this.genericCode = genericCode;
    }

    public Map<String, String> getForm() {
        return form;
    }

    public void setForm(Map<String, String> form) {
        this.form = form;
    }

    public boolean isChronic() {
        return isChronic;
    }

    public void setChronic(boolean chronic) {
        isChronic = chronic;
    }

    public boolean isNarcotic() {
        return isNarcotic;
    }

    public void setNarcotic(boolean narcotic) {
        isNarcotic = narcotic;
    }

    public Map<String, String> getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Map<String, String> manufacturer) {
        this.manufacturer = manufacturer;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Map<String, String> getStrength() {
        return strength;
    }

    public void setStrength(Map<String, String> strength) {
        this.strength = strength;
    }

    public int getUsualDuration() {
        return usualDuration;
    }

    public void setUsualDuration(int usualDuration) {
        this.usualDuration = usualDuration;
    }

    public int getUsualQuantity() {
        return usualQuantity;
    }

    public void setUsualQuantity(int usualQuantity) {
        this.usualQuantity = usualQuantity;
    }

    public VigilanceGeneric getVigilanceGeneric() {
        return vigilanceGeneric;
    }

    public void setVigilanceGeneric(VigilanceGeneric vigilanceGeneric) {
        this.vigilanceGeneric = vigilanceGeneric;
    }

    public float getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(float unitCost) {
        this.unitCost = unitCost;
    }

    public String[] getExceptionalMedicationCodesList() {
        return exceptionalMedicationCodesList;
    }

    public void setExceptionalMedicationCodesList(String[] exceptionalMedicationCodesList) {
        this.exceptionalMedicationCodesList = exceptionalMedicationCodesList;
    }
}
