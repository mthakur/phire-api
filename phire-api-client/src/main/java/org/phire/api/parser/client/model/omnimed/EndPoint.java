package org.phire.api.parser.client.model.omnimed;

import java.util.Map;

/**
 * Created by hxiao on 14/02/2017.
 */
public class EndPoint {
    private Map<String, String> name;

    public Map<String, String> getName() {
        return name;
    }

    public void setName(Map<String, String> name) {
        this.name = name;
    }
}
