package org.phire.api.parser.client.model.omnimed;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Institution {
    private String uuid;
    private int version;
    private String dsqIdentifier;
    private String dsqName;
    private String ramqFmgId;
    private String name;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getDsqIdentifier() {
        return dsqIdentifier;
    }

    public void setDsqIdentifier(String dsqIdentifier) {
        this.dsqIdentifier = dsqIdentifier;
    }

    public String getDsqName() {
        return dsqName;
    }

    public void setDsqName(String dsqName) {
        this.dsqName = dsqName;
    }

    public String getRamqFmgId() {
        return ramqFmgId;
    }

    public void setRamqFmgId(String ramqFmgId) {
        this.ramqFmgId = ramqFmgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
