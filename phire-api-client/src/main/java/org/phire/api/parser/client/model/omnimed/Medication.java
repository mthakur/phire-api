package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Medication {
    private LocalDate date;
    private boolean isSuccessful;
    private List<String> messageList;
    @JsonProperty("response")
    private List<OmniMedication> omniMedication;
    private String server;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
    @JsonGetter("isSuccessful")
    public boolean isSuccessful() {
        return isSuccessful;
    }

    @JsonSetter("isSuccessful")
    public void setSuccessful(boolean successful) {
        this.isSuccessful = successful;
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    public List<OmniMedication> getOmniMedication() {
        return omniMedication;
    }

    public void setOmniMedication(List<OmniMedication> omniMedication) {
        this.omniMedication = omniMedication;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }
}
