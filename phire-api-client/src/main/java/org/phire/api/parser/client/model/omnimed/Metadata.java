package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Metadata {
    @JsonProperty("isArchived")
    private boolean isArchived;
    @JsonProperty("isInSummary")
    private boolean isInSummary;

    public boolean isArchived() {
        return isArchived;
    }

    public void setArchived(boolean archived) {
        isArchived = archived;
    }

    public boolean isInSummary() {
        return isInSummary;
    }

    public void setInSummary(boolean inSummary) {
        isInSummary = inSummary;
    }
}
