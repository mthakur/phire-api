package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * Created by hxiao on 10/02/2017.
 */
public class OmniMedication {
    private DataManipulationDetails dataManipulationDetails;
    private UUID uuid;
    private int version;
    private Consultation consultation;
    private Dosage dosage;
    private Drug drug;
    @JsonProperty("isNoneMention")
    private boolean isNoneMention;
    private Metadata metadata;
    private Prescription prescription;

    public DataManipulationDetails getDataManipulationDetails() {
        return dataManipulationDetails;
    }

    public void setDataManipulationDetails(DataManipulationDetails dataManipulationDetails) {
        this.dataManipulationDetails = dataManipulationDetails;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Consultation getConsultation() {
        return consultation;
    }

    public void setConsultation(Consultation consultation) {
        this.consultation = consultation;
    }

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public boolean isNoneMention() {
        return isNoneMention;
    }

    public void setNoneMention(boolean noneMention) {
        isNoneMention = noneMention;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }
}
