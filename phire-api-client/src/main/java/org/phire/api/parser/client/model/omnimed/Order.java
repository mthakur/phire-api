package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.LocalDate;

import java.util.UUID;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Order {
    private UUID uuid;
    private int version;
    @JsonProperty("isExpired")
    private boolean isExpired;
    private String orderStatus;
    private LocalDate completionDate;
    private DataManipulationDetails dataManipulationDetails;

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean expired) {
        isExpired = expired;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public DataManipulationDetails getDataManipulationDetails() {
        return dataManipulationDetails;
    }

    public void setDataManipulationDetails(DataManipulationDetails dataManipulationDetails) {
        this.dataManipulationDetails = dataManipulationDetails;
    }
}
