package org.phire.api.parser.client.model.omnimed;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Patient {
    private String uuid;
    private int version;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String gender;
    private String type;
    private String dsqIdentifier;
    private String medicalCareNumber;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDsqIdentifier() {
        return dsqIdentifier;
    }

    public void setDsqIdentifier(String dsqIdentifier) {
        this.dsqIdentifier = dsqIdentifier;
    }

    public String getMedicalCareNumber() {
        return medicalCareNumber;
    }

    public void setMedicalCareNumber(String medicalCareNumber) {
        this.medicalCareNumber = medicalCareNumber;
    }
}
