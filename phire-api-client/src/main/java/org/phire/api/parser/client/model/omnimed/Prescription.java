package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.LocalDate;

import java.util.UUID;

/**
 * Created by hxiao on 10/02/2017.
 */
public class Prescription {
    private UUID uuid;
    private int version;
    private String type;
    private DataManipulationDetails dataManipulationDetails;
    private DispensingDetail dispensingDetails;
    private LocalDate beginningDate;
    private LocalDate endDate;
    @JsonProperty("isIncomplete")
    private boolean isIncomplete;
    private Order order;
    private String prescriptionStatus;
    private TextualContent textualContent;

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DataManipulationDetails getDataManipulationDetails() {
        return dataManipulationDetails;
    }

    public void setDataManipulationDetails(DataManipulationDetails dataManipulationDetails) {
        this.dataManipulationDetails = dataManipulationDetails;
    }

    public DispensingDetail getDispensingDetails() {
        return dispensingDetails;
    }

    public void setDispensingDetails(DispensingDetail dispensingDetails) {
        this.dispensingDetails = dispensingDetails;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public boolean isIncomplete() {
        return isIncomplete;
    }

    public void setIncomplete(boolean incomplete) {
        isIncomplete = incomplete;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getPrescriptionStatus() {
        return prescriptionStatus;
    }

    public void setPrescriptionStatus(String prescriptionStatus) {
        this.prescriptionStatus = prescriptionStatus;
    }

    public TextualContent getTextualContent() {
        return textualContent;
    }

    public void setTextualContent(TextualContent textualContent) {
        this.textualContent = textualContent;
    }

    public LocalDate getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(LocalDate beginningDate) {
        this.beginningDate = beginningDate;
    }
}
