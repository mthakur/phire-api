package org.phire.api.parser.client.model.omnimed;

import java.util.Map;

/**
 * Created by hxiao on 10/02/2017.
 */
public class ProductType {
    private String type;
    private Map<String, String> name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public Map<String, String> getName() {
        return name;
    }


    public void setName(Map<String, String> values) {
        this.name = values;
    }
}
