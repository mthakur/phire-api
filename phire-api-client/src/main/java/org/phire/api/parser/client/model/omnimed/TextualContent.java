package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by hxiao on 10/02/2017.
 */
public class TextualContent {
    private String type;
    @JsonProperty("value")
    private Object value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public Object getValue() {
        return value;
    }


    public void setValue(Object values) {
        this.value = values;
    }
}
