package org.phire.api.parser.client.model.omnimed;

/**
 * Created by hxiao on 10/02/2017.
 */
public class User {
    private String uuid;
    private String firstName;
    private String lastName;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
