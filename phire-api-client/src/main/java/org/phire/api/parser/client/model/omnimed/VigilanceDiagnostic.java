package org.phire.api.parser.client.model.omnimed;

import java.util.Map;
import java.util.UUID;

/**
 * Created by hxiao on 14/02/2017.
 */
public class VigilanceDiagnostic {
    private String type;
    private UUID uuid;
    private String englishTerm;
    private String frenchTerm;
    private String diagnosticCode;
    private DiagnosticType diagnosticType;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getEnglishTerm() {
        return englishTerm;
    }

    public void setEnglishTerm(String englishTerm) {
        this.englishTerm = englishTerm;
    }

    public String getFrenchTerm() {
        return frenchTerm;
    }

    public void setFrenchTerm(String frenchTerm) {
        this.frenchTerm = frenchTerm;
    }

    public String getDiagnosticCode() {
        return diagnosticCode;
    }

    public void setDiagnosticCode(String diagnosticCode) {
        this.diagnosticCode = diagnosticCode;
    }

    public DiagnosticType getDiagnosticType() {
        return diagnosticType;
    }

    public void setDiagnosticType(DiagnosticType diagnosticType) {
        this.diagnosticType = diagnosticType;
    }

    public class DiagnosticType{
        private String type;
        private Map<String, String> name;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Map<String, String> getName() {
            return name;
        }

        public void setName(Map<String, String> name) {
            this.name = name;
        }
    }

}
