package org.phire.api.parser.client.model.omnimed;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by hxiao on 10/02/2017.
 */
public class VigilanceGeneric {
    private String type;
    private UUID uuid;
    private String englishTerm;
    private String frenchTerm;
    private String genericClass;
    private String genericCode;
    @JsonProperty("isDrug")
    private boolean isDrug;
    private String monographyNumber;
    private Map<String, String> usualName;
    private VigilanceDiagnostic vigilanceDiagnostic;
    private List<VigilanceGeneric> vigilanceGenericCombinationList;

    public List<VigilanceGeneric> getVigilanceGenericCombinationList() {
        return vigilanceGenericCombinationList;
    }

    public void setVigilanceGenericCombinationList(List<VigilanceGeneric> vigilanceGenericCombinationList) {
        this.vigilanceGenericCombinationList = vigilanceGenericCombinationList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getEnglishTerm() {
        return englishTerm;
    }

    public void setEnglishTerm(String englishTerm) {
        this.englishTerm = englishTerm;
    }

    public String getFrenchTerm() {
        return frenchTerm;
    }

    public void setFrenchTerm(String frenchTerm) {
        this.frenchTerm = frenchTerm;
    }

    public String getGenericClass() {
        return genericClass;
    }

    public void setGenericClass(String genericClass) {
        this.genericClass = genericClass;
    }

    public String getGenericCode() {
        return genericCode;
    }

    public void setGenericCode(String genericCode) {
        this.genericCode = genericCode;
    }

    public boolean isDrug() {
        return isDrug;
    }

    public void setDrug(boolean drug) {
        isDrug = drug;
    }

    public String getMonographyNumber() {
        return monographyNumber;
    }

    public void setMonographyNumber(String monographyNumber) {
        this.monographyNumber = monographyNumber;
    }

    public Map<String, String> getUsualName() {
        return usualName;
    }

    public void setUsualName(Map<String, String> usualName) {
        this.usualName = usualName;
    }

    public VigilanceDiagnostic getVigilanceDiagnostic() {
        return vigilanceDiagnostic;
    }

    public void setVigilanceDiagnostic(VigilanceDiagnostic vigilanceDiagnostic) {
        this.vigilanceDiagnostic = vigilanceDiagnostic;
    }
}
