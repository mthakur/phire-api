package org.phire.api.parser.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.*;

/**
 * Created by hxiao on 01/03/2017.
 */
//@Service
public class FileService {
  //  @Value("classpath:DSQ.json")
    private File dsqFile;
    //@Value("classpath:Omnimed.json")
    private File omnimedFile;
    //@Autowired
    ResourceLoader resourceLoader;

    public String getFile(FileType fileType)  {
        StringBuilder sb = new StringBuilder();
        File file;
        switch (fileType){
            case DSQ:
                file=dsqFile;
                break;
            case Omnimed:
                file=omnimedFile;
                break;
            default:
                file=null;
                break;
        }
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "ISO-8859-1"));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }

            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public enum FileType{
        DSQ,
        Omnimed
    }
}
