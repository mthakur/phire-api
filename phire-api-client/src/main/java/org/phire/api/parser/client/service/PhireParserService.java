package org.phire.api.parser.client.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.phire.api.parser.client.dto.ParsedDrugInfo;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service("phireParserService")
public class PhireParserService {


    final static GsonBuilder builder = new GsonBuilder();
    final static Gson gson = builder.create();

    public ParsedDrugInfo parserPrescriptionHttpClient(String din, String drugName, String instructions, String source) {

        ParsedDrugInfo parsedDrugInfo = new ParsedDrugInfo();

        try {

            String url = "http://localhost:8080/phire/api/parse";
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(url);

            // add header
            post.setHeader("User-Agent", "");
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            if (din != null && !din.isEmpty()) {
                urlParameters.add(new BasicNameValuePair("din", din));
            }
            urlParameters.add(new BasicNameValuePair("drugName", drugName));
            urlParameters.add(new BasicNameValuePair("instructions", instructions));
            urlParameters.add(new BasicNameValuePair("source", source));

            post.setEntity(new UrlEncodedFormEntity(urlParameters));
            HttpResponse response = client.execute(post);

            //if (Response.Status.OK.equals(response.getStatusLine().getStatusCode())) {
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            parsedDrugInfo = gson.fromJson(result.toString(), ParsedDrugInfo.class);
            if (parsedDrugInfo != null) {
                parsedDrugInfo.setJsonRs(result.toString());
            }
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parsedDrugInfo;
    }
}

