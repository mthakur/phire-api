package org.phire.api.parser.client.service;

import com.google.gson.GsonBuilder;
import org.phire.api.parser.client.dto.ParsedDrugInfo;
import org.phire.api.parser.client.dto.PrescriptionData;
import org.phire.api.parser.client.dto.RightRxSentence;
import org.phire.api.parser.client.dto.VigilSentence;
import org.phire.api.parser.client.util.FieldType;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("sentenceService")
public class SentenceService {

    final static GsonBuilder builder = new GsonBuilder();
    final static String RRX_FREE_TEXT_FILE_NAME = "rrx-free-text.txt";
    final static String CHART_ABST_PRESCRIPTION = "chart-abstraction.txt";
    final static String VIGIL_PRESCRIPTION_FILE_NAME = "vigil-order-sentance.txt";
    final static String DSQ_PRESCRIPTION_FILE_NAME = "dsq-instructions-complete.txt";
    final static String TEST_PRESCRIPTION_FILE_NAME = "rrx-prescriptions-standards-new.txt";
    final static String TEST_PRESCRIPTION_FILE_NAME_RRX = "rrx.txt";
    final static String RIGHTRX_PRESCRIPTION_FILE_NAME = "rightrx-prescription-complete-real.txt";

    public List<VigilSentence> readVigilanceOrder(int num) {

        System.out.println("-- readVigilanceOrder -- " + num);
        List<VigilSentence> sentences = new ArrayList<VigilSentence>();

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(VIGIL_PRESCRIPTION_FILE_NAME), "UTF8"));
            VigilSentence vigilSentence;
            String strLine;
            int counter = 0;

            while ((strLine = br.readLine()) != null && counter < num + 50) {
                counter++;
                if (counter < num) {
                    continue;
                }
                String[] strLine_split = strLine.split("\t");

                if (strLine_split.length == 8) {
                    vigilSentence = new VigilSentence();
                    vigilSentence.setRxString(strLine_split[0]);
                    vigilSentence.setGenericName(strLine_split[1]);
                    vigilSentence.setGenCode(strLine_split[2]);
                    vigilSentence.setDose(strLine_split[3]);
                    vigilSentence.setDoseUnit(strLine_split[4]);
                    vigilSentence.setFrequencyId(strLine_split[5]);
                    vigilSentence.setPrn(strLine_split[6]);
                    vigilSentence.setLanguageCode(strLine_split[7]);
                    sentences.add(vigilSentence);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sentences;
    }


    public List<PrescriptionData> readDsqOrder(int startIndex, int maxLine) {

        System.out.println("-- readDsqOrder -- " + maxLine);
        List<PrescriptionData> prescriptionDataList = new ArrayList<PrescriptionData>();

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(DSQ_PRESCRIPTION_FILE_NAME), "UTF8"));
            String strLine;
            int counter = 0;

            while ((strLine = br.readLine()) != null) {// && counter < startIndex + maxLine) {
                counter++;

                if (counter < startIndex) {
                    continue;
                }

                PrescriptionData prescriptionData = new PrescriptionData();
                ParsedDrugInfo drugInfo = new ParsedDrugInfo();
                prescriptionData.setDrugInfo(drugInfo);

                String[] dsqPrescriptions = strLine.split("\t");

                for (int count = 0; count < dsqPrescriptions.length; count++) {
                    if (count == FieldType.DIN.getIntegerValue().intValue()) {
                        prescriptionData.setDin(dsqPrescriptions[FieldType.DIN.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.DRUG_NAME.getIntegerValue().intValue()) {
                        prescriptionData.setDrugName(dsqPrescriptions[FieldType.DRUG_NAME.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.INSTRUCTIONS.getIntegerValue().intValue()) {
                        prescriptionData.setInstructions(dsqPrescriptions[FieldType.INSTRUCTIONS.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.DIN_RS.getIntegerValue().intValue()) {
                        drugInfo.setDin(dsqPrescriptions[FieldType.DIN_RS.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.GENERIC_NAME.getIntegerValue().intValue()) {
                        drugInfo.setGenericName(dsqPrescriptions[FieldType.GENERIC_NAME.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.GEN_CODE.getIntegerValue().intValue()) {
                        drugInfo.setGenCode(dsqPrescriptions[FieldType.GEN_CODE.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.STRENGTH.getIntegerValue().intValue()) {
                        drugInfo.setStrength(dsqPrescriptions[FieldType.STRENGTH.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.STRENGTH_UNIT.getIntegerValue().intValue()) {
                        drugInfo.setStrength(dsqPrescriptions[FieldType.STRENGTH_UNIT.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.FORMAT.getIntegerValue().intValue()) {
                        drugInfo.setFormat(dsqPrescriptions[FieldType.FORMAT.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.QTY_PER_ADMIN.getIntegerValue().intValue()) {
                        drugInfo.setQuantityPerAdmin(dsqPrescriptions[FieldType.QTY_PER_ADMIN.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.FORMAT_AT_ADMIN.getIntegerValue().intValue()) {
                        drugInfo.setFormatAtAdmin(dsqPrescriptions[FieldType.FORMAT_AT_ADMIN.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.DOSE_PER_ADMIN.getIntegerValue().intValue()) {
                        drugInfo.setDin(dsqPrescriptions[FieldType.DOSE_PER_ADMIN.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.UNIT_AT_ADMIN.getIntegerValue().intValue()) {
                        drugInfo.setUnitAtAdmin(dsqPrescriptions[FieldType.UNIT_AT_ADMIN.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.FREQUENCY.getIntegerValue().intValue()) {
                        drugInfo.setFrequency(dsqPrescriptions[FieldType.FREQUENCY.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.FREQUENCY_CODE.getIntegerValue().intValue()) {
                        drugInfo.setFreqCode(dsqPrescriptions[FieldType.FREQUENCY_CODE.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.ROUTE.getIntegerValue().intValue()) {
                        drugInfo.setRoute(dsqPrescriptions[FieldType.ROUTE.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.DURATION.getIntegerValue().intValue()) {
                        drugInfo.setDuration(dsqPrescriptions[FieldType.DURATION.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.REPEAT.getIntegerValue().intValue()) {
                        drugInfo.setRepeat(dsqPrescriptions[FieldType.REPEAT.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.PRN.getIntegerValue().intValue()) {
                        drugInfo.setPrn(dsqPrescriptions[FieldType.PRN.getIntegerValue().intValue()].toLowerCase());
                    } else if (count == FieldType.DIRECTIVE.getIntegerValue().intValue()) {
                        drugInfo.setDirective(dsqPrescriptions[FieldType.DIRECTIVE.getIntegerValue().intValue()].toLowerCase());
                    }
                }
                prescriptionDataList.add(prescriptionData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prescriptionDataList;
    }


    public List<RightRxSentence> fetRightRxPrescription(int startIndex, int maxLine) {

        System.out.println("-- fetRightRxPrescription -- " + maxLine);
        List<RightRxSentence> dsqSentanceList = new ArrayList<RightRxSentence>();

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(RIGHTRX_PRESCRIPTION_FILE_NAME), "UTF8"));
            String strLine = "";
            int counter = 0;

            while ((strLine = br.readLine()) != null && counter < startIndex + maxLine) {
                counter++;

                if (counter < startIndex) {
                    continue;
                }
                RightRxSentence parserInput = new RightRxSentence();
                String[] dsqPrescriptions = strLine.split("\t");

                if (dsqPrescriptions.length == 13) {
                    parserInput.setRxString(dsqPrescriptions[0]);
                    parserInput.setBrandName(dsqPrescriptions[1]);
                    parserInput.setStrength(dsqPrescriptions[2]);
                    //parserInput.
                    parserInput.setGenericName(dsqPrescriptions[4]);
                    parserInput.setGenCode(dsqPrescriptions[5]);
                    parserInput.setAhfsClass(dsqPrescriptions[6]);
                    parserInput.setDose(dsqPrescriptions[7]);
                    parserInput.setDoseUnit(dsqPrescriptions[8]);
                    parserInput.setRoute(dsqPrescriptions[9]);
                    parserInput.setFrequency(dsqPrescriptions[10]);
                    parserInput.setDuration(dsqPrescriptions[11]);
                } else {
                    parserInput.setRxString(strLine);
                }
                dsqSentanceList.add(parserInput);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dsqSentanceList;
    }


    public Map<String, List<PrescriptionData>> fetchTestPrescriptons(int recordLimit) {

        List<PrescriptionData> prescriptionDataList = new ArrayList<PrescriptionData>();
        HashMap<String, List<PrescriptionData>> rrxListByFormat = new HashMap();

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(TEST_PRESCRIPTION_FILE_NAME_RRX), "UTF8"));
            int count = 0;

            List<String> formatList = new ArrayList<>();

//            formatList.add("top. liquid");
//            formatList.add("inj.auto");
//            formatList.add("nasal powder");
//--            formatList.add("dr-tablet");
//            formatList.add("8h-tablet");
//            formatList.add("gel");
//            formatList.add("ointment");
            formatList.add("lozenge");
//            formatList.add("enema");
            formatList.add("inj.pen");
//            formatList.add("cream");
//            formatList.add("s/l tablet");
//            formatList.add("oral drops");
//            formatList.add("efferv. tab.");
            formatList.add("inj.syr.la");
            //formatList.add("gum");
//            formatList.add("inj.la");
//--            formatList.add("desint. tab.");
//            formatList.add("inj.implant");
//            formatList.add("syrup");
//            formatList.add("inj.powder");
//            formatList.add("vag. cream");
//            formatList.add("suppository");
//            formatList.add("des. tab. la");
//            formatList.add("inh. nebule");
//            formatList.add("insulin-reg");
//            formatList.add("inj.la.pwd");
//            formatList.add("la-capsule");
//            formatList.add("insulin-la");
//            formatList.add("suspension");
//            formatList.add("insulin-x");
//            formatList.add("patch");
//            formatList.add("nasal spray");
//            formatList.add("72h-patch");
//            formatList.add("chewable tab");
//            formatList.add("24h-patch");
//--            formatList.add("12h-capsule");
//            formatList.add("insulin-n");
//            formatList.add("oral liquid");
//            formatList.add("insulin-rap");
//            formatList.add("oral powder");
//            formatList.add("injectable");
//            formatList.add("enteric cap.");
//            formatList.add("12h-tablet");
//            formatList.add("oral spray");
//--            formatList.add("24h-capsule");
//            formatList.add("caplet");
//            formatList.add("24h-tablet");
//            formatList.add("inj.syringe");
//            formatList.add("powder inh.");
//            formatList.add("metered inh.");
//            formatList.add("la-tablet");
//            formatList.add("enteric tab.");
//            formatList.add("capsule");
//            formatList.add("tablet");

            List<String> unitList = new ArrayList<>();
            //unitList.add("mg");
            //unitList.add("mcg");
            //unitList.add("ml");
            //unitList.add("gram");
            //unitList.add(".");
            //unitList.add("chamber");
            //unitList.add("grams");
            //unitList.add("00");
            //unitList.add("ui");
            //unitList.add("syr");
            //unitList.add("mmol");
            //unitList.add("lancet");
            //unitList.add("meq");

            //HashMap hashMapAdded = new HashMap();
            String strLine = br.readLine();

            while (strLine != null) {

                PrescriptionData prescriptionData = new PrescriptionData();
                String[] dsqPrescriptions = strLine.split("\t");

                //if (dsqPrescriptions.length > 13) {
                //hashMapAdded.put(dsqPrescriptions[4].toLowerCase(), formatList.contains(dsqPrescriptions[4].toLowerCase()));

                if (formatList.contains(dsqPrescriptions[4].toLowerCase())) {

                    prescriptionData.setDin(dsqPrescriptions[1].toLowerCase());
                    prescriptionData.setDrugName("");
                    prescriptionData.setInstructions(dsqPrescriptions[0].toLowerCase());
                    prescriptionData.setSource("");
                    ParsedDrugInfo drugInfo = new ParsedDrugInfo();
                    prescriptionData.setDrugInfo(drugInfo);
                    drugInfo.setDin(dsqPrescriptions[1].toLowerCase());
                    drugInfo.setTrademarkName(dsqPrescriptions[2].toLowerCase());
                    drugInfo.setGenericName(dsqPrescriptions[5].toLowerCase());
                    drugInfo.setGenCode(dsqPrescriptions[6].toLowerCase());
                    drugInfo.setAtcCode("");
                    drugInfo.setStrength(dsqPrescriptions[3].toLowerCase());
                    drugInfo.setFormat(dsqPrescriptions[4].toLowerCase());
                    drugInfo.setQuantityPerAdmin(dsqPrescriptions[8].toLowerCase());
                    drugInfo.setFormatAtAdmin(dsqPrescriptions[9].toLowerCase());
                    drugInfo.setDosePerAdmin("");
                    drugInfo.setUnitAtAdmin("");
                    drugInfo.setFrequency(dsqPrescriptions[11].toLowerCase());
                    drugInfo.setRoute(dsqPrescriptions[10].toLowerCase());
                    drugInfo.setDuration(dsqPrescriptions[12].toLowerCase());
                    prescriptionDataList.add(prescriptionData);

                    if (rrxListByFormat.containsKey(drugInfo.getFormat())) {
                        rrxListByFormat.get(drugInfo.getFormat()).add(prescriptionData);
                    } else {
                        List<PrescriptionData> prescriptionDataList1 = new ArrayList<>();
                        prescriptionDataList1.add(prescriptionData);
                        rrxListByFormat.put(drugInfo.getFormat(), prescriptionDataList1);
                    }
                }
                //}
                strLine = br.readLine();
                count++;
                continue;
            }
            System.err.println("Total Record in txt file : " + count);
        } catch (Exception e) {
            System.err.println("Exception while retrieving prescription data");
            e.printStackTrace();
        }
        return rrxListByFormat;
    }


    public List<PrescriptionData> fetchChartAbstractionsRx(int limitRecords) {

        List<PrescriptionData> prescriptionDataList = new ArrayList<PrescriptionData>();

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(CHART_ABST_PRESCRIPTION), "UTF8"));
            String strLine = br.readLine();
            String previousStrLine = "";
            int count = 0;

            while (strLine != null) {
                if (count > limitRecords) {
                    break;
                }
                PrescriptionData prescriptionData = new PrescriptionData();
                String[] dsqPrescriptions = strLine.split("\t");

                if (dsqPrescriptions.length >= 5 && !previousStrLine.equalsIgnoreCase(strLine)) {
                    prescriptionData.setInstructions(dsqPrescriptions[0].toLowerCase());
                    ParsedDrugInfo drugInfo = new ParsedDrugInfo();
                    prescriptionData.setDrugInfo(drugInfo);
                    drugInfo.setDosePerAdmin(dsqPrescriptions[1].toLowerCase());
                    drugInfo.setUnitAtAdmin(dsqPrescriptions[2].toLowerCase());
                    drugInfo.setRoute(dsqPrescriptions[3].toLowerCase());
                    drugInfo.setFrequency(dsqPrescriptions[4].toLowerCase());
                    if (dsqPrescriptions.length == 6) {
                        drugInfo.setFormat(dsqPrescriptions[5].toLowerCase());
                    }
                    if (dsqPrescriptions.length == 7) {
                        drugInfo.setDirective(dsqPrescriptions[6].toLowerCase());
                    }
                    prescriptionDataList.add(prescriptionData);
                }
                previousStrLine = strLine;
                strLine = br.readLine();
                count++;
            }
        } catch (Exception e) {
            System.err.println("Exception while retrieving prescription data");
            e.printStackTrace();
        }
        return prescriptionDataList;
    }


    public List<PrescriptionData> fetchRRxFreeText() {

        List<PrescriptionData> prescriptionDataList = new ArrayList<PrescriptionData>();

        try {
            int loopBreak = 20000;
            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(RRX_FREE_TEXT_FILE_NAME), "UTF8"));
            String strLine = br.readLine();
            int count = 0;

            while (strLine != null) {
                if (count > loopBreak) {
                    break;
                }
                PrescriptionData prescriptionData = new PrescriptionData();
                String[] dsqPrescriptions = strLine.split("\t");

                if (dsqPrescriptions.length >= 3) {
                    prescriptionData.setDin(dsqPrescriptions[0].toLowerCase());
                    prescriptionData.setDrugName(dsqPrescriptions[1].toLowerCase());
                    prescriptionData.setInstructions(dsqPrescriptions[2].toLowerCase());
                    prescriptionDataList.add(prescriptionData);
                }
                strLine = br.readLine();
                count++;
            }
        } catch (Exception e) {
            System.err.println("Exception while retrieving prescription data");
            e.printStackTrace();
        }
        return prescriptionDataList;
    }

}
