package org.phire.api.parser.client.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Service;

@Service("vigilSentenceService")
public class VigilSentenceService {


    final static GsonBuilder builder = new GsonBuilder();
    final static Gson gson = builder.create();

//    public List<VigilSentence> readVigilanceOrder(int num) {
//        //System.err.println("-- readVigilanceOrder -- " + num);
//        List<VigilSentence> sentences = new ArrayList<VigilSentence>();
//        try {
//
//            FileInputStream fstream = new FileInputStream(this.getClass().getClassLoader().getResource("vigil-order-sentance.txt").getFile());
//            DataInputStream in = new DataInputStream(fstream);
//            BufferedReader br = new BufferedReader(new InputStreamReader(in));
//            VigilSentence vigilSentence;
//            String strLine;
//            int counter = 0;
//
//            while ((strLine = br.readLine()) != null && counter < num + 50) {
//
//                counter++;
//                if (counter < num) {
//                    continue;
//                }
//
//                String[] strLine_split = strLine.split("\t");
//                if (strLine_split.length == 8) {
//                    vigilSentence = new VigilSentence();
//                    //System.err.println("-- " + strLine_split[0]);
//                    vigilSentence.setRxString(strLine_split[0]);
//                    vigilSentence.setGenericName(strLine_split[1]);
//                    vigilSentence.setGenCode(strLine_split[2]);
//                    vigilSentence.setDose(strLine_split[3]);
//                    vigilSentence.setDoseUnit(strLine_split[4]);
//                    vigilSentence.setFrequencyId(strLine_split[5]);
//                    vigilSentence.setPrn(strLine_split[6]);
//                    vigilSentence.setLanguageCode(strLine_split[7]);
//                    sentences.add(vigilSentence);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return sentences;
//    }
}