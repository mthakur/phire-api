package org.phire.api.parser.client.util;

public enum FieldType {

    DIN(0),
    DRUG_NAME(1),
    INSTRUCTIONS(2),

    DIN_RS(3),
    GENERIC_NAME(4),
    GEN_CODE(5),
    STRENGTH(6),
    STRENGTH_UNIT(7),
    FORMAT(8),
    QTY_PER_ADMIN(9),
    FORMAT_AT_ADMIN(10),
    DOSE_PER_ADMIN(11),
    UNIT_AT_ADMIN(12),
    FREQUENCY(13),
    FREQUENCY_CODE(14),
    ROUTE(15),
    DURATION(16),
    REPEAT(17),
    PRN(18),
    DIRECTIVE(19);


    private Integer integerValue;

    FieldType(Integer integerValue) {
        this.integerValue = integerValue;
    }

    public Integer getIntegerValue() {
        return integerValue;
    }

    public static FieldType convert(Integer integerValue) {

        if (DIN.getIntegerValue().equals(integerValue)) {
            return DIN;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else if (DRUG_NAME.getIntegerValue().equals(integerValue)) {
            return DRUG_NAME;
        } else if (INSTRUCTIONS.getIntegerValue().equals(integerValue)) {
            return INSTRUCTIONS;
        } else {
            throw new RuntimeException("Invalid value for converter.");
        }
    }
}
