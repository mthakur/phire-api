package org.phire.api.parser.client.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

    public static void main(String[] args) {

        System.err.println(transformDrugName("polyurethane - film + carboxymethylcellulose - dressings"));
    }

    public static String transformDrugName(String input) {

        Pattern p = Pattern.compile(" \\- ");
        Matcher m = p.matcher(input);

        if (m.find()) {
            input = m.replaceAll("-");
        }
        return input;
    }
}
