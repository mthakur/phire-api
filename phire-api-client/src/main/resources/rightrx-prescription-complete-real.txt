acetaminophen 325-650 mg po QID PRN x30 day(s)
acetaminophen 325-650 mg po QID PRN x30 day(s) refill(s): 3
acetaminophen tab 650 MG po-oral qid prn x30 day(s)
acetaminophen tab 650 MG po-oral qid prn x30 day(s) refill(s): 6
acetaminophen tab 975 MG po-oral q6h prn x30 day(s)
acetaminophen tab 975 MG po-oral q6h prn x30 day(s) no refill
amlodipine (10 MG tablet) 10 mg oral daily x30 day(s)
amlodipine (10 MG tablet) 10 mg oral daily x30 day(s) refill(s): 3
amlodipine (5 MG tablet) mg oral
amlodipine tab 5 MG po daily x30 day(s)
amlodipine tab 5 MG po daily x30 day(s) refill(s): 3
amlodipine tab 5 MG po-oral daily x30 day(s)
amlodipine tab 5 MG po-oral daily x30 day(s) refill(s): 3
amoxicillin cap 500 MG po-oral q12h 500 mg = 1 cap po every 12 hours ad 04nov2016 to complete 7 days course x3 day(s)
amoxicillin cap 500 MG po-oral q12h x3 day(s) no refill
asa (325 MG enteric tab.) mg oral
asa 325 MG enteric tab. oral
asa chw tab 80 MG po daily x30 day(s)
asa chw tab 80 MG po daily x30 day(s) refill(s): 3
asa EC 80 mg po daily x30 day(s)
asa EC 80 mg po daily x30 day(s) refill(s): 3
asa-antiplatelet (80 MG enteric tab.) 80 mg oral daily
asa-antiplatelet (80 MG enteric tab.) 80 mg oral daily prn x30 day(s)
asa-antiplatelet (80 MG enteric tab.) 80 mg oral daily prn x30 day(s) (quantity:10  tablet) no refill
asa-antiplatelet 80 MG enteric tab. 1 tablet oral daily x30 day(s)
asa-antiplatelet 80 MG enteric tab. 1 tablet oral daily x30 day(s) refill(s): 3
asa-antiplatelet EC 325 mg PO die x30 days then 80 mg PO die x30 day(s)
asa-antiplatelet EC 325 mg PO die x30 days then 80 mg PO die x30 day(s) refill(s): 3
atorvastatin (80 MG tablet) 80 mg oral qhs
atorvastatin 80 MG tablet 1 tablet oral qhs x30 day(s)
atorvastatin 80 MG tablet 1 tablet oral qhs x30 day(s) refill(s): 3
atorvastatin tab 20 MG po-oral qhs x30 day(s)
atorvastatin tab 20 MG po-oral qhs x30 day(s) refill(s): 3
atorvastatin tab 40 MG po-oral qhs x30 day(s)
atorvastatin tab 40 MG po-oral qhs x30 day(s) refill(s): 3
bisoprolol (5 MG tablet) 2.5 mg oral daily
bisoprolol 5 MG tablet oral
calcium-carbonate (500 MG tablet) 500 mg oral daily x30 day(s)
calcium-carbonate (500 MG tablet) 500 mg oral daily x30 day(s) refill(s): 3
candesartan (8 MG tablet) 8 mg oral daily
carvedilol (12.5 MG tablet) 12.5 mg oral daily x30 day(s)
carvedilol (12.5 MG tablet) 12.5 mg oral daily x30 day(s) refill(s): 3
celecoxib 200 MG capsule oral
chlorpheniramine+pseudoephedrine+ibuprofen 2+30+200 MG tablet 1 tablet oral daily before meal(s) prn x30 day(s)
chlorpheniramine+pseudoephedrine+ibuprofen 2+30+200 MG tablet 1 tablet oral daily before meal(s) x30 day(s)
chlorpheniramine+pseudoephedrine+ibuprofen 2+30+200 MG tablet 1 tablet oral daily x30 day(s)
chlorpheniramine+pseudoephedrine+ibuprofen 2+30+200 MG tablet 1 tablet oral x30 day(s)
chlorpheniramine+pseudoephedrine+ibuprofen 2+30+200 MG tablet oral x30 day(s)
cyclophosphamide - will receive Monthly IV Cyclophosphamide as per NIH protocole x 6 months then r/a, 1st dose was given in hospital 25NOV2016 x6 month(s)
cyclophosphamide - will receive Monthly IV Cyclophosphamide as per NIH protocole x 6 months then r/a, 1st dose was given in hospital 25NOV2016 x6 month(s) no refill
dalteparin 5000 units SC daily
dalteparin 5000 UNITS SC DIE X 14 DAYS THEN R/A x14 day(s)
dalteparin 5000 UNITS SC DIE X 14 DAYS THEN R/A x14 day(s) no refill
darbepoetin alfa 30 MCG sc injection qweek 30 mcg = 0.3 ml sc q1w x30 day(s)
darbepoetin alfa 30 MCG sc injection qweek refill(s): 3 x30 day(s)
desonide (0.05% ointment) topical
dexlansoprazole (60 MG enteric cap.) 60 mg oral daily
diflucortolone-valerate cream 0.1% Apply in thin layer ob lesions BID x30 day(s)
diflucortolone-valerate cream 0.1% Apply in thin layer ob lesions BID x30 day(s) refill(s): 3
digoxin (0.125 MG tablet) 0.125 mg oral daily
digoxin (0.125 MG tablet) 0.125 mg oral daily prn x30 day(s)
digoxin (0.125 MG tablet) 0.125 mg oral daily prn x30 day(s) (quantity:10  tablet) no refill
digoxin test x30 day(s)
digoxin test x30 day(s) refill(s): 2
docusate-sodium (100 MG capsule) 200 mg oral qhs
docusate-sodium 100 MG capsule 1 capsule oral bid prn x7 day(s)
docusate-sodium 100 MG capsule 1 capsule oral bid prn x7 day(s) (quantity:14  capsule) no refill
docusate-sodium 100 mg PO daily-BID (CODE GI28)
enalapril (10 MG tablet) 1 mg oral tid x30 day(s)
enalapril (10 MG tablet) 1 mg oral tid x30 day(s) refill(s): 3
enalapril (10 MG tablet) 1 mg oral x30 day(s)
enalapril (10 MG tablet) mg oral x30 day(s)
ethinyl-estradiol+norgestimate 35+250 MCG tablets-21 1 tablet oral daily
ferrous-sulfate 300 mg po BID  x30 day(s)
ferrous-sulfate 300 mg po BID  x30 day(s) refill(s): 3
finasteride (5 MG tablet) mg oral
finasteride tab 5 MG po daily x30 day(s)
finasteride tab 5 MG po daily x30 day(s) refill(s): 3
fluticasone 125 MCG metered inh. 1 inhalation oral-inhalation bid x30 day(s)
fluticasone 125 MCG metered inh. 1 inhalation oral-inhalation bid x30 day(s) no refill
fosinopril (10 MG tablet) 10 mg oral daily
furosemide (20 MG tablet) 20 mg oral daily x30 day(s)
furosemide (20 MG tablet) 20 mg oral daily x30 day(s) refill(s): 3
furosemide (20 MG tablet) 60 mg oral bid
furosemide 20 mg PO BID x21 day(s)
furosemide 20 mg PO BID x21 day(s) no refill
furosemide 40 MG tablet 1 tablet oral daily
furosemide tab 40 MG po-oral bid x30 day(s)
furosemide tab 40 MG po-oral bid x30 day(s) refill(s): 3
glycerin 2.7 g suppository + bisacodyl 10 mg suppository to given pr daily prn for constipation, code gi27 x30 day(s)
glycerin 2.7 g suppository + bisacodyl 10 mg suppository to given pr daily prn for constipation, code gi27 x30 day(s) refill(s): 1
hydromorphone 0.5 mg sc q3h regular + 0.25 mg sc q2h prn for dyspnea or pain x30 day(s)
hydromorphone 0.5 mg sc q3h regular + 0.25 mg sc q2h prn for dyspnea or pain x30 day(s) (quantity:150  mg) no refill
hydromorphone 1 mg po q4h regular + 1 mg po q2h prn
hydroxychloroquine (200 MG tablet) 200 mg oral daily x30 day(s)
hydroxychloroquine (200 MG tablet) 200 mg oral daily x30 day(s) refill(s): 3
hydroxyzine (10 MG capsule) 10 mg oral qid prn
ibuprofen (100 MG/5 ML suspension) mg oral x30 day(s)
ipratropium 20 MCG metered inh. 2 inhalation oral-inhalation qid prn x30 day(s)
ipratropium 20 MCG metered inh. 2 inhalation oral-inhalation qid prn x30 day(s) (quantity:200  dose) no refill
iron-salts+calcium-salts+multivitamins tablet 1 tablet oral daily
leuprolide depot kit 11.25 mg IM q12weeks x 2 doses then r/a, 1st dose to be given on 09DEC2016 x6 month(s)
leuprolide depot kit 11.25 mg IM q12weeks x 2 doses then r/a, 1st dose to be given on 09DEC2016 x6 month(s) no refill
lisinopril (5 MG tablet) 7.5 mg oral daily
losartan tab 50 MG po-oral daily x30 day(s)
losartan tab 50 MG po-oral daily x30 day(s) refill(s): 3
medroxyprogesterone 150 mg IM q3months, start on 09DEC2016 x30 day(s)
medroxyprogesterone 150 mg IM q3months, start on 09DEC2016 x30 day(s) refill(s): 3
metoprolol (25 MG tablet) 25 mg oral bid
midazolam for major distress orders : midazolam 2.5 mg sc q15min prn x 3 + hydromorphone 1 mg sc q30min x 2 (dispense 3 syringes of 2.5 mg of midazolam and 2 syringes of hydromorphone 1 mg) x30 day(s)
midazolam for major distress orders : midazolam 2.5 mg sc q15min prn x 3 + hydromorphone 1 mg sc q30min x 2 (dispense 3 syringes of 2.5 mg of midazolam and 2 syringes of hydromorphone 1 mg) x30 day(s) (quantity:1  kit) no refill
morphine (5 MG tablet) 5 mg oral q4h prn
multivitamins as Forza 1 tab po die
mycophenolate mofetil (500 MG tablet) 1500 mg oral bid
nicotine IF PATIENT ACCEPTS : 2 mg gum to chew slowly q1h prn, max 24/day x30 day(s)
nicotine IF PATIENT ACCEPTS : 2 mg gum to chew slowly q1h prn, max 24/day x30 day(s) refill(s): 3
nicotine IF PATIENT ACCEPTS : 21 mg TD daily x 8 weeks then 14 mg TD daily x 2 weeks then 7 mg TD daily x 2 weeks then stop - total x12 week(s)
nicotine IF PATIENT ACCEPTS : 21 mg TD daily x 8 weeks then 14 mg TD daily x 2 weeks then 7 mg TD daily x 2 weeks then stop - total x12 week(s) no refill
nicotine lozenge 2 mg PO Q1-2H PRN **MAX 16 LOZENGES PER DAY** x12 week(s)
nicotine lozenge 2 mg PO Q1-2H PRN **MAX 16 LOZENGES PER DAY** x12 week(s) no refill
nitroglycerin-emergency 0.4 mg/spray - 1 spray SL q5 mins PRN if chest pain (max 3 times per attacks)
oxycodone (5 MG tablet) 5 mg oral qhs
pantoprazole delay rel tab 40 MG po-oral daily x30 day(s)
pantoprazole delay rel tab 40 MG po-oral daily x30 day(s) refill(s): 3
pneumococcus-vaccine as Pneumovax 23 0.5 mL IM x  in 8 weeks (was given Prevnar 13 on 30NOV) x1 day(s)
pneumococcus-vaccine as Pneumovax 23 0.5 mL IM x  in 8 weeks (was given Prevnar 13 on 30NOV) x1 day(s) no refill
polyethylene-glycol 3350 (100% oral powder) 17 g oral daily
potassium-chloride 20 MMOL la-tablet 1 tablet oral daily x21 day(s)
potassium-chloride 20 MMOL la-tablet 1 tablet oral daily x21 day(s) no refill
potassium-chloride 20 MMOL la-tablet oral
prednisone (5 MG tablet) 20 mg oral daily
prednisone 60 mg po qAM until reassessed by nephrology, for lupus nephritis x14 day(s)
prednisone 60 mg po qAM until reassessed by nephrology, for lupus nephritis x14 day(s) no refill
pregabalin (25 MG capsule) 25 mg oral qhs
ramipril (2.5 MG capsule) 2.5 mg oral daily x30 day(s)
ramipril (2.5 MG capsule) 2.5 mg oral daily x30 day(s) refill(s): 3
ramipril (5 MG capsule) 5 mg oral daily x30 day(s)
ramipril (5 MG capsule) 5 mg oral daily x30 day(s) refill(s): 3
rivaroxaban tab 20 MG po-oral daily x30 day(s)
rivaroxaban tab 20 MG po-oral daily x30 day(s) refill(s): 3
scopolamine 0.3 MG sc injection (med order) q4h 0.3 mg = 0.75 ml sc q4h prn  for secretions prn x30 day(s)
scopolamine 0.3 MG sc injection (med order) q4h no refill x30 day(s)
sennosides-a-b 8.6 MG tablet 1 tablet oral qhs prn x7 day(s)
sennosides-a-b 8.6 MG tablet 1 tablet oral qhs prn x7 day(s) (quantity:7  tablet) no refill
sodium bicarbonate 1000 mg po bid x 5 more days then stop x5 day(s)
sodium bicarbonate 1000 mg po bid x 5 more days then stop x5 day(s) no refill
sotalol tab 80 MG - 0.5 TAB po-oral q12h x30 day(s)
sotalol tab 80 MG - 0.5 TAB po-oral q12h x30 day(s) refill(s): 3
sulfamethoxazole+trimethoprim 800+160 MG tablet 1 tablet oral q3x/week x30 day(s)
sulfamethoxazole+trimethoprim 800+160 MG tablet 1 tablet oral q3x/week x30 day(s) refill(s): 3
tobramycin 0.3%: 4 drops in the left ear 2 nights per week x30 day(s)
tobramycin 0.3%: 4 drops in the left ear 2 nights per week x30 day(s) refill(s): 3
vaccine hb as Engerix or Recombivax 40 mcg IM in 1 and 6 months (1st dose given  in hospital 26NOV2016) x6 month(s)
vaccine hb as Engerix or Recombivax 40 mcg IM in 1 and 6 months (1st dose given  in hospital 26NOV2016) x6 month(s) no refill
vitamin-b1 (100 MG tablet) 100 mg oral daily
vitamin-b1 (50 MG tablet) 50 mg oral daily
vitamin-b12 (100 MCG/ML injectable) mcg intramuscular
vitamin-b12 100 MCG im qmonth refill(s): 3 x30 day(s)
vitamin-b12 100 MCG im qmonth x30 day(s)
vitamin-d3 (10,000 IU tablet) 10000 units oral qwednesday x30 day(s)
vitamin-d3 (10,000 IU tablet) 10000 units oral qwednesday x30 day(s) refill(s): 3
warfarin (1 MG tablet) 1 mg oral bid x30 day(s)
warfarin (1 MG tablet) 1 mg oral bid x30 day(s) refill(s): 3
warfarin (2 MG tablet) 2 mg oral daily x30 day(s)
warfarin (2 MG tablet) 2 mg oral daily x30 day(s) refill(s): 3
warfarin (5 MG tablet) 5 mg oral daily
warfarin 3 mg po qHS until INR on Monday june 1 x5 day(s)
warfarin 3 mg po qHS until INR on Monday june 1 x5 day(s) no refill
warfarin tab 5 MG po qhs x30 day(s)
warfarin tab 5 MG po qhs x30 day(s) refill(s): 3
dalteparin 5000 units SC daily
acetaminophen 325-650 mg po QID PRN x30 day(s)
infliximab 400 mg IV q8weeks (last service 21JUNE2016)
prednisone 40 mg po DIE for 1 week then decrease by 5 mg every week
prednisone 40 mg po DIE for 1 week then decrease by 5 mg every week  x8 week(s)
prednisone 40 mg po DIE for 1 week then decrease by 5 mg every week  x8 week(s) no refill
calcium-carbonate 500 mg po BID x30 day(s)
calcium-carbonate 500 mg po BID x30 day(s) refill(s): 3
vitamin-d3 (10,000 IU capsule) 10000 units oral qweek x30 day(s)
vitamin-d3 (10,000 IU capsule) 10000 units oral qweek x30 day(s) refill(s): 3
potassium chloride ext rel tab 40 mEq po-oral bid x30 day(s)
potassium chloride ext rel tab 40 mEq po-oral bid x30 day(s) no refill
vancomycin 125 mg = 5 mL PO Q6H until Aug 20th, 2016 for total 14 days  x10 day(s)
vancomycin 125 mg = 5 mL PO Q6H until Aug 20th, 2016 for total 14 days  x10 day(s) no refill
5-aminosalicylic acid supp 500 mg - supp 1000 MG pr-rectal qhs x30 day(s)
5-aminosalicylic acid supp 500 mg - supp 1000 MG pr-rectal qhs refill(s): 3 x30 day(s)
professional-services Note that patient will start Humira when her C.diff resolved x1 day(s)
professional-services Note that patient will start Humira when her C.diff resolved x1 day(s) no refill
mesalamine (4 G/60 G enema) 1 enema rectal qhs
mesalamine (1000 MG enteric tab.) 4000 mg oral daily
mesalamine EC 1 g po QID - Salofalk ec x30 day(s)
mesalamine EC 1 g po QID - Salofalk ec x30 day(s) refill(s): 3
prednisone 40 mg po DAILY x30 day(s)
prednisone 40 mg po DAILY x30 day(s) refill(s): 3
lorazepam tab 1 MG po-oral daily prn x30 day(s)
lorazepam tab 1 MG po-oral daily prn x30 day(s) no refill
mesalamine (1 G enema) 1 gram rectal qhs x30 day(s)
mesalamine (1 G enema) 1 gram rectal qhs x30 day(s) no refill
adalimumab Humira 80 mg SC once on Aug 12th then 40 mg SC q2weeks as maintenance dose - GI will reassess at 8 weeks x30 day(s)
adalimumab Humira 80 mg SC once on Aug 12th then 40 mg SC q2weeks as maintenance dose - GI will reassess at 8 weeks x30 day(s) refill(s): 3
ferrous-fumarate (300 MG capsule) 300 mg oral daily x7 day(s)
ferrous-fumarate (300 MG capsule) 300 mg oral daily x7 day(s) no refill
ulipristal (5 MG tablet) 5 mg oral daily x7 day(s)
ulipristal (5 MG tablet) 5 mg oral daily x7 day(s) no refill
methotrexate 5 mg = 0,2 mL SC qweek
baclofen 10 mg po BID at 22h and 3h
pregabalin 25 mg po TID at 8h-17h-22h
baclofen 20 mg po TID at 8h00 - 16h00 - 22h00
baclofen Decrease to 10 mg po TID at 8h00 - 16h00 - 22h00  x30 day(s)
baclofen Decrease to 10 mg po TID at 8h00 - 16h00 - 22h00  x30 day(s) refill(s): 3
pregabalin 100 mg po TID at 8h-17h-22h (with 25 mg for total dose 125 mg)
pregabalin Decrease to 50 mg po BID at 8h-20h x30 day(s)
pregabalin Decrease to 50 mg po BID at 8h-20h x30 day(s) refill(s): 3
hydrocortisone-acetate 1% cream Apply BID in thinlayer to thighs/buttocks x30 day(s)
hydrocortisone-acetate 1% cream Apply BID in thinlayer to thighs/buttocks x30 day(s) refill(s): 3
amoxicillin + clavulanate 500 mg po q8h for total 10 days until Aug 15th inclusively  x5 day(s)
amoxicillin + clavulanate 500 mg po q8h for total 10 days until Aug 15th inclusively  x5 day(s) no refill
professional-services Methotrexate to be held until infection resolved x1 day(s)
professional-services Methotrexate to be held until infection resolved x1 day(s) no refill
fosinopril 5 mg po DIE at 8h00 x7 day(s)
fosinopril 5 mg po DIE at 8h00 x7 day(s) no refill
rosuvastatin 20 mg po DAILY at 17h00 x7 day(s)
rosuvastatin 20 mg po DAILY at 17h00 x7 day(s) no refill
clopidogrel (75 MG tablet) 75 mg oral daily x7 day(s)
clopidogrel (75 MG tablet) 75 mg oral daily x7 day(s) no refill
nicoumalone 2 mg po qPM 5 days a week and 3 mg po 2 days a week (Sunday - Wed) x7 day(s)
nicoumalone 2 mg po qPM 5 days a week and 3 mg po 2 days a week (Sunday - Wed) x7 day(s) no refill
calcium-carbonate+vitamin-d 500 MG+400 IU tablet 1 tablet oral bid x7 day(s)
calcium-carbonate+vitamin-d 500 MG+400 IU tablet 1 tablet oral bid x7 day(s) no refill
estrogens vag cream 0,625 mg/g Apply to vagina 2 times a week (Mon-Frid) x7 day(s)
estrogens vag cream 0,625 mg/g Apply to vagina 2 times a week (Mon-Frid) x7 day(s) no refill
methotrexate 25 mg/mL   5 mg = 0,2 mL SC qweek on Wednesday x7 day(s)
methotrexate 25 mg/mL   5 mg = 0,2 mL SC qweek on Wednesday x7 day(s) no refill
cyclobenzaprine (10 MG tablet) 10 mg oral qhs x7 day(s)
cyclobenzaprine (10 MG tablet) 10 mg oral qhs x7 day(s) no refill
clonazepam 0,25 mg po DIE at 23h00 x7 day(s)
clonazepam 0,25 mg po DIE at 23h00 x7 day(s) no refill
levodopa+carbidopa CR 100+25 mg 1,5 tablets po 7 times a day at 3h-6h-8h-14h-17h-20h-23h x7 day(s)
levodopa+carbidopa CR 100+25 mg 1,5 tablets po 7 times a day at 3h-6h-8h-14h-17h-20h-23h x7 day(s) no refill
levodopa+carbidopa 100+25 mg 1 tablet po at 6h and 0,5 tablet 5 times a day at 8h-11h-14h-17h-20h x7 day(s)
levodopa+carbidopa 100+25 mg 1 tablet po at 6h and 0,5 tablet 5 times a day at 8h-11h-14h-17h-20h x7 day(s) no refill
docusate-sodium 200 mg po BID at 8h and 22h (CODE GI27) x7 day(s)
docusate-sodium 200 mg po BID at 8h and 22h (CODE GI27) x7 day(s) no refill
pantoprazole (40 MG enteric tab.) 40 mg oral qam x7 day(s)
pantoprazole (40 MG enteric tab.) 40 mg oral qam x7 day(s) no refill
sennosides-a-b 17,2 mg po qHS at 22h (CODE GI27) x7 day(s)
sennosides-a-b 17,2 mg po qHS at 22h (CODE GI27) x7 day(s) no refill
folic-acid 5 mg po qweek the day after methotrexate (thursday) x7 day(s)
folic-acid 5 mg po qweek the day after methotrexate (thursday) x7 day(s) no refill
calcium-carbonate+vitamin-d 500 MG+400 IU tablet 1 tablet oral daily
pancrelipase (25,000 IU++ enteric cap.) 1 cap oral daily
etanercept (50 MG inject.unit) 50 mg subcutaneous q2weeks
prednisone 2.5 mg PO q2days (taken qMon-Wed-Fri by patient)
prednisone 2.5 mg PO q Mon, Wed, Fri as actually taken prior to admission x30 day(s)
prednisone 2.5 mg PO q Mon, Wed, Fri as actually taken prior to admission x30 day(s) refill(s): 3
atenolol (50 MG tablet) 50 mg oral qhs x30 day(s)
atenolol (50 MG tablet) 50 mg oral qhs x30 day(s) refill(s): 3
losartan (50 MG tablet) 50 mg oral qam x30 day(s)
losartan (50 MG tablet) 50 mg oral qam x30 day(s) refill(s): 3
esomeprazole (40 MG enteric tab.) 40 mg oral qam x30 day(s)
esomeprazole (40 MG enteric tab.) 40 mg oral qam x30 day(s) refill(s): 3
vitamin-d3 (10,000 IU tablet) 10000 ui oral qweek x30 day(s)
vitamin-d3 (10,000 IU tablet) 10000 ui oral qweek x30 day(s) refill(s): 12
calcium carbonate tab 500 MG po-oral daily 500 mg = 1 tab po daily with breakfast x30 day(s)
calcium carbonate tab 500 MG po-oral daily x30 day(s) refill(s): 12
lactulose oral soln 667 mg/ml (500ml) - oral soln 20 ML po-oral bid prn x30 day(s)
lactulose oral soln 667 mg/ml (500ml) - oral soln 20 ML po-oral bid prn no refill x30 day(s)
cefazolin 2 g iv q8h until 09JAN2016 to complete 8 weeks  --> will be organized as home IV ATBx through Calea pharmacy 514 335 3500 x8 week(s)
cefazolin 2 g iv q8h until 09JAN2016 to complete 8 weeks  --> will be organized as home IV ATBx through Calea pharmacy 514 335 3500 x8 week(s) no refill
hydromorphone tab 2 MG po-oral q6h prn x30 day(s)
hydromorphone tab 2 MG po-oral q6h prn x30 day(s) no refill
acetaminophen 650 mg po QID x 2 weeks then 650 mg po QID PRN x30 day(s)
acetaminophen 650 mg po QID x 2 weeks then 650 mg po QID PRN x30 day(s) refill(s): 2
docusate-sodium (100 MG capsule) 100 mg oral bid x30 day(s)
docusate-sodium (100 MG capsule) 100 mg oral bid x30 day(s) refill(s): 1
sennosides-a-b (8.6 MG tablet) 8.6 mg oral bid prn x30 day(s)
sennosides-a-b (8.6 MG tablet) 8.6 mg oral bid prn x30 day(s) refill(s): 1
alendronate (70 MG tablet) 70 mg oral qweek x30 day(s)
alendronate (70 MG tablet) 70 mg oral qweek x30 day(s) refill(s): 12
miscellaneous Vaccination given on 26NOV2015 in hospital : Influvac 0.5 mL IM x 1 , Boostrix 0.5 mL IM x 1 and Prevnar 13 0.5 mL IM x 1 x1 day(s)
miscellaneous Vaccination given on 26NOV2015 in hospital : Influvac 0.5 mL IM x 1 , Boostrix 0.5 mL IM x 1 and Prevnar 13 0.5 mL IM x 1 x1 day(s) no refill
hydrocortisone-valerate cream 0.2% apply topically BID x7 day(s)
hydrocortisone-valerate cream 0.2% apply topically BID x7 day(s) no refill
oils-miscellaneous Rhinaris Nozoil 100% nasal spray apply BID for 30 days x7 day(s)
oils-miscellaneous Rhinaris Nozoil 100% nasal spray apply BID for 30 days x7 day(s) no refill
polyethylene-glycol Secaris 15%-20% apply in each nostril qHS for 30 days. x7 day(s)
polyethylene-glycol Secaris 15%-20% apply in each nostril qHS for 30 days. x7 day(s) no refill
metformin (500 MG tablet) mg oral
tamsulosin (0.4 MG la-tablet) mg oral
omeprazole (20 MG enteric cap.) mg oral
urea (20% cream) topical
urea (20% cream) 1 MG topical 5x per day x30 day(s)
urea (20% cream) 1 MG topical 5x per day x30 day(s) refill(s): 3
hydrochlorothiazide (12.5 MG tablet) 1 mg oral qid x30 day(s)
hydrochlorothiazide (12.5 MG tablet) 1 mg oral qid x30 day(s) refill(s): 3
ibuprofen (200 MG tablet) 2 mg oral daily x30 day(s)
ibuprofen (200 MG tablet) 2 mg oral daily x30 day(s) refill(s): 3
atorvastatin (10 MG tablet) 1 mg oral daily x7 day(s)
atorvastatin (10 MG tablet) 1 mg oral daily x7 day(s) no refill
methocarbamol+ibuprofen 500+200 MG caplet oral x30 day(s)
digoxin (0.0625 MG tablet) 0.062 mg oral daily x30 day(s)
digoxin (0.0625 MG tablet) 0.062 mg oral daily x30 day(s) refill(s): 3
digoxin (0.0625 MG tablet) 0.062 mg oral daily x30 day(s) refill(s): 1
digoxin (0.125 MG tablet) mg oral x30 day(s)
digoxin tab 62.5 MCG po daily x30 day(s)
digoxin (0.125 MG tablet) mg oral
digoxin tab 62.5 MCG po daily x30 day(s) refill(s): 3
atorvastatin tab 20 MG po qhs x30 day(s)
atorvastatin (20 MG tablet) mg oral
atorvastatin tab 20 MG po qhs x30 day(s) refill(s): 3
carvedilol (6.25 MG tablet) mg oral
ramipril (2.5 MG capsule) mg oral x7 day(s)
ramipril (2.5 MG capsule) 2 mg oral x7 day(s)
ramipril (2.5 MG capsule) 2.5 mg oral x7 day(s)
ramipril (2.5 MG capsule) 2.5 mg oral daily x7 day(s)
ramipril (2.5 MG capsule) 2.5 mg oral daily x7 day(s) no refill
warfarin (2 MG tablet) mg oral x7 day(s)
warfarin (2 MG tablet) 2 mg oral x7 day(s)
warfarin (2 MG tablet) 2 mg oral daily x7 day(s)
warfarin (2 MG tablet) 2 mg oral daily x7 day(s) no refill
warfarin (5 MG tablet) mg oral
asa-antiplatelet (80 MG enteric tab.) mg oral
furosemide tab 20 MG po daily x30 day(s)
furosemide (20 MG tablet) mg oral
furosemide tab 20 MG po daily x30 day(s) refill(s): 3
cefadroxil (500 MG capsule) mg oral
vancomycin (250 MG capsule) mg oral
vancomycin (250 MG capsule) 2 mg oral
vancomycin (250 MG capsule) 25 mg oral
vancomycin (250 MG capsule) 250 mg oral
vancomycin (250 MG capsule) 250 mg oral daily
vancomycin (250 MG capsule) 250 mg oral daily x1 day(s)
vancomycin (250 MG capsule) 250 mg oral daily x10 day(s)
vancomycin (250 MG capsule) 250 mg oral daily x10 day(s) no refill
tamsulosin cr tab 0.4 MG po daily x30 day(s)
tamsulosin cr tab 0.4 MG po daily x30 day(s) refill(s): 3
acetaminophen (325 MG caplet) 325 mg oral daily x30 day(s)
acetaminophen (325 MG caplet) 325 mg oral daily x30 day(s) refill(s): 3
lorazepam (1 MG tablet) mg oral x30 day(s)
lorazepam (1 MG tablet) 1 mg oral x30 day(s)
lorazepam (1 MG tablet) 1 mg oral daily x30 day(s)
lorazepam (1 MG tablet) 1 mg oral daily x30 day(s) refill(s): 3
desonide (0.05% ointment) topical x7 day(s)
desonide x7 day(s)
desonide t x7 day(s)
desonide te x7 day(s)
desonide tes x7 day(s)
desonide test x7 day(s)
desonide test x7 day(s) no refill
diflucortolone-valerate test x30 day(s)
diflucortolone-valerate test x30 day(s) refill(s): 3
vitamin-b12 100 MCG im q4w 100 mcg = 1 ml im every 4 weeks  monthly next dose march 1st x30 day(s)
polyurethane-film+carboxymethylcellulose-dressings 10-10 CM dressing x30 day(s)
polyurethane-film+carboxymethylcellulose-dressings x30 day(s)
polyurethane-film+carboxymethylcellulose-dressings t x30 day(s)
polyurethane-film+carboxymethylcellulose-dressings tes x30 day(s)
polyurethane-film+carboxymethylcellulose-dressings test x30 day(s)
polyurethane-film+carboxymethylcellulose-dressings test x30 day(s) refill(s): 3
ramipril (2.5 MG capsule) 2.5 mg oral daily x1 day(s)
ramipril (2.5 MG capsule) 2.5 mg oral daily x1 day(s) no refill
tamsulosin cr tab 0.4 MG po daily x30 day(s) refill(s): 1
digoxin (0.125 MG tablet) 125 mcg oral daily
atorvastatin (20 MG tablet) 20 mg oral qhs x30 day(s)
atorvastatin (20 MG tablet) 20 mg oral qhs x30 day(s) refill(s): 3
carvedilol (6.25 MG tablet) 6.25 mg oral bid x30 day(s)
carvedilol (6.25 MG tablet) 6.25 mg oral bid x30 day(s) refill(s): 3
carvedilol (12.5 MG tablet) 12.5 mg oral bid
antigens-miscellaneous (sc allergens) x30 day(s)
ramipril (2.5 MG capsule) 2.5 mg oral daily
ramipril (5 MG capsule) 5 mg oral daily
warfarin (1 MG tablet) 1 mg oral qhs
tamsulosin (0.4 MG la-tablet) 0.4 mg oral daily x30 day(s)
tamsulosin (0.4 MG la-tablet) 0.4 mg oral daily x30 day(s) refill(s): 3
lorazepam (1 MG tablet) 1 mg oral qhs prn
desonide 0,05% oint Apply DAILY PRN in thin layer x7 day(s)
desonide 0,05% oint Apply DAILY PRN in thin layer x7 day(s) no refill
vitamin-b12 (100 MCG/ML injectable) 100 mcg intramuscular qmonth x7 day(s)
vitamin-b12 (100 MCG/ML injectable) 100 mcg intramuscular qmonth x7 day(s) no refill
finasteride (5 MG tablet) 5 mg oral daily x7 day(s)
finasteride (5 MG tablet) 5 mg oral daily x7 day(s) no refill
polyurethane-film+carboxymethylcellulose-dressings 10-15 CM dressing 1 application q2d
tamsulosin (0.4 MG la-tablet) 0.4 mg oral daily x7 day(s)
tamsulosin (0.4 MG la-tablet) 0.4 mg oral daily x7 day(s) no refill
furosemide (20 MG tablet) 20 mg oral daily x7 day(s)
furosemide (20 MG tablet) 20 mg oral daily x7 day(s) no refill
digoxin (0.0625 MG tablet) 62.5 mcg oral daily
hydromorphone (1 MG tablet) mg oral
hydromorphone (1 MG tablet) 0 mg oral
hydromorphone (1 MG tablet) 5 mg oral
hydromorphone (1 MG tablet) 0.5 mg oral
hydromorphone (1 MG tablet) 0.5 mg oral tid
hydromorphone (1 MG tablet) 0.5 mg oral q6h
hydromorphone (1 MG tablet) 0.5 mg oral q6h prn
hydromorphone (1 MG tablet) 0.5 mg oral q6h prn x2 day(s)
hydromorphone (1 MG tablet) 0.5 mg oral q6h prn x20 day(s)
hydromorphone (1 MG tablet) 0.5 mg oral q6h prn x1 day(s)
hydromorphone (1 MG tablet) 0.5 mg oral q6h prn x10 day(s)
hydromorphone (1 MG tablet) 0.5 mg oral q6h prn x10 day(s) (quantity:20  tablet) no refill
atorvastatin (20 MG tablet) 20 mg oral qhs x7 day(s)
atorvastatin (20 MG tablet) 20 mg oral qhs x7 day(s) no refill
carvedilol (6.25 MG tablet) 6.25 mg oral bid x7 day(s)
carvedilol (6.25 MG tablet) 6.25 mg oral bid x7 day(s) no refill
carvedilol (6.25 MG tablet) 6.25 mg oral bid
carvedilol 6,25 mg po BID (with 1 tab of 12,5 mg = total dose 18,75 mg) x30 day(s)
carvedilol 6,25 mg po BID (with 1 tab of 12,5 mg = total dose 18,75 mg)
carvedilol 6,25 mg po BID (with 1 tab of 12,5 mg = total dose 18,75 mg) x30 day(s) refill(s): 3
carvedilol 6,25 mg po BID (w x30 day(s)
carvedilol 6,25 mg po BID ( x30 day(s)
carvedilol 6,25 mg po BID  x30 day(s)
carvedilol 6,25 mg po BID x30 day(s)
carvedilol 6,25 mg po BID x30 day(s) refill(s): 3
