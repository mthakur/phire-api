<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Parser Info.</title>
    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<jsp:include page="/WEB-INF/pages/parser/navbar.jsp">
    <jsp:param name="title" value="home"/>
</jsp:include>

<div class="container">

    <div class="blog-header">
        <h3 class="blog-title">Parsed Prescriptions</h3>
        <!--p class="lead blog-description">The official example template of creating a blog with Bootstrap.</p-->
    </div>

    <hr/>
    ${parsedPrescription}
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Prescription</th>
                <th>Json Response</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>${parsedPrescription.prescription}</td>
                <td>${parsedPrescription.jsonResponse}</td>
            </tr>
            </tbody>
        </table>
    </div>

</div>

</body>
</html>
