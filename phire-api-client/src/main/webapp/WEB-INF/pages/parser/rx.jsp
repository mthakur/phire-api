<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Parser Info.</title>

    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <!--script src="<c:url value="/resources/js/bootstrap.min.js" />"></script-->
    <!--script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<jsp:include page="navbar.jsp">
    <jsp:param name="title" value="DSQ Prescriptions"/>
    <jsp:param name="page" value="dsq"/>
</jsp:include>

<div class="container">

    <jsp:include page="pagination.jsp">
        <jsp:param name="title" value="DSQ Prescription"/>
    </jsp:include>

    <table class="table table-bordered table-condensed table-striped">
        <c:forEach items="${prescriptionList}" var="prescription" varStatus="theCount">
            <tr>
                <td>${prescription.instructions}</td>
            </tr>
        </c:forEach>
    </table>

</div>
</body>
</html>
