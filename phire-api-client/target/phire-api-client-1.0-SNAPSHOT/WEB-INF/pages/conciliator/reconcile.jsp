<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html" charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reconciler.</title>
    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/js/bootstrap.js" />" rel="stylesheet">
    <script src="http://cdn.jsdelivr.net/webjars/jquery/3.1.1/jquery.min.js"
            th:src="@{/webjars/jquery/3.1.1/jquery.min.js}" type="text/javascript"></script>
    <script src="http://cdn.jsdelivr.net/webjars/bootstrap/3.3.7/js/bootstrap.min.js"
            th:src="@{/webjars/bootstrap/3.3.7/js/bootstrap.min.js}" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function(){

            $('#btnPopulate').click(function (event) {
                event.preventDefault();
                $.get('dsq', function (data) {
                    $('#dsqJson').val(data);
                })
                $.get('omnimed', function (data) {
                    $('#omnimedJson').val(data);
                })
            });

            $('#btnClearAll').click(function (evnet) {
                evnet.preventDefault();
                $('#dsqJson').val('');
                $('#omnimedJson').val('');
                $('#matchedResult').empty();
            })

            $('#btnClearOutput').click(function (evnet) {
                evnet.preventDefault();
                $('#resultReconcilation').empty();
                $('#matchedResult').empty();
            })

            $('#btnSVG').click(function (event) {
                event.preventDefault();
                var omnimedProfile=$("#omnimedJson").val();
                var dsqJson=$('#dsqJson').val();
                var data=new FormData();
                data.append('physicianLicenceNumber', '1841907');
                data.append('omniMedProfile', omnimedProfile);
                data.append('dsqResponse', dsqJson);
                $.ajax({
                    type: 'POST',
                    url: '/reconciler/reconciler/reconcile_svg',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $("#matched_result").empty();
                        var svgWrapper=$("<div id='svgWrapper' style='border:1px solid blue; overflow:scroll; width:1100px; height:500px;margin-top: 20px'></div>");
                        svgWrapper.append(data);
                        $("#matched_result").append(svgWrapper);

//                        $("#reconcilerGraph").style.width="200%";
                    }
                });
            })

            $('#btnReconcileMapId').click(function (event) {
                event.preventDefault();
                var omnimedProfile=$("#omnimedJson").val();
                var dsqJson=$('#dsqJson').val();
                var data=new FormData();
                data.append('omniMedProfile', omnimedProfile);
                data.append('dsqResponse', dsqJson);
                $.ajax({
                    type:'POST',
                    url:'/reconciler/reconciler/reconcileMap',
                    data: data,
                    processData:false,
                    contentType: false,
                    success: function(data){
                        var str='';
                        var row=0;
                        $("#matched_result").empty();
                        var $table=$('<table id="match_result" class="table"></table>');
                        $table.append('<thead><tr><th> No. </th><th> Omnimed UUID </th><th> || </th><th>DSQ ID</th></tr></thead>');
                        $table.append('<tbody id="matchedResult"></tbody>');
                        $("#matched_result").append($table);
                        str=JSON.stringify(data);
                        $.each(data, function (index, item) {
                            var $trData=$("<tr></tr>");
                            $trData.addClass('success');
                            row++;
                            var $td=$('<td></td>').append(row);
                            $trData.append($td);
                            $td=$('<td></td>').append(item.emrId);
                            $trData.append($td);
                            $trData.append('<td>||</td>');
                            $td=$('<td></td>').append(item.dsqId);
                            $trData.append($td);
                            $('#matchedResult').append($trData);
                        })
                    },
                });
            })

            $('#btnReconcile').click(function (event) {
                event.preventDefault();
                var omnimedProfile=$("#omnimedJson").val();
                var dsqJson=$('#dsqJson').val();
                var data=new FormData();
                data.append('omniMedProfile', omnimedProfile);
                data.append('dsqResponse', dsqJson);
                $.ajax({
                    type:'POST',
                    url:'/reconciler/reconciler/reconcile',
                    data: data,
                    processData:false,
                    contentType: false,
                    success: function(data){
                        var str='';
                        var row=0;
                        $("#matched_result").empty();
                        var $table=$('<table id="match_result" class="table"></table>');
                        $table.append('<thead><tr><th> No. </th><th>Licence No.</th><th> DIN</th><th>GenCode</th><th>Drug Name</th><th>Date</th><th> || </th><th>Licence No.</th><th> DIN</th><th>GenCode</th><th>Drug Name</th><th>Date</th></tr></thead>');
                        $table.append('<tbody id="matchedResult"></tbody>');
                        $("#matched_result").append($table);
                        str=JSON.stringify(data);
//                        $('#resultJson').val(str);
                        $.each(data, function (index, item) {
                            var $trData=$("<tr></tr>");
                            $trData.addClass('success');
                            row++;
                            var $td=$('<td></td>').append(row);
                            $trData.append($td);
                            var omnimedMedication = item.omnimedMedication;
                            if(omnimedMedication != null){
                                var drug = omnimedMedication.drug;
                                var genericCode;
                                var din;
                                var drugName;
                                if(drug){
                                    genericCode=drug.genericCode;
                                    din=drug.dinCode;
                                    drugName=drug.englishTerm;
                                }
                                var licenceNumber=omnimedMedication.consultation.caregiver.licenceNumber;
                                var drugDate=omnimedMedication.prescription.beginningDate;

                                $td=$('<td></td>').append(licenceNumber);
                                $trData.append($td);
                                $td=$('<td></td>').append(din);
                                $trData.append($td);
                                $td=$('<td></td>').append(genericCode);
                                $trData.append($td);
                                $td=$('<td></td>').append(drugName);
                                $trData.append($td);
                                $td=$('<td></td>').append(drugDate.year+'/'+(drugDate.monthOfYear)+'/'+drugDate.dayOfMonth);
                                $trData.append($td);
                            }
                            else{
                                $trData.append('<td>-</td><td>-</td><td>-</td><td>-</td><td>-</td>');
                            }
                            $trData.append('<td>||</td>');
//                            $('#matched_omnimed').append($trData);
                            var dsqMedication=item.dsqMedication;
                            if(dsqMedication != null){
                                var licenceNumber=dsqMedication.author.licenceNumber;
                                var dispenDate=null;
                                if(dsqMedication.deliveryStatus.firstDeliveryDate !=null)
                                    dispenDate =dsqMedication.deliveryStatus.firstDeliveryDate;
                                else
                                    dispenDate =dsqMedication.createdDate;
                                var din=dsqMedication.product.code;
                                var genCode=dsqMedication.product.genCode;
                                var drugName=dsqMedication.product.name;
                                $td=$('<td></td>').append(licenceNumber);
                                $trData.append($td);
                                $td=$('<td></td>').append(din);
                                $trData.append($td);
                                $td=$('<td></td>').append(genCode);
                                $trData.append($td);
                                $td=$('<td></td>').append(drugName);
                                $trData.append($td);
                                $td=$('<td></td>').append(dispenDate.year+'/'+(dispenDate.monthOfYear)+'/'+dispenDate.dayOfMonth);
                                $trData.append($td);
                            }
                            else{
                                $trData.append('<td>-</td><td>-</td><td>-</td><td>-</td><td>-</td>');
                            }

                            $('#matchedResult').append($trData);
                        })
                    },

                });
            })
        });
    </script>
</head>
<body>

<jsp:include page="/WEB-INF/pages/parser/navbar.jsp">
    <jsp:param name="title" value="home"/>
</jsp:include>

<div class="container">

    <div class="blog-header">
        <h3 class="blog-title">Reconciler</h3>
    </div>

    <form:form method="post" modelAttribute="reconcilerData" >

        <div class="form-group  col-lg-14">
            <div class="col-lg-6">
                <label for="omnimedJson">Omnimed Json : </label>
                <form:textarea path="omnimedJson" class="form-control" rows="1" id="omnimedJson"></form:textarea>
            </div>
            <div class="col-lg-6">
                <label for="dsqJson">Dsq Json : </label>
                <form:textarea path="dsqJson" class="form-control" rows="1" id="dsqJson"></form:textarea>
            </div>
        </div>
        <div class="col-lg-14">
            <button type="submit" class="btn btn-primary  col-lg-2" id="btnSVG" style="margin:0px;">SVG Graph</button>
            <button type="submit" class="btn btn-primary  col-lg-2" id="btnReconcile" style="margin:0px;">Map(Omnimed-Dsq)</button>
            <button type="submit" class="btn btn-primary  col-lg-2" id="btnReconcileMapId" style="margin:0px;">Map(OmniId- DsqId)</button>
            <button type="button" class="btn btn-primary  col-lg-2" id="btnPopulate" style="margin:0px;">Populate</button>
            <button type="button" class="btn btn-primary  col-lg-2" id="btnClearAll" style="margin:0px;">Clear All</button>
            <button type="button" class="btn btn-primary  col-lg-2" id="btnClearOutput" style="margin:0px;">Clear Output</button>
        </div>

        <div class="col-lg-12 pre_scrollable" id="matched_result"></div>
        <%--<div class="col-lg-12">--%>
        <%--<label for="resultReconcilation">Matched Result: </label>--%>
        <%--<form:textarea path="resultReconcilation" class="form-control" rows="10" id="resultReconcilation"></form:textarea>--%>
        <%--</div>--%>

        <%--<c:if test="${not empty reconcilerData.resultReconcilation}">--%>
            <%--<div class="col-lg-12 pre_scrollable" id="matched_result">--%>
                    <%--<table id="match_result" class="table">--%>
                    <%--<thead>--%>
                    <%--<tr>--%>
                    <%--<th>Licence No.</th>--%>
                    <%--<th> DIN</th>--%>
                    <%--<th>GenCode</th>--%>
                    <%--<th>Drug Name</th>--%>
                    <%--<th>Date</th>--%>
                    <%--<th> || </th>--%>
                    <%--<th>Licence No.</th>--%>
                    <%--<th> DIN</th>--%>
                    <%--<th>GenCode</th>--%>
                    <%--<th>Drug Name</th>--%>
                    <%--<th>Date</th>--%>
                    <%--</tr>--%>
                    <%--</thead>--%>
                    <%--<tbody id="matchedResult">--%>
                    <%--<c:forEach var="item" items="${reconcilerData.resultReconcilation}">--%>
                    <%--<tr class="success">--%>
                    <%--<c:choose>--%>
                    <%--<c:when test="${not empty item.omnimedMedication}" >--%>
                    <%--<td> <c:out value="${item.omnimedMedication.consultation.caregiver.licenceNumber}"/></td>--%>
                    <%--<td> <c:out value="${item.omnimedMedication.drug.dinCode}"/></td>--%>
                    <%--<td> <c:out value="${item.omnimedMedication.drug.genericCode}"/></td>--%>
                    <%--<td> <c:out value="${item.omnimedMedication.drug.englishTerm}"/></td>--%>
                    <%--&lt;%&ndash;<td> <joda:format value="${item.omnimedMedication.dataManipulationDetails.createdDate}" style="MM"/></td>&ndash;%&gt;--%>
                    <%--<td> <c:out value="${item.omnimedMedication.dataManipulationDetails.createdDate.year}-${item.omnimedMedication.dataManipulationDetails.createdDate.monthOfYear}-${item.omnimedMedication.dataManipulationDetails.createdDate.dayOfMonth}"/></td>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${empty item.omnimedMedication}">--%>
                    <%--<td>-</td><td>-</td><td>-</td><td>-</td><td>-</td>--%>
                    <%--</c:when>--%>
                    <%--</c:choose>--%>
                    <%--<td> || </td>--%>
                    <%--<c:choose>--%>
                    <%--<c:when test="${not empty item.dsqMedication}">--%>
                    <%--<td> <c:out value="${item.dsqMedication.author.licenceNumber}"/></td>--%>
                    <%--<td> <c:out value="${item.dsqMedication.product.code}"/></td>--%>
                    <%--<td> <c:out value="${item.dsqMedication.product.genCode}"/></td>--%>
                    <%--<td> <c:out value="${item.dsqMedication.product.name}"/></td>--%>
                    <%--&lt;%&ndash;<td> <joda:format value="${item.dsqMedication.createdDate}" style="MM"/></td>&ndash;%&gt;--%>
                    <%--<td> <c:out value="${item.dsqMedication.createdDate.year}-${item.dsqMedication.createdDate.monthOfYear}-${item.dsqMedication.createdDate.dayOfMonth}" /></td>--%>
                    <%--</c:when>--%>
                    <%--<c:when test="${empty item.dsqMedication}">--%>
                    <%--<td>-</td><td>-</td><td>-</td><td>-</td><td>-</td>--%>
                    <%--</c:when>--%>
                    <%--</c:choose>--%>

                    <%--</tr>--%>
                    <%--</c:forEach>--%>
                    <%--</tbody>--%>
                    <%--</table>--%>
            <%--</div>--%>
        <%--</c:if>--%>
    </form:form>
</div>
</body>
</html>
