<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Parser Info.</title>
    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/js/bootstrap.js" />" rel="stylesheet">

</head>
<body>

<jsp:include page="/WEB-INF/pages/parser/navbar.jsp">
    <jsp:param name="title" value="home"/>
</jsp:include>

<div class="container">

    <div class="blog-header">
        <h3 class="blog-title">Parse Prescriptions</h3>
    </div>

    <form:form method="post" modelAttribute="parserInput" action="parse">
        <div class="form-group">
            <label for="prescription">Enter Prescriptions : </label>
            <form:textarea path="prescriptions" class="form-control" rows="5" id="prescription"
                           acceptCharset="UTF-8"></form:textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form:form>

    <c:if test="${not empty parserInputs}">
        <hr/>
        <table class="table table-hover table-bordered">
            <tbody>
            <c:forEach items="${parserInputs}" var="p" varStatus="theCount">
                <tr class="${not empty p.parsedDrugInfo.error ? "danger" : "success"}">
                    <td colspan="2">${p.drugName} ${p.instruction} ${p.prescriptions}</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <pre id="json${theCount.count}" content=""></pre>
                        <script>
                            document.getElementById("json${theCount.count}").innerHTML = JSON.stringify(${p.parsedDrugInfo.jsonRs}, undefined, 2);
                            document.getElementById("json${theCount.count}").style.fontWeight = 'bold';
                        </script>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>
</body>
</html>
