<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href='<c:url value="/"/>'>PHIRE API Client</a>
        </div>
        <ul class="nav navbar-nav">
            <li class=""><a href='<c:url value="/"/>'>Home</a></li>
            <li class=""><a href='<c:url value="/parse"/>'>Parser Input</a></li>
            <li class=""><a href='<c:url value="/parse/rrx"/>'>RightRx Generated</a></li>
            <li class=""><a href='<c:url value="/parse/chart"/>'>Chart Abstractions</a></li>
            <li class=""><a href='<c:url value="/parse/dsq"/>'>DSQ</a></li>
            <li class=""><a href='<c:url value="/reconcile"/>'>reconciler</a></li>
        </ul>
    </div>
</nav>