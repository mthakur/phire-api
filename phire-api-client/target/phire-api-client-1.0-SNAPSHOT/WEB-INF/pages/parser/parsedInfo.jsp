<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>

<c:if test="${not empty prescriptionDataList}">

    <hr/>
    <div class="panel-group" id="sentences">
            <%--
                    <div class="panel-body">

            <table class="table table-bordered table-condensed table-striped">
                <tr>
                    <td colspan="2"><b>Success Rate : ${successRate}, Processing Time : ${averageProcessingTime}</b></td>
                    <td colspan="2">Frequency</td>
                    <td colspan="2">Dose</td>
                    <td colspan="2">Dose Unit</td>
                    <!--td colspan="2">PRN</td-->
                    <!--td colspan="2">Directives</td-->
                    <td colspan="2">Format</td>
                </tr>
                <c:forEach items="${prescriptionDataList}" var="prescriptionData" varStatus="theCount">
                    <tr>
                        <!--class="${prescriptionData.success() ? 'success' : 'warning' }"-->
                        <td>${theCount.count}</td>

                        <td>
                               ${fn:toLowerCase(prescriptionData.drugName)} ${fn:toLowerCase(prescriptionData.instructions)}
                        </td>
                        <!--td>${prescriptionData.drugInfo.frequency}</td-->


                        <td class="${(prescriptionData.drugInfo.frequency ne prescriptionData.parsedDrugInfo.frequency) and (prescriptionData.drugInfo.frequency ne prescriptionData.parsedDrugInfo.freqCode) ? 'info' : ''}">
                               ${prescriptionData.drugInfo.frequency}
                        </td>
                        <td class="${(prescriptionData.drugInfo.frequency ne prescriptionData.parsedDrugInfo.frequency) and (prescriptionData.drugInfo.frequency ne prescriptionData.parsedDrugInfo.freqCode) ? 'info' : ''}">
                               ${prescriptionData.parsedDrugInfo.frequency} (${prescriptionData.parsedDrugInfo.freqCode})
                        </td>

                        <td class="${prescriptionData.drugInfo.dosePerAdmin ne prescriptionData.parsedDrugInfo.dosePerAdmin ? 'warning' : ''}">
                               ${prescriptionData.drugInfo.dosePerAdmin}
                        </td>
                        <td class="${prescriptionData.drugInfo.dosePerAdmin ne prescriptionData.parsedDrugInfo.dosePerAdmin ? 'warning' : ''}">
                               ${prescriptionData.parsedDrugInfo.dosePerAdmin}
                        </td>

                        <td class="${prescriptionData.drugInfo.unitAtAdmin ne prescriptionData.parsedDrugInfo.unitAtAdmin ? 'danger' : ''}">
                               ${prescriptionData.drugInfo.unitAtAdmin}
                        </td>
                        <td class="${prescriptionData.drugInfo.unitAtAdmin ne prescriptionData.parsedDrugInfo.unitAtAdmin ? 'danger' : ''}">
                               ${prescriptionData.parsedDrugInfo.unitAtAdmin}
                        </td>

                        <!--td class="${prescriptionData.drugInfo.quantityPerAdmin ne prescriptionData.parsedDrugInfo.quantityPerAdmin ? 'success' : ''}">
                               ${prescriptionData.drugInfo.quantityPerAdmin} |  ${prescriptionData.parsedDrugInfo.quantityPerAdmin}
                        </td>

                        <td class="${prescriptionData.drugInfo.formatAtAdmin ne prescriptionData.parsedDrugInfo.formatAtAdmin ? 'success' : ''}">
                               ${prescriptionData.drugInfo.formatAtAdmin} |  ${prescriptionData.parsedDrugInfo.formatAtAdmin}
                        </td-->

                        <td class="${prescriptionData.drugInfo.format ne prescriptionData.parsedDrugInfo.format ? 'info' : ''}">
                                ${prescriptionData.drugInfo.format}
                        </td>
                        <td class="${prescriptionData.drugInfo.format ne prescriptionData.parsedDrugInfo.format ? 'info' : ''}">
                                ${prescriptionData.parsedDrugInfo.format}
                        </td>

                        <!--td class="${prescriptionData.drugInfo.directive ne prescriptionData.parsedDrugInfo.directive ? 'warning' : ''}">
                                ${prescriptionData.drugInfo.directive}
                        </td>
                        <td class="${prescriptionData.drugInfo.directive ne prescriptionData.parsedDrugInfo.directive ? 'warning' : ''}">
                                ${prescriptionData.parsedDrugInfo.directive}
                        </td-->

                    </tr>
                </c:forEach>
            </table>

                    </div>
            --%>
        <c:forEach items="${prescriptionDataList}" var="prescriptionData" varStatus="theCount">

            <div class="panel panel-default">

                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#st${theCount.count}">
                                ${theCount.count} -
                            <span class="">
                                    ${fn:toLowerCase(prescriptionData.din)} ${fn:toLowerCase(prescriptionData.drugName)} ${fn:toLowerCase(prescriptionData.instructions)}
                            </span>
                        </a>
                    </h4>
                </div>

                <div id="st${theCount.count}" class="panel-collapse collapse">

                    <div class="panel-body">
                        <pre id="json${theCount.count}"></pre>
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <td>din</td>
                                <td>${prescriptionData.din}</td>
                                <td>${prescriptionData.parsedDrugInfo.din}</td>
                            </tr>
                            <tr>
                                <td>trademarkName</td>
                                <td>${prescriptionData.drugInfo.trademarkName }</td>
                                <td>${prescriptionData.parsedDrugInfo.trademarkName}</td>
                            </tr>
                            <tr>
                                <td>strength</td>
                                <td>${prescriptionData.drugInfo.strength}</td>
                                <td>${prescriptionData.parsedDrugInfo.strength}</td>
                            </tr>
                            <tr>
                                <td>format</td>
                                <td>${prescriptionData.drugInfo.format}</td>
                                <td>${prescriptionData.parsedDrugInfo.format}</td>
                            </tr>
                            <tr>
                                <td>genericName</td>
                                <td>${prescriptionData.drugInfo.genericName }</td>
                                <td>${prescriptionData.parsedDrugInfo.genericName}</td>
                            </tr>
                            <tr>
                                <td>genCode</td>
                                <td>${prescriptionData.drugInfo.genCode }</td>
                                <td>${prescriptionData.parsedDrugInfo.genCode}</td>
                            </tr>
                            <tr>
                                <td>multiplier</td>
                                <td>${prescriptionData.drugInfo.quantityPerAdmin }</td>
                                <td>${prescriptionData.parsedDrugInfo.quantityPerAdmin}</td>
                            </tr>
                            <tr>
                                <td>multiplierUnit</td>
                                <td>${prescriptionData.drugInfo.formatAtAdmin }</td>
                                <td>${prescriptionData.parsedDrugInfo.formatAtAdmin}</td>
                            </tr>
                            <tr>
                                <td>dose</td>
                                <td>${prescriptionData.drugInfo.dosePerAdmin }</td>
                                <td>${prescriptionData.parsedDrugInfo.dosePerAdmin}</td>
                            </tr>
                            <tr>
                                <td>multiplierUnit</td>
                                <td>${prescriptionData.drugInfo.unitAtAdmin }</td>
                                <td>${prescriptionData.parsedDrugInfo.unitAtAdmin}</td>
                            </tr>
                            <tr>
                                <td>frequency</td>
                                <td>${prescriptionData.drugInfo.frequency }</td>
                                <td>${prescriptionData.parsedDrugInfo.frequency}</td>
                            </tr>
                            <tr>
                                <td>freqCode</td>
                                <td>${prescriptionData.drugInfo.freqCode}</td>
                                <td>${prescriptionData.parsedDrugInfo.freqCode}</td>
                            </tr>
                            <tr>
                                <td>route</td>
                                <td>${prescriptionData.drugInfo.route}</td>
                                <td>${prescriptionData.parsedDrugInfo.route}</td>
                            </tr>
                            <tr>
                                <td>duration</td>
                                <td>${prescriptionData.drugInfo.duration}</td>
                                <td>${prescriptionData.parsedDrugInfo.duration}</td>
                            </tr>
                            <tr>
                                <td>repeat</td>
                                <td>${prescriptionData.drugInfo.repeat}</td>
                                <td>${prescriptionData.parsedDrugInfo.repeat}</td>
                            </tr>
                            <tr>
                                <td>prn</td>
                                <td>${prescriptionData.drugInfo.prn}</td>
                                <td>${prescriptionData.parsedDrugInfo.prn}</td>
                            </tr>
                            <tr>
                                <td>directive</td>
                                <td>${prescriptionData.drugInfo.directive}</td>
                                <td>${prescriptionData.parsedDrugInfo.directive}</td>
                            </tr>
                        </table>
                        <script>
                            document.getElementById("json${theCount.count}").innerHTML = JSON.stringify(${prescriptionData.parsedDrugInfo.jsonRs}, undefined, 2);
                            document.getElementById("json${theCount.count}").style.fontWeight = 'bold';
                        </script>
                    </div>
                </div>
            </div>
            <!--/c:if-->
        </c:forEach>
    </div>
</c:if>
