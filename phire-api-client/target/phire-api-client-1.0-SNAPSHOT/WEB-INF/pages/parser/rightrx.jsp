<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Parser Info.</title>

    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <!--script src="<c:url value="/resources/js/bootstrap.min.js" />"></script-->
    <!--script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<jsp:include page="/WEB-INF/pages/parser/navbar.jsp">
    <jsp:param name="title" value="dsq"/>
</jsp:include>

<div class="container">

    <div class="blog-header">
        <h3 class="blog-title">RightRx Parsing</h3>
        <ul class="pagination">
        </ul>
    </div>

    <c:if test="${not empty vigilSentences}">

        <hr/>
        <div class="panel-group" id="sentences">
            <c:forEach items="${vigilSentences}" var="vigilSentence" varStatus="theCount">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#st${theCount.count}">
                                <c:if test="${not vigilSentence.hasSameParsedInfo()}">
                                    <span class="label label-default">${vigilSentence.rxString}</span>
                                </c:if>
                                <c:if test="${vigilSentence.hasSameParsedInfo()}">
                                    ${vigilSentence.rxString}
                                </c:if>
                            </a>
                        </h4>
                    </div>
                    <div id="st${theCount.count}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <pre id="json${theCount.count}"></pre>
                            <script>
                                document.getElementById("json${theCount.count}").innerHTML = JSON.stringify(${vigilSentence.parsedPrescription.jsonResponse}, undefined, 2);
                                document.getElementById("json${theCount.count}").style.fontWeight = 'bold';
                            </script>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </c:if>


</div>
</body>
</html>
