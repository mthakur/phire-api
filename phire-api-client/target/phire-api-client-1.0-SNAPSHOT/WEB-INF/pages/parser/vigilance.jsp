<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Parser Info.</title>

    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <!--script src="<c:url value="/resources/js/bootstrap.min.js" />"></script-->
    <!--script src="<c:url value="/resources/js/jquery-3.1.1.min.js" />"></script-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<jsp:include page="/WEB-INF/pages/parser/navbar.jsp">
    <jsp:param name="title" value="vigil"/>
</jsp:include>

<div class="container">

    <div class="blog-header">
        <h3 class="blog-title">Vigilance Order Sentence Parsing</h3>
    </div>

    <c:if test="${not empty vigilSentences}">

        <hr/>
        <div class="panel-group" id="sentences">
            <c:forEach items="${vigilSentences}" var="vigilSentence" varStatus="theCount">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#st${theCount.count}">
                                <c:if test="${vigilSentence.parsedPrescription.parsedDrugInfo.isEmpty()}">
                                    <span class="label label-default">${vigilSentence.rxString}</span>
                                </c:if>
                                <c:if test="${not vigilSentence.parsedPrescription.parsedDrugInfo.isEmpty()}">
                                    ${vigilSentence.rxString}
                                </c:if>
                            </a>
                        </h4>
                    </div>
                    <div id="st${theCount.count}" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table table-bordered table-condensed table-striped  table-hover">
                                <tr>
                                    <td><b>Generic Name</b></td>
                                    <td>${vigilSentence.genericName}</td>
                                    <td>${vigilSentence.parsedPrescription.parsedDrugInfo.genericName}</td>
                                </tr>
                                <tr>
                                    <td><b>Gen Code</b></td>
                                    <td>${vigilSentence.genCode}</td>
                                    <td>${vigilSentence.parsedPrescription.parsedDrugInfo.genCode}</td>
                                </tr>
                                <tr>
                                    <td><b>Dose</b></td>
                                    <td>${vigilSentence.dose}</td>
                                    <td>${vigilSentence.parsedPrescription.parsedDrugInfo.dosePerAdmin}</td>
                                </tr>
                                <tr>
                                    <td><b>Dose Unit</b></td>
                                    <td>${vigilSentence.doseUnit}</td>
                                    <td>${vigilSentence.parsedPrescription.parsedDrugInfo.unitAtAdmin}</td>
                                </tr>
                                <tr>
                                    <td><b>Freq Id</b></td>
                                    <td>${vigilSentence.frequencyId}</td>
                                    <td>${vigilSentence.parsedPrescription.parsedDrugInfo.frequency}</td>
                                </tr>
                                <tr>
                                    <td><b>Prn</b></td>
                                    <td>${vigilSentence.prn}</td>
                                    <td>${vigilSentence.parsedPrescription.parsedDrugInfo.prn}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="panel-footer">${vigilSentence.parsedPrescription.jsonResponse}</div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </c:if>

</div>
</body>
</html>
