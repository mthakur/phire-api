<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Parser Info.</title>
    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/js/bootstrap.js" />" rel="stylesheet">

</head>
<body>

<jsp:include page="/WEB-INF/pages/parser/navbar.jsp">
    <jsp:param name="title" value="home"/>
</jsp:include>

<div class="container">
    <h2>PHIRE API Client</h2>
    <a href="<c:url value="/parse"/>" class="btn btn-primary" role="button">Parse Prescription</a>
    <!--a href="<c:url value="/vigil/0"/>" class="btn btn-primary" role="button">Vigilance Order Sentence</a-->
    <a href="<c:url value="/dsq/0"/>" class="btn btn-primary" role="button">DSQ Instructions</a>
</div>

</body>