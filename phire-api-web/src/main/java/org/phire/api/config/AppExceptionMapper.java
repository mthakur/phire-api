package org.phire.api.config;

import org.phire.api.parser.util.AppException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Provider
public class AppExceptionMapper implements ExceptionMapper<AppException> {

    @Override
    public Response toResponse(AppException e) {
        final Map<Integer, String> errors = new HashMap<>();
        errors.put(e.getCode().getStatusCode(), e.getMessage());
        return Response.status(e.getCode()).entity(errors).build();
    }
}
