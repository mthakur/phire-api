package org.phire.api.config;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Provider
public class ConstraintViolationMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException e) {

        final Map<String, String> errors = new HashMap<>();
        for (ConstraintViolation vi : e.getConstraintViolations()) {
            if (vi.getPropertyPath().toString().split("\\.").length > 2) {
                String propertyName = vi.getPropertyPath().toString().split("\\.")[2];
                errors.put(propertyName, vi.getMessage());
            } else {
                errors.put(Response.Status.BAD_REQUEST.getStatusCode() + "", vi.getMessage());
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).entity(errors).build();
    }
}
