package org.phire.api.config;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Provider
public class ExceptionMapper implements javax.ws.rs.ext.ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception e) {
        final Map<Integer, String> errors = new HashMap<>();
        errors.put(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), e.getMessage());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errors).build();
    }
}
