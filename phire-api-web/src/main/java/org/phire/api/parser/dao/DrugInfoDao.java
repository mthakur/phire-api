package org.phire.api.parser.dao;

import org.phire.api.parser.model.DrugInfo;

import javax.ejb.Stateless;
import java.util.List;

@Stateless(name = "drugInfoDao")
public class DrugInfoDao extends GenericHibernateDao<DrugInfo, String> {

//    public List<DrugInfoDto> fetchAll() {
//        return copy(getAll());
//    }

    private List<DrugInfo> getAll() {
        return findAll();
    }

//    private List<DrugInfoDto> copy(List<DrugInfo> drugInfos) {
//
//        if (drugInfos == null) {
//            return null;
//        }
//        List<DrugInfoDto> drugInfoDtos = new ArrayList<DrugInfoDto>();
//
//        for (DrugInfo drugInfo : drugInfos) {
//            drugInfoDtos.add(copy(drugInfo));
//        }
//        return drugInfoDtos;
//    }
//
//
//    private DrugInfoDto copy(DrugInfo drugInfo) {
//
//        if (drugInfo == null) {
//            return null;
//        }
//
//        DrugInfoDto drugInfoDto = new DrugInfoDto();
//        drugInfoDto.setId(drugInfo.getId());
//        drugInfoDto.setDin(drugInfo.getDin());
//        drugInfoDto.setBrandName(drugInfo.getBrandName());
//        drugInfoDto.setGenericName(drugInfo.getGenericName());
//        drugInfoDto.setGenCode(drugInfo.getGenCode());
//        drugInfoDto.setAtcCode(drugInfo.getAtcCode());
//        //drugInfoDto.setAhfs(drugInfo.getAhfs());
//        drugInfoDto.setStrength(drugInfo.getStrength());
//        drugInfoDto.setRoute(drugInfo.getRoute());
//        drugInfoDto.setFormat(drugInfo.getFormat());
//        drugInfoDto.setMonographId(drugInfo.getMonographId());
//        drugInfoDto.setSent(drugInfo.getSent());
//
//        return drugInfoDto;
//    }
}
