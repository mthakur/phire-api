package org.phire.api.parser.dao;

import org.phire.api.parser.model.FrequencyCodeMap;

import javax.ejb.Stateless;
import java.util.List;


@Stateless(name = "frequencyCodeMapDao")
public class FrequencyCodeMapDao extends GenericHibernateDao<FrequencyCodeMap, String> {

    public FrequencyCodeMap getParseRequesrt(String id) {
        return find(id);
    }

    public List<FrequencyCodeMap> getAll() {
        return findAll();
    }

}
