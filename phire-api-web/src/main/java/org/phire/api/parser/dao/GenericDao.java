package org.phire.api.parser.dao;

import javax.ejb.Local;
import java.io.Serializable;
import java.util.List;

@Local
public interface GenericDao<T, U extends Serializable> {

    T find(U id);

    List<T> findAll();

    T persist(T entity);

    void remove(T entity);

}
