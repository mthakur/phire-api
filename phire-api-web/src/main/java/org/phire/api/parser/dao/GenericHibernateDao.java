package org.phire.api.parser.dao;

import org.hibernate.criterion.Criterion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class GenericHibernateDao<T, U extends Serializable> implements GenericDao<T, U> {

    //private static final Logger LOG = LoggerFactory.getLogger(GenericHibernateDao.class);

    @PersistenceContext
    private EntityManager em;

    private final Class<T> clazz;

    public GenericHibernateDao() {
        if (this.getClass().getGenericSuperclass() instanceof ParameterizedType) {
            this.clazz = (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        } else {
            this.clazz = (Class) this.getClass().getGenericSuperclass();
        }
    }

    public T find(U id) {
        return em.find(getClazz(), id);
    }

    public List<T> findAll() {
        return findByCriteria();
    }

    public T persist(T entity) {
        em.persist(entity);
        return entity;
    }

    public void remove(T entity) {
        em.remove(entity);
    }

    public void setEntityManager(EntityManager em) {
        if (em == null) {
            //LOG.warn("Setting a null EntityManager in DAO {}", getClass().getName());
        }
        this.em = em;
    }

    protected Class<T> getClazz() {
        return clazz;
    }

    protected EntityManager getEntityManager() {
        return em;
    }

    @SuppressWarnings({"unchecked"})
    protected List<T> findByCriteria(Criterion... criteria) {
        Query q = this.em.createQuery("select x FROM " + this.clazz.getSimpleName() + " x");
        return q.getResultList();
    }
}
