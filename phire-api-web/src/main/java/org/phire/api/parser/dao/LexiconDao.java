package org.phire.api.parser.dao;

import org.phire.api.parser.model.Lexicon;

import javax.ejb.Stateless;
import java.util.List;

@Stateless(name = "lexiconDao")
public class LexiconDao extends GenericHibernateDao<Lexicon, String> {

    public List<Lexicon> getAll() {
        return findAll();
    }

    public List<Lexicon> getAllEabled() {
        javax.persistence.Query query = getEntityManager().createNamedQuery("findAllEnabledLexicon");
        List<Lexicon> lexicon = query.getResultList();
        return lexicon;
    }
}
