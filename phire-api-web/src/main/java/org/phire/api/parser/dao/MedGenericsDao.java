package org.phire.api.parser.dao;

import org.phire.api.parser.model.MedGenerics;

import javax.ejb.Stateless;
import java.util.List;

@Stateless(name = "genericsDao")
public class MedGenericsDao extends GenericHibernateDao<MedGenerics, String> {

    public List<MedGenerics> getAllLexicons() {
        return findAll();
    }
}
