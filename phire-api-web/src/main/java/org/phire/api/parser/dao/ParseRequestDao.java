package org.phire.api.parser.dao;

import org.phire.api.parser.model.ParseRequest;

import javax.ejb.Stateless;
import java.util.List;

@Stateless(name = "parseRequest")
public class ParseRequestDao extends GenericHibernateDao<ParseRequest, String> {

    public ParseRequest getParseRequesrt(String id) {
        return find(id);
    }

    public List<ParseRequest> getAll() {
        return findAll();
    }

}