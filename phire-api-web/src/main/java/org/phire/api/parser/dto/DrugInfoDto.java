package org.phire.api.parser.dto;

public class DrugInfoDto {

    private String id;

    private String sent;

    private String din;

    private String brandName;

    private String genericName;

    private String genCode;

    private String atcCode;

    //private String ahfs;

    private String strength;

    private String route;

    private String format;

    private String monographId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSent() {
        return sent;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

//    public String getAhfs() {
//        return ahfs;
//    }
//
//    public void setAhfs(String ahfs) {
//        this.ahfs = ahfs;
//    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getMonographId() {
        return monographId;
    }

    public void setMonographId(String monographId) {
        this.monographId = monographId;
    }

    public String getAtcCode() {
        return atcCode;
    }

    public void setAtcCode(String atcCode) {
        this.atcCode = atcCode;
    }

    @Override
    public String toString() {
        return "DrugInfoDto{" +
                "id='" + id + '\'' +
                ", din='" + din + '\'' +
                ", sent='" + sent + '\'' +
                ", brandName='" + brandName + '\'' +
                ", genericName='" + genericName + '\'' +
                ", genCode='" + genCode + '\'' +
                ", atcCode='" + atcCode + '\'' +
                // ", ahfs='" + ahfs + '\'' +
                ", strength='" + strength + '\'' +
                ", route='" + route + '\'' +
                ", format='" + format + '\'' +
                ", monographId='" + monographId + '\'' +
                '}';
    }
}
