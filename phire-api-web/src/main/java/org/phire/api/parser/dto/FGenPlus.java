package org.phire.api.parser.dto;

public class FGenPlus {

    private String ahfs;

//    private String atcCode;
//
    private String genericNameEn;

    private String genericNameFr;

    private String genericNameEnSimple;

    private String genericNameFrSimple;

    public String getGenericNameEnSimple() {
        return genericNameEnSimple;
    }

    public void setGenericNameEnSimple(String genericNameEnSimple) {
        this.genericNameEnSimple = genericNameEnSimple;
    }

    public String getGenericNameFrSimple() {
        return genericNameFrSimple;
    }

    public void setGenericNameFrSimple(String genericNameFrSimple) {
        this.genericNameFrSimple = genericNameFrSimple;
    }

    public String getAhfs() {
        return ahfs;
    }

    public void setAhfs(String ahfs) {
        this.ahfs = ahfs;
    }

    public String getGenericNameEn() {
        return genericNameEn;
    }

    public void setGenericNameEn(String genericNameEn) {
        this.genericNameEn = genericNameEn;
    }

    public String getGenericNameFr() {
        return genericNameFr;
    }

    public void setGenericNameFr(String genericNameFr) {
        this.genericNameFr = genericNameFr;
    }

//    public String getAtcCode() {
//        return atcCode;
//    }
//
//    public void setAtcCode(String atcCode) {
//        this.atcCode = atcCode;
//    }
//
    @Override
    public String toString() {
        return "FGenPlus{" +
                "ahfs='" + ahfs + '\'' +
                ", genericNameEn='" + genericNameEn + '\'' +
                ", genericNameFr='" + genericNameFr + '\'' +
                ", genericNameEnSimple='" + genericNameEnSimple + '\'' +
                ", genericNameFrSimple='" + genericNameFrSimple + '\'' +
                '}';
    }
}
