package org.phire.api.parser.dto;

public class GenerxPlusDat {

    private String activity;
    private String cerxFormCode;
    private String cerxRouteCode;
    private String codegen;
    private String dfltFrequency;
    private String dfltRouteOfAdministration;
    private String dosageValidated;
    private String doseUnits;
    private String doseUnitsPerDoseformunit;
    private String formatAng;
    private String formatFra;
    private String genNameAng;
    private String genNameFra;
    private String lcFormatAng;
    private String lcFormatFra;
    private String lcGenNameAng;
    private String lcGenNameFra;
    private String lcStrengthAng;
    private String lcStrengthFra;
    private String strengthAng;
    private String strengthFra;
    private String uniqueIdentifier;

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getCerxFormCode() {
        return cerxFormCode;
    }

    public void setCerxFormCode(String cerxFormCode) {
        this.cerxFormCode = cerxFormCode;
    }

    public String getCerxRouteCode() {
        return cerxRouteCode;
    }

    public void setCerxRouteCode(String cerxRouteCode) {
        this.cerxRouteCode = cerxRouteCode;
    }

    public String getCodegen() {
        return codegen;
    }

    public void setCodegen(String codegen) {
        this.codegen = codegen;
    }

    public String getDfltFrequency() {
        return dfltFrequency;
    }

    public void setDfltFrequency(String dfltFrequency) {
        this.dfltFrequency = dfltFrequency;
    }

    public String getDfltRouteOfAdministration() {
        return dfltRouteOfAdministration;
    }

    public void setDfltRouteOfAdministration(String dfltRouteOfAdministration) {
        this.dfltRouteOfAdministration = dfltRouteOfAdministration;
    }

    public String getDosageValidated() {
        return dosageValidated;
    }

    public void setDosageValidated(String dosageValidated) {
        this.dosageValidated = dosageValidated;
    }

    public String getDoseUnits() {
        return doseUnits;
    }

    public void setDoseUnits(String doseUnits) {
        this.doseUnits = doseUnits;
    }

    public String getDoseUnitsPerDoseformunit() {
        return doseUnitsPerDoseformunit;
    }

    public void setDoseUnitsPerDoseformunit(String doseUnitsPerDoseformunit) {
        this.doseUnitsPerDoseformunit = doseUnitsPerDoseformunit;
    }

    public String getFormatAng() {
        return formatAng;
    }

    public void setFormatAng(String formatAng) {
        this.formatAng = formatAng;
    }

    public String getFormatFra() {
        return formatFra;
    }

    public void setFormatFra(String formatFra) {
        this.formatFra = formatFra;
    }

    public String getGenNameAng() {
        return genNameAng;
    }

    public void setGenNameAng(String genNameAng) {
        this.genNameAng = genNameAng;
    }

    public String getGenNameFra() {
        return genNameFra;
    }

    public void setGenNameFra(String genNameFra) {
        this.genNameFra = genNameFra;
    }

    public String getLcFormatAng() {
        return lcFormatAng;
    }

    public void setLcFormatAng(String lcFormatAng) {
        this.lcFormatAng = lcFormatAng;
    }

    public String getLcFormatFra() {
        return lcFormatFra;
    }

    public void setLcFormatFra(String lcFormatFra) {
        this.lcFormatFra = lcFormatFra;
    }

    public String getLcGenNameAng() {
        return lcGenNameAng;
    }

    public void setLcGenNameAng(String lcGenNameAng) {
        this.lcGenNameAng = lcGenNameAng;
    }

    public String getLcGenNameFra() {
        return lcGenNameFra;
    }

    public void setLcGenNameFra(String lcGenNameFra) {
        this.lcGenNameFra = lcGenNameFra;
    }

    public String getLcStrengthAng() {
        return lcStrengthAng;
    }

    public void setLcStrengthAng(String lcStrengthAng) {
        this.lcStrengthAng = lcStrengthAng;
    }

    public String getLcStrengthFra() {
        return lcStrengthFra;
    }

    public void setLcStrengthFra(String lcStrengthFra) {
        this.lcStrengthFra = lcStrengthFra;
    }

    public String getStrengthAng() {
        return strengthAng;
    }

    public void setStrengthAng(String strengthAng) {
        this.strengthAng = strengthAng;
    }

    public String getStrengthFra() {
        return strengthFra;
    }

    public void setStrengthFra(String strengthFra) {
        this.strengthFra = strengthFra;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public void setUniqueIdentifier(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    @Override
    public String toString() {
        return "GenerxPlusDat{" +
                "activity='" + activity + '\'' +
                ", cerxFormCode='" + cerxFormCode + '\'' +
                ", cerxRouteCode='" + cerxRouteCode + '\'' +
                ", codegen='" + codegen + '\'' +
                ", dfltFrequency='" + dfltFrequency + '\'' +
                ", dfltRouteOfAdministration='" + dfltRouteOfAdministration + '\'' +
                ", dosageValidated='" + dosageValidated + '\'' +
                ", doseUnits='" + doseUnits + '\'' +
                ", doseUnitsPerDoseformunit='" + doseUnitsPerDoseformunit + '\'' +
                ", formatAng='" + formatAng + '\'' +
                ", formatFra='" + formatFra + '\'' +
                ", genNameAng='" + genNameAng + '\'' +
                ", genNameFra='" + genNameFra + '\'' +
                ", lcFormatAng='" + lcFormatAng + '\'' +
                ", lcFormatFra='" + lcFormatFra + '\'' +
                ", lcGenNameAng='" + lcGenNameAng + '\'' +
                ", lcGenNameFra='" + lcGenNameFra + '\'' +
                ", lcStrengthAng='" + lcStrengthAng + '\'' +
                ", lcStrengthFra='" + lcStrengthFra + '\'' +
                ", strengthAng='" + strengthAng + '\'' +
                ", strengthFra='" + strengthFra + '\'' +
                ", uniqueIdentifier='" + uniqueIdentifier + '\'' +
                '}';
    }
}
