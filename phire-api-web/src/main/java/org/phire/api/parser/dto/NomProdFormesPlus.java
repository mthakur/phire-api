package org.phire.api.parser.dto;

public class NomProdFormesPlus {

    private String cerxDefltRouteElectrTrans;
    private String descriptionEnd;
    private String descriptionFre;
    private String engAbbrSigUnit;
    private String formEng;
    private String formFre;
    private String freAbbrSigUnit;
    private String rxUnitEngTermPlural;
    private String rxUnitEngTermSungular;
    private String rxUnitFreTermPlural;
    private String rxUnitFreTermSungular;
    private String rxUnitGenreFreTerm;
    private String sigEngTermPlural;
    private String sigEngTermSungular;
    private String sigFreTermPlural;
    private String sigFreTermSungular;
    private String sigGenreFreTerm;
    private String uidSigUnits;
    private String vigiDefltRouteDosValidtn;

    public String getCerxDefltRouteElectrTrans() {
        return cerxDefltRouteElectrTrans;
    }

    public void setCerxDefltRouteElectrTrans(String cerxDefltRouteElectrTrans) {
        this.cerxDefltRouteElectrTrans = cerxDefltRouteElectrTrans;
    }

    public String getDescriptionEnd() {
        return descriptionEnd;
    }

    public void setDescriptionEnd(String descriptionEnd) {
        this.descriptionEnd = descriptionEnd;
    }

    public String getDescriptionFre() {
        return descriptionFre;
    }

    public void setDescriptionFre(String descriptionFre) {
        this.descriptionFre = descriptionFre;
    }

    public String getEngAbbrSigUnit() {
        return engAbbrSigUnit;
    }

    public void setEngAbbrSigUnit(String engAbbrSigUnit) {
        this.engAbbrSigUnit = engAbbrSigUnit;
    }

    public String getFormEng() {
        return formEng;
    }

    public void setFormEng(String formEng) {
        this.formEng = formEng;
    }

    public String getFormFre() {
        return formFre;
    }

    public void setFormFre(String formFre) {
        this.formFre = formFre;
    }

    public String getFreAbbrSigUnit() {
        return freAbbrSigUnit;
    }

    public void setFreAbbrSigUnit(String freAbbrSigUnit) {
        this.freAbbrSigUnit = freAbbrSigUnit;
    }

    public String getRxUnitEngTermPlural() {
        return rxUnitEngTermPlural;
    }

    public void setRxUnitEngTermPlural(String rxUnitEngTermPlural) {
        this.rxUnitEngTermPlural = rxUnitEngTermPlural;
    }

    public String getRxUnitEngTermSungular() {
        return rxUnitEngTermSungular;
    }

    public void setRxUnitEngTermSungular(String rxUnitEngTermSungular) {
        this.rxUnitEngTermSungular = rxUnitEngTermSungular;
    }

    public String getRxUnitFreTermPlural() {
        return rxUnitFreTermPlural;
    }

    public void setRxUnitFreTermPlural(String rxUnitFreTermPlural) {
        this.rxUnitFreTermPlural = rxUnitFreTermPlural;
    }

    public String getRxUnitFreTermSungular() {
        return rxUnitFreTermSungular;
    }

    public void setRxUnitFreTermSungular(String rxUnitFreTermSungular) {
        this.rxUnitFreTermSungular = rxUnitFreTermSungular;
    }

    public String getRxUnitGenreFreTerm() {
        return rxUnitGenreFreTerm;
    }

    public void setRxUnitGenreFreTerm(String rxUnitGenreFreTerm) {
        this.rxUnitGenreFreTerm = rxUnitGenreFreTerm;
    }

    public String getSigEngTermPlural() {
        return sigEngTermPlural;
    }

    public void setSigEngTermPlural(String sigEngTermPlural) {
        this.sigEngTermPlural = sigEngTermPlural;
    }

    public String getSigEngTermSungular() {
        return sigEngTermSungular;
    }

    public void setSigEngTermSungular(String sigEngTermSungular) {
        this.sigEngTermSungular = sigEngTermSungular;
    }

    public String getSigFreTermPlural() {
        return sigFreTermPlural;
    }

    public void setSigFreTermPlural(String sigFreTermPlural) {
        this.sigFreTermPlural = sigFreTermPlural;
    }

    public String getSigFreTermSungular() {
        return sigFreTermSungular;
    }

    public void setSigFreTermSungular(String sigFreTermSungular) {
        this.sigFreTermSungular = sigFreTermSungular;
    }

    public String getSigGenreFreTerm() {
        return sigGenreFreTerm;
    }

    public void setSigGenreFreTerm(String sigGenreFreTerm) {
        this.sigGenreFreTerm = sigGenreFreTerm;
    }

    public String getUidSigUnits() {
        return uidSigUnits;
    }

    public void setUidSigUnits(String uidSigUnits) {
        this.uidSigUnits = uidSigUnits;
    }

    public String getVigiDefltRouteDosValidtn() {
        return vigiDefltRouteDosValidtn;
    }

    public void setVigiDefltRouteDosValidtn(String vigiDefltRouteDosValidtn) {
        this.vigiDefltRouteDosValidtn = vigiDefltRouteDosValidtn;
    }

    @Override
    public String toString() {
        return "NomProdFormesPlus{" +
                "cerxDefltRouteElectrTrans='" + cerxDefltRouteElectrTrans + '\'' +
                ", descriptionEnd='" + descriptionEnd + '\'' +
                ", descriptionFre='" + descriptionFre + '\'' +
                ", engAbbrSigUnit='" + engAbbrSigUnit + '\'' +
                ", formEng='" + formEng + '\'' +
                ", formFre='" + formFre + '\'' +
                ", freAbbrSigUnit='" + freAbbrSigUnit + '\'' +
                ", rxUnitEngTermPlural='" + rxUnitEngTermPlural + '\'' +
                ", rxUnitEngTermSungular='" + rxUnitEngTermSungular + '\'' +
                ", rxUnitFreTermPlural='" + rxUnitFreTermPlural + '\'' +
                ", rxUnitFreTermSungular='" + rxUnitFreTermSungular + '\'' +
                ", rxUnitGenreFreTerm='" + rxUnitGenreFreTerm + '\'' +
                ", sigEngTermPlural='" + sigEngTermPlural + '\'' +
                ", sigEngTermSungular='" + sigEngTermSungular + '\'' +
                ", sigFreTermPlural='" + sigFreTermPlural + '\'' +
                ", sigFreTermSungular='" + sigFreTermSungular + '\'' +
                ", sigGenreFreTerm='" + sigGenreFreTerm + '\'' +
                ", uidSigUnits='" + uidSigUnits + '\'' +
                ", vigiDefltRouteDosValidtn='" + vigiDefltRouteDosValidtn + '\'' +
                '}';
    }
}
