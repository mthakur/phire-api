package org.phire.api.parser.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class ParserInput implements Serializable {

    @Digits(integer = 8, fraction = 0, message = "Invalid value for din.")
    private String din;

    private String drugName;

    @NotNull
    private String instruction;

    private String source;

    public ParserInput(String din, String drugName, String instruction, String source) {
        this.din = din;
        this.drugName = drugName;
        this.instruction = instruction;
        this.source = source;
    }

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getPrescription() {
        return drugName + " " + instruction;
    }
}
