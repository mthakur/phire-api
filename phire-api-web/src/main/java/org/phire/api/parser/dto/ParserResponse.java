package org.phire.api.parser.dto;

import java.io.Serializable;

public class ParserResponse implements Serializable {

    private String din;
    private String trademarkName;
    private String genericName;
    private String genCode;
    private String atcCode;
    private String strength;
    private String format;
    private String quantityPerAdmin;
    private String formatAtAdmin;
    private String dosePerAdmin;
    private String unitAtAdmin;
    private String frequency;
    private String freqCode;
    private String route;
    private String duration;
    private String repeat;
    private String prn;
    private String directive;

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        if (strength != null) {
            this.strength = strength.replaceAll(" ", "");
        } else {
            this.strength = strength;
        }
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getTrademarkName() {
        return trademarkName;
    }

    public void setTrademarkName(String trademarkName) {
        this.trademarkName = trademarkName;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getDirective() {
        return directive;
    }

    public void setDirective(String directive) {
        this.directive = directive;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

    public String getFreqCode() {
        return freqCode;
    }

    public void setFreqCode(String freqCode) {
        this.freqCode = freqCode;
    }

    public String getQuantityPerAdmin() {
        return quantityPerAdmin;
    }

    public void setQuantityPerAdmin(String quantityPerAdmin) {
        this.quantityPerAdmin = quantityPerAdmin;
    }

    public String getFormatAtAdmin() {
        return formatAtAdmin;
    }

    public void setFormatAtAdmin(String formatAtAdmin) {
        this.formatAtAdmin = formatAtAdmin;
    }

    public String getUnitAtAdmin() {
        return unitAtAdmin;
    }

    public void setUnitAtAdmin(String unitAtAdmin) {
        this.unitAtAdmin = unitAtAdmin;
    }

    public String getDosePerAdmin() {
        return dosePerAdmin;
    }

    public void setDosePerAdmin(String dosePerAdmin) {
        this.dosePerAdmin = dosePerAdmin;
    }

    public void setAtcCode(String atcCode) {
        this.atcCode = atcCode;
    }

    public Boolean hasMissingRequiredInfo() {
        return getGenericName() == null || (getQuantityPerAdmin() == null && getFormatAtAdmin() == null) || getFrequency() == null;
    }

    @Override
    public String toString() {
        return "ParserResponse{" +
                "din='" + din + '\'' +
                ", trademarkName='" + trademarkName + '\'' +
                ", genericName='" + genericName + '\'' +
                ", genCode='" + genCode + '\'' +
                ", atcCode='" + atcCode + '\'' +
                ", strength='" + strength + '\'' +
                ", format='" + format + '\'' +
                ", quantityPerAdmin='" + quantityPerAdmin + '\'' +
                ", formatAtAdmin='" + formatAtAdmin + '\'' +
                ", dosePerAdmin='" + dosePerAdmin + '\'' +
                ", unitAtAdmin='" + unitAtAdmin + '\'' +
                ", frequency='" + frequency + '\'' +
                ", freqCode='" + freqCode + '\'' +
                ", route='" + route + '\'' +
                ", duration='" + duration + '\'' +
                ", repeat='" + repeat + '\'' +
                ", prn='" + prn + '\'' +
                ", directive='" + directive + '\'' +
                '}';
    }
}
