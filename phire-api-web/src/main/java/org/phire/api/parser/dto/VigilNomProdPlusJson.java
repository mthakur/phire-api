package org.phire.api.parser.dto;

public class VigilNomProdPlusJson {

    private String din;
    private String codegen;
    private String capProdNameFr;
    private String capStrengthFr;
    private String capProdNameEn;
    private String capStrengthEn;
    private String fabricant;
    private String discontinued;
    private String legalStatus;
    private NomProdFormesPlus nomprodformesplus;
    private GenerxPlusDat generx;
    private FGenPlus fgenplus;

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getCodegen() {
        return codegen;
    }

    public void setCodegen(String codegen) {
        this.codegen = codegen;
    }

    public String getCapProdNameFr() {
        return capProdNameFr;
    }

    public void setCapProdNameFr(String capProdNameFr) {
        this.capProdNameFr = capProdNameFr;
    }

    public String getCapStrengthFr() {
        return capStrengthFr;
    }

    public void setCapStrengthFr(String capStrengthFr) {
        this.capStrengthFr = capStrengthFr;
    }

    public String getCapProdNameEn() {
        return capProdNameEn;
    }

    public void setCapProdNameEn(String capProdNameEn) {
        this.capProdNameEn = capProdNameEn;
    }

    public String getCapStrengthEn() {
        return capStrengthEn;
    }

    public void setCapStrengthEn(String capStrengthEn) {
        this.capStrengthEn = capStrengthEn;
    }

    public String getFabricant() {
        return fabricant;
    }

    public void setFabricant(String fabricant) {
        this.fabricant = fabricant;
    }

    public String getDiscontinued() {
        return discontinued;
    }

    public void setDiscontinued(String discontinued) {
        this.discontinued = discontinued;
    }

    public String getLegalStatus() {
        return legalStatus;
    }

    public void setLegalStatus(String legalStatus) {
        this.legalStatus = legalStatus;
    }

    public FGenPlus getFgenplus() {
        return fgenplus;
    }

    public void setFgenplus(FGenPlus fgenplus) {
        this.fgenplus = fgenplus;
    }

    public NomProdFormesPlus getNomProdFormesPlus() {
        return nomprodformesplus;
    }

    public void setNomProdFormesPlus(NomProdFormesPlus nomprodformesplus) {
        this.nomprodformesplus = nomprodformesplus;
    }

    public GenerxPlusDat getGenerx() {
        return generx;
    }

    public void setGenerx(GenerxPlusDat generxPlusDat) {
        this.generx = generxPlusDat;
    }

    @Override
    public String toString() {
        return "VigilNomProdPlusJson{" +
                "din='" + din + '\'' +
                ", codegen='" + codegen + '\'' +
                ", capProdNameFr='" + capProdNameFr + '\'' +
                ", capStrengthFr='" + capStrengthFr + '\'' +
                ", capProdNameEn='" + capProdNameEn + '\'' +
                ", capStrengthEn='" + capStrengthEn + '\'' +
                ", fabricant='" + fabricant + '\'' +
                ", discontinued='" + discontinued + '\'' +
                ", legalStatus='" + legalStatus + '\'' +
                ", nomprodformesplus=" + nomprodformesplus +
                ", generx=" + generx +
                ", fgenplus=" + fgenplus +
                '}';
    }
}
