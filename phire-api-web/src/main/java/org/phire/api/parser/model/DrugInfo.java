package org.phire.api.parser.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "API_DRUG_INFO")

public class DrugInfo implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "ID")
    private String id;

    @Column(name = "SENT")
    private String sent;

    @Column(name = "DIN")
    private String din;

    @Column(name = "BRAND_NAME")
    private String brandName;

    @Column(name = "GENERIC_NAME")
    private String genericName;

    @Column(name = "GEN_CODE")
    private String genCode;

    @Column(name = "ATC_CODE")
    private String atcCode;

    //@Column(name = "AHFS")
    //private String ahfs;

    @Column(name = "STRENGTH")
    private String strength;

    @Column(name = "ROUTE")
    private String route;

    @Column(name = "FORMAT")
    private String format;

    @Column(name = "MONOGRAPH_ID")
    private String monographId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSent() {
        return sent;
    }

    public void setSent(String sent) {
        this.sent = sent;
    }

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

//    public String getAhfs() {
//        return ahfs;
//    }
//
//    public void setAhfs(String ahfs) {
//        this.ahfs = ahfs;
//    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getMonographId() {
        return monographId;
    }

    public void setMonographId(String monographId) {
        this.monographId = monographId;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getAtcCode() {
        return atcCode;
    }

    public void setAtcCode(String atcCode) {
        this.atcCode = atcCode;
    }
}
