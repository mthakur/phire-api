package org.phire.api.parser.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "API_FREQ_CODE_MAP")

public class FrequencyCodeMap implements Serializable {

    @Id
    @Column(name = "NAME")
    String name;

    @Column(name = "CODE")
    String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
