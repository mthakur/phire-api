package org.phire.api.parser.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "API_LEXICON")
@NamedQueries(
        {
                @NamedQuery(
                        name = "findAllEnabledLexicon",
                        query = "from Lexicon l where l.enabled = true"
                )
        }
)

public class Lexicon implements Serializable {

    @Id
    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "TYPE", nullable = false)
    private String type;

    @Column(name = "ENABLED_FLAG", nullable = false)
    private Boolean enabled;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
