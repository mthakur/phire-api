package org.phire.api.parser.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "GENERIC_CODES")
public class MedGenerics implements Serializable {

    @Id
    @Column(name = "GENERIC_NAME", nullable = false)
    private String genericName;

    @Column(name = "GENERIC_CODE", nullable = false)
    private String getnCode;

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getGetnCode() {
        return getnCode;
    }

    public void setGetnCode(String getnCode) {
        this.getnCode = getnCode;
    }
}
