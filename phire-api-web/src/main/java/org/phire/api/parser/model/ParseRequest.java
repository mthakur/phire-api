package org.phire.api.parser.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

@Entity
@Table(name = "API_PARSE_RQ")
public class ParseRequest implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "ID")
    private String id;

    @Column(name = "DIN")
    private String din;

    @Column(name = "DRUG_NAME")
    private String drugName;

    @Column(name = "INSTRUCTIONS", nullable = false)
    private String instruction;

    @Column(name = "SOURCE")
    private String source;

    @Column(name = "CREATION_TIMESTAMP")
    private Calendar creationTimeStamp;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "PARSE_RS_ID", nullable = false)
    private ParseResponse parseResponse;

    public String getId() {
        return id;
    }

    public void setId(String hashId) {
        this.id = hashId;
    }

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Calendar getCreationTimeStamp() {
        return creationTimeStamp;
    }

    public void setCreationTimeStamp(Calendar creationTimeStamp) {
        this.creationTimeStamp = creationTimeStamp;
    }

    public ParseResponse getParseResponse() {
        return parseResponse;
    }

    public void setParseResponse(ParseResponse parseResponse) {
        this.parseResponse = parseResponse;
    }

}
