package org.phire.api.parser.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;

@Entity
@Table(name = "API_PARSE_RS")
public class ParseResponse implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "ID")
    private String id;
    @Column(name = "DIN")
    private String din;
    @Column(name = "TRADEMARK_NAME")
    private String trademarkName;
    @Column(name = "GENERIC_NAME")
    private String genericName;
    @Column(name = "GEN_CODE")
    private String genCode;
    @Column(name = "STRENGTH")
    private String strength;
//    @Column(name = "STRENGTH_UNIT")
//    private String strengthUnit;
    @Column(name = "FORMAT")
    private String format;
    @Column(name = "DOSE_PER_ADMIN")
    private String dosePerAdmin;
    @Column(name = "UNIT_AT_ADMIN")
    private String unitAtAdmin;
    @Column(name = "QTY_PER_ADMIN")
    private String qtyAtAdmin;
    @Column(name = "FORMAT_AT_ADMIN")
    private String formatAtAdmin;
    @Column(name = "FREQUENCY")
    private String frequency;
    @Column(name = "FREQUENCY_CODE")
    private String frequencyCode;
    @Column(name = "ROUTE")
    private String route;
    @Column(name = "DIRECTIVE")
    private String directive;
    @Column(name = "DURATION")
    private String duration;
    @Column(name = "REPEAT")
    private String repeat;
    @Column(name = "PRN")
    private String prn;
    @Column(name = "WARNING")
    private String warning;
    @Column(name = "ERROR")
    private String error;
    @Column(name = "CREATION_TIMESTAMP")
    private Calendar creationTimeStamp;

    public String getID() {
        return id;
    }

    public void setID(String hashId) {
        this.id = hashId;
    }

    public String getDin() {
        return din;
    }

    public void setDin(String din) {
        this.din = din;
    }

    public String getTrademarkName() {
        return trademarkName;
    }

    public void setTrademarkName(String trademarkName) {
        this.trademarkName = trademarkName;
    }

    public String getGenericName() {
        return genericName;
    }

    public void setGenericName(String genericName) {
        this.genericName = genericName;
    }

    public String getGenCode() {
        return genCode;
    }

    public void setGenCode(String genCode) {
        this.genCode = genCode;
    }

    public String getStrength() {
        return strength;
    }

    public void setStrength(String strength) {
        this.strength = strength;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getDirective() {
        return directive;
    }

    public void setDirective(String directive) {
        this.directive = directive;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Calendar getCreationTimeStamp() {
        return creationTimeStamp;
    }

    public void setCreationTimeStamp(Calendar creationTimeStamp) {
        this.creationTimeStamp = creationTimeStamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDosePerAdmin() {
        return dosePerAdmin;
    }

    public void setDosePerAdmin(String dosePerAdmin) {
        this.dosePerAdmin = dosePerAdmin;
    }

    public String getUnitAtAdmin() {
        return unitAtAdmin;
    }

    public void setUnitAtAdmin(String unitAtAdmin) {
        this.unitAtAdmin = unitAtAdmin;
    }

    public String getQtyAtAdmin() {
        return qtyAtAdmin;
    }

    public void setQtyAtAdmin(String qtyAtAdmin) {
        this.qtyAtAdmin = qtyAtAdmin;
    }

    public String getFormatAtAdmin() {
        return formatAtAdmin;
    }

    public void setFormatAtAdmin(String formatAtAdmin) {
        this.formatAtAdmin = formatAtAdmin;
    }

    public String getFrequencyCode() {
        return frequencyCode;
    }

    public void setFrequencyCode(String frequencyCode) {
        this.frequencyCode = frequencyCode;
    }
}
