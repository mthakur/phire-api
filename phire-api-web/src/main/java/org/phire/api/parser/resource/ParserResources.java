package org.phire.api.parser.resource;

import org.phire.api.parser.dto.ParserInput;
import org.phire.api.parser.dto.ParserResponse;
import org.phire.api.parser.service.DrugInfoService;
import org.phire.api.parser.service.ParserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("parse")
public class ParserResources {

    private static final Logger LOG = LoggerFactory.getLogger(ParserResources.class);

    @Inject
    ParserService parserService;

    @Inject
    DrugInfoService drugInfoService;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response parse() {
        return Response.ok("Success !!!").build();
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/{prescription}")
    public Response parse(@PathParam("prescription") String prescription) {
        LOG.debug("parsePrescriptionGet : " + prescription);
        return parseInput(new ParserInput(null, null, prescription, null));
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    public Response parse(//@Digits(integer = 8, fraction = 0, message = "{din.invalid}")
                          @FormParam("din") String din,
                          @FormParam("drugName") String drugName,
                          @NotNull(message = "{instruction.null}") @FormParam("instructions") String instructions,
                          @FormParam("source") String source) {

        LOG.debug("Input Parameters : DIN : " + din + ", Drug Name : " + drugName + ", Instructions : " + instructions);
        return parseInput(new ParserInput(din, drugName, instructions, source));
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response parse(@Valid ParserInput parserRequest) {
        return parseInput(parserRequest);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("/vws/{din}")
    public String callVigilanceService(@PathParam("din") String din) {
        return drugInfoService.search(din);
    }

    private Response parseInput(ParserInput parserInput) {
        LOG.debug(parserInput.toString());
        ParserResponse parserResponse = parserService.parse(parserInput);
        if (parserResponse == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.ok(parserResponse).build();
    }
}
