package org.phire.api.parser.service;

import org.apache.commons.lang3.StringUtils;
import org.phire.api.parser.dao.DrugInfoDao;
import org.phire.api.parser.dao.FrequencyCodeMapDao;
import org.phire.api.parser.dao.LexiconDao;
import org.phire.api.parser.dto.DrugInfoDto;
import org.phire.api.parser.model.DrugInfo;
import org.phire.api.parser.model.FrequencyCodeMap;
import org.phire.api.parser.util.Lexicon;
import org.phire.api.parser.util.medex.Encoder;
import org.phire.api.parser.util.medex.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Singleton
@Startup
public class ApiCacheService {

    private static final Logger LOG = LoggerFactory.getLogger(ApiCacheService.class);

    @Inject
    private LexiconDao lexiconDao;

    @Inject
    private DrugInfoDao drugInfoDao;

    @Inject
    private FrequencyCodeMapDao frequencyCodeMapDao;

    private final static String BRAND_GENERIC_FILE = "/brand_generic.cfg";
    private final static String NORM_FILE = "/norm.cfg";
    private final static String CODE_FILE = "code.cfg";
    private final static String VIGILANCE_GENCODE_FILE = "/vigil-gen-codes.txt";
    private final static String LEXICON_FILE = "lexicon.cfg";
    //private final static String GRAMMER_FILE = "grammar.txt";

    private HashMap<String, ArrayList<String>> rxNormMap;
    private HashMap<String, ArrayList<String>> rxCodeMap;
    private HashMap<String, ArrayList<DrugInfoDto>> rxCodeMapObj;
    private HashMap<String, HashMap<String, Integer>> rxFormMap;
    private HashMap<String, String> normalizationMap;
    private HashMap<String, String> rxGenericMap;
    private HashMap<String, String> rxVigilGeneric;
    private HashMap<String, String> frequencyMap;
    private Lexicon lex;
    private Encoder coder;
    private Util utility;

    @PostConstruct
    private void init() {

        try {

            LOG.info("Initializing cache....");
            long startTime = System.currentTimeMillis();
            rxNormMap = new HashMap<>();
            rxCodeMap = new HashMap<>(60);
            rxCodeMapObj = new HashMap<>(60);
            rxFormMap = new HashMap<>();
            rxGenericMap = new HashMap<>();
            frequencyMap = new HashMap<>();
            rxVigilGeneric = new HashMap<>();
            normalizationMap = new HashMap<>();
            utility = new Util(normalizationMap);
            coder = new Encoder(rxCodeMap, rxCodeMapObj, normalizationMap, rxGenericMap);

            // loading norm file
            loadNormMapping();

            // loading generics for
            loadVigilGeneric();

            // load brand generic mapping
            loadBrandGenericMapping();

            // loading frequency mapping
            loadFrequencyMapping();

            // loading standard rx
            loadNewRxCodingFrom();
            // loading lexicon data
            lex = new Lexicon(lexiconDao.getAllEabled());

            //lex = new Lexicon(this.getClass().getClassLoader().getResource(LEXICON_FILE).getFile());
            //loadRXCoding();
            LOG.info("Caching finished : Time Taken : " + (System.currentTimeMillis() - startTime) + " ms");
        } catch (Exception e) {
            LOG.error("Exception while initializing cache : " + e.getMessage());
        }
    }

    private void loadNewRxCodingFrom() {

        List<DrugInfo> drugInfoList = drugInfoDao.findAll();
        String genericName, brand;

        for (DrugInfo drugInfo : drugInfoList) {
            genericName = drugInfo.getGenericName() == null ? "NULL" : drugInfo.getGenericName();
            brand = drugInfo.getGenericName() == null ? "NULL" : drugInfo.getBrandName();

            if (StringUtils.isNoneBlank(genericName)) {
                genericName = utility.normalizeDrugName(genericName);
            }
            addToMapObject(genericName, drugInfo);
            for (String word : genericName.split(StringUtils.SPACE)) {
                addToMapObject(word, drugInfo);
            }
            addToMapObject(brand, drugInfo);
        }
        LOG.info("Loaded NewRxCodingForm : " + rxCodeMapObj.size());
    }

    private void addToMapObject(String key, DrugInfo drugInfoDto) {

        if (!rxCodeMapObj.containsKey(key)) {
            ArrayList<DrugInfoDto> newDrugCodes = new ArrayList<DrugInfoDto>();
            newDrugCodes.add(copy(drugInfoDto));
            rxCodeMapObj.put(key, newDrugCodes);

        } else {
            ArrayList<DrugInfoDto> drugCodes = rxCodeMapObj.get(key);
            drugCodes.add(copy(drugInfoDto));
            rxCodeMapObj.put(key, drugCodes);
        }
    }

    private void loadBrandGenericMapping() {

        try {

            InputStream inputStream = this.getClass().getResourceAsStream(BRAND_GENERIC_FILE);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(inputStreamReader);
            String strLine;

            while ((strLine = br.readLine()) != null) {
                String[] strLine_split = strLine.split("\t");

                if (strLine_split.length == 2) {
                    String brand = strLine_split[1].toLowerCase();
                    String generic = strLine_split[0].toLowerCase();

                    if (!rxNormMap.containsKey(brand)) {
                        ArrayList<String> newList = new ArrayList<String>();
                        newList.add(generic);
                        rxNormMap.put(brand, newList);

                    } else {
                        ArrayList<String> generic_list = rxNormMap.get(brand);
                        generic_list.add(generic);
                        rxNormMap.put(brand, generic_list);
                    }
                }
            }
            br.close();
            LOG.info("Load Brand Generic Mapping : " + rxNormMap.size());
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Error while loading rxNorm : " + BRAND_GENERIC_FILE + "\t" + e.getMessage());
        }
    }

    private void loadNormMapping() {

        try {

            InputStream inputStream = this.getClass().getResourceAsStream(NORM_FILE);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));

            String strLine;
            while ((strLine = br.readLine()) != null) {
                String items[] = strLine.split("\t");
                this.normalizationMap.put(items[0], items[1]);
            }
            LOG.info("Norm Mapping Loaded : " + this.normalizationMap.size());
        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Error while loading norm_file " + NORM_FILE + "\t" + e.getMessage());
        }
    }

    private void loadVigilGeneric() {

        try {

            InputStream inputStream = this.getClass().getResourceAsStream(VIGILANCE_GENCODE_FILE);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String strLine;

            while ((strLine = br.readLine()) != null) {
                String[] lineSplit = strLine.split("\t");

                if (lineSplit.length == 2) {
                    String genericName = lineSplit[0].toLowerCase();
                    String genericCode = lineSplit[1].toLowerCase();
                    rxGenericMap.put(genericName, genericCode);

                    if (genericName != null && StringUtils.containsAny(genericName, "-+")) {
                        genericName = genericName.replaceAll("-", " ");
                        genericName = genericName.replaceAll("\\+", " ");
                        rxGenericMap.put(genericName, genericCode);
                    }
                }
            }
            br.close();
            LOG.info("Loaded Vigilance Generic : " + rxGenericMap.size());

        } catch (Exception e) {
            e.printStackTrace();
            LOG.error("Error while loading vigilance generics file : " + VIGILANCE_GENCODE_FILE + "\t" + e.getMessage());
        }
    }

    private void loadFrequencyMapping() {
        List<FrequencyCodeMap> frequencyCodeMaps = frequencyCodeMapDao.getAll();
        for (FrequencyCodeMap frequencyCodeMap : frequencyCodeMaps) {
            frequencyMap.put(frequencyCodeMap.getName().replaceAll("\\s+", ""), frequencyCodeMap.getCode());
        }
        LOG.info("Frequency map loaded : " + frequencyMap.size());
    }

    public HashMap<String, ArrayList<String>> getRxNormMap() {
        if (rxNormMap == null) {
            init();
        }
        return rxNormMap;
    }

    public Lexicon getLex() {
        if (lex == null) {
            init();
        }
        return lex;
    }

    public void setLex(Lexicon lex) {
        this.lex = lex;
    }

    public Encoder getCoder() {
        if (coder == null) {
            init();
        }
        return coder;
    }

    public void setCoder(Encoder coder) {
        this.coder = coder;
    }

    public Util getUtility() {
        if (utility == null) {
            init();
        }
        return utility;
    }

    public void setUtility(Util utility) {
        this.utility = utility;
    }

    public HashMap<String, String> getFrequencyMap() {
        return frequencyMap;
    }

    public HashMap<String, ArrayList<DrugInfoDto>> getRxCodeMapObj() {
        return rxCodeMapObj;
    }

    private DrugInfoDto copy(DrugInfo drugInfo) {

        if (drugInfo == null) {
            return null;
        }
        DrugInfoDto drugInfoDto = new DrugInfoDto();
        drugInfoDto.setId(drugInfo.getId());
        drugInfoDto.setDin(drugInfo.getDin());
        drugInfoDto.setBrandName(drugInfo.getBrandName());
        drugInfoDto.setGenericName(drugInfo.getGenericName());
        drugInfoDto.setGenCode(drugInfo.getGenCode());
        drugInfoDto.setAtcCode(drugInfo.getAtcCode());
        //drugInfoDto.setAhfs(drugInfo.getAhfs());
        drugInfoDto.setStrength(drugInfo.getStrength());
        drugInfoDto.setRoute(drugInfo.getRoute());
        drugInfoDto.setFormat(drugInfo.getFormat());
        drugInfoDto.setMonographId(drugInfo.getMonographId());
        drugInfoDto.setSent(drugInfo.getSent());
        return drugInfoDto;
    }
}