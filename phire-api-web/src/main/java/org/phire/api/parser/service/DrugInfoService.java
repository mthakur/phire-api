package org.phire.api.parser.service;

import org.phire.api.parser.dao.FrequencyCodeMapDao;
import org.phire.api.parser.model.FrequencyCodeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


@Stateless
@Named(value = "drugInfoService")
public class DrugInfoService {

    private static final Logger LOG = LoggerFactory.getLogger(DrugInfoService.class);

    @Inject
    FrequencyCodeMapDao frequencyCodeMapDao;

    public String search(String key) {

        try {

            List<FrequencyCodeMap> frequencyCodeMapList = frequencyCodeMapDao.getAll();
            LOG.error("frequencyCodeMapList : " + frequencyCodeMapList);
            for (FrequencyCodeMap frequencyCodeMap : frequencyCodeMapList) {
                LOG.error("==" + frequencyCodeMap.getCode());
            }

            URL url = new URL("http://132.206.151.177:6080/drug_mgt/ws/drug/din/" + key.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("jsse.enableSNIExtension", "false");
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                StringBuilder builder = new StringBuilder();

                String aux = "";
                while ((aux = br.readLine()) != null) {
                    builder.append(aux);
                }
                conn.disconnect();
                return builder.toString();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }
}