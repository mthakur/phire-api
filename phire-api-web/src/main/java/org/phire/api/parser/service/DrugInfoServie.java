package org.phire.api.parser.service;

import org.apache.commons.lang3.StringUtils;
import org.phire.api.parser.dto.DrugInfoDto;
import org.phire.api.parser.util.medex.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class DrugInfoServie {

    @Inject
    private ApiCacheService parserCacheService;

    private static final Logger LOG = LoggerFactory.getLogger(DrugInfoServie.class);

    public DrugInfoDto fetchDrugInfo(String drugName) {

        LOG.debug("fetch drug info : " + drugName);

        if (StringUtils.isBlank(drugName)) {
            return null;
        }
        DrugInfoDto drugInfo = null;
        drugName = Util.transformDrugName(drugName);

        try {
            if (parserCacheService.getRxCodeMapObj().containsKey(drugName)) {
                LOG.debug("drug info available for : " + drugName);

                for (DrugInfoDto drugInfoDto : parserCacheService.getRxCodeMapObj().get(drugName)) {

                    if (drugInfoDto.getDin() == null && drugName.equalsIgnoreCase(Util.transformDrugName(drugInfoDto.getGenericName()))) {
                        return drugInfoDto;
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error("Exception while fetching drug info : " + ex.getMessage());
            ex.printStackTrace();
        }

        LOG.debug("returning drug info : " + drugInfo);
        return drugInfo;
    }

    public DrugInfoDto fetchDrugInfo(String din, String drugName) {

        LOG.debug("fetch drug info : " + din + " : " + drugName);

        if (StringUtils.isBlank(din) || StringUtils.isBlank(drugName)) {
            LOG.debug("invalid method parameter : " + din + " : " + drugName);
            return null;
        }
        DrugInfoDto drugInfoDto = null;
        drugName = Util.transformDrugName(drugName);

        try {
            if (parserCacheService.getRxCodeMapObj().containsKey(drugName)) {
                for (DrugInfoDto drugInfo : parserCacheService.getRxCodeMapObj().get(drugName)) {
                    if (StringUtils.isNotBlank(drugInfo.getDin()) && (Integer.parseInt(din) == Integer.parseInt(drugInfo.getDin()))) {
                        drugInfoDto = drugInfo;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error("Exception while fetching drug info : " + ex.getMessage());
            ex.printStackTrace();
        }
        LOG.debug("returning drug info : " + drugInfoDto);
        return drugInfoDto;
    }
}
