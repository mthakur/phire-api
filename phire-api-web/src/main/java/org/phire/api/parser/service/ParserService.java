package org.phire.api.parser.service;

import org.apache.commons.lang3.StringUtils;
import org.phire.api.parser.dto.DrugInfoDto;
import org.phire.api.parser.dto.ParserResponse;
import org.phire.api.parser.dto.ParserInput;
import org.phire.api.parser.dto.VigilNomProdPlusJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

@Stateless
@Named(value = "parserService")
public class ParserService {

    private static final Logger LOG = LoggerFactory.getLogger(ParserService.class);

    @Inject
    private DrugInfoServie drugInfoServie;
    @Inject
    private TaggerService taggerService;
    @Inject
    private PersistRqRsService persistRqRsService;

    public ParserResponse parse(ParserInput parserInput) {

        LOG.debug("parsing prescription : " + parserInput);

        // parsing prescription text
        ParserResponse parserResponse = taggerService.parse(parserInput.getPrescription());

        // update info from vigilance
        updateDinInfo(parserInput, parserResponse);

        // Validate Response
        // validateResponse(parserResponse);

        // Saving request response info
        //persisitInfo(parserInput, parserResponse);

        //LOG.info("rs : " + parserResponse);
        return parserResponse;
    }


    private void validateResponse(ParserResponse parsedDrugInfo) {
        if (parsedDrugInfo.hasMissingRequiredInfo()) {
            //parsedDrugInfo.setError("APPLICATION FAILS TO PARSE INFO");
        }
    }

    private void persisitInfo(ParserInput parseDrugRq, ParserResponse parserResponse) {
        try {
            //parseDrugRq.setParseDrugRs(parserResponse);
            persistRqRsService.saveRqRsInfo(parseDrugRq);
        } catch (Exception ex) {
            LOG.error("Error while saving rq rs into database : " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    private void updateDinInfo(ParserInput parseDrugRqInfo, ParserResponse parsedDrugInfo) {

        try {

//            VigilNomProdPlusJson nomProdFormesPlus = vigilanceDrugService.search(modifyDin(parsedDrugInfo.getDin()));
//            copyDinInfoFromVigilance(parsedDrugInfo, nomProdFormesPlus);

            if (StringUtils.isNotBlank(parseDrugRqInfo.getDin()) && StringUtils.isBlank(parsedDrugInfo.getDin())) {
                DrugInfoDto drugInfoDto = drugInfoServie.fetchDrugInfo(parseDrugRqInfo.getDin(), parsedDrugInfo.getGenericName());
                copyDinInfo(parsedDrugInfo, drugInfoDto);
            }
        } catch (Exception ex) {
            LOG.error("Exception while updating drug info : " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    private void copyDinInfo(ParserResponse parserResponse, DrugInfoDto drugInfoDto) {

        if (parserResponse == null || drugInfoDto == null) {
            return;
        }
        parserResponse.setDin(drugInfoDto.getDin());
        parserResponse.setAtcCode(drugInfoDto.getAtcCode());
        parserResponse.setTrademarkName(drugInfoDto.getBrandName());
        parserResponse.setStrength(drugInfoDto.getStrength());
        if (StringUtils.isBlank(parserResponse.getFormat())) {
            parserResponse.setFormat(drugInfoDto.getFormat());
        }
        if (StringUtils.isBlank(parserResponse.getRoute())) {
            parserResponse.setRoute(drugInfoDto.getRoute());
        }
        parserResponse.setGenericName(drugInfoDto.getGenericName());
    }

    private void copyDinInfoFromVigilance(ParserResponse parserResponse, VigilNomProdPlusJson nomProdFormesPlus) {

        if (nomProdFormesPlus == null) {
            return;
        }

        parserResponse.setDin(nomProdFormesPlus.getDin());
        // get ATC code fgenPlus
        //parserResponse.setAtcCode(nomProdFormesPlus.getCodegen());
        parserResponse.setTrademarkName(nomProdFormesPlus.getCapProdNameEn());
        parserResponse.setStrength(nomProdFormesPlus.getCapStrengthEn());
        if (nomProdFormesPlus.getNomProdFormesPlus() != null && StringUtils.isBlank(nomProdFormesPlus.getNomProdFormesPlus().getFormEng())) {
            parserResponse.setFormat(nomProdFormesPlus.getNomProdFormesPlus().getFormEng());
        }
        if (nomProdFormesPlus.getNomProdFormesPlus() != null && StringUtils.isBlank(parserResponse.getRoute())) {
            parserResponse.setRoute(nomProdFormesPlus.getNomProdFormesPlus().getVigiDefltRouteDosValidtn());
        }
        parserResponse.setGenericName(nomProdFormesPlus.getFgenplus().getGenericNameEn());
    }

    // Temporary as drug details service do not entertain din having length less than 8 char
    private String modifyDin(String din) {

        if (StringUtils.isNoneBlank(din) && din.length() < 8) {
            while (din.length() < 8) {
                din = "0" + din;
            }
        }
        return din;
    }
}
