package org.phire.api.parser.service;

import org.phire.api.parser.dao.ParseRequestDao;
import org.phire.api.parser.dto.ParserResponse;
import org.phire.api.parser.dto.ParserInput;
import org.phire.api.parser.model.ParseRequest;
import org.phire.api.parser.model.ParseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Named;
import java.util.Calendar;

@Stateless
@Named(value = "persistRqRsService")
public class PersistRqRsService {

    @EJB
    ParseRequestDao parseRequestDao;

    private static final Logger LOG = LoggerFactory.getLogger(PersistRqRsService.class);

    public ParserInput saveRqRsInfo(ParserInput parseDrugRq) {
        return copy(parseRequestDao.persist(copy(parseDrugRq)));
    }

    private ParseRequest copy(ParserInput parseDrugRq) {

        if (parseDrugRq == null) {
            return null;
        }

        ParseRequest parseRequest = new ParseRequest();
        parseRequest.setDin(parseDrugRq.getDin());
        parseRequest.setDrugName(parseDrugRq.getDrugName());
        parseRequest.setInstruction(parseDrugRq.getInstruction());
        parseRequest.setSource(parseDrugRq.getSource());
        parseRequest.setCreationTimeStamp(Calendar.getInstance());
        //parseRequest.setParseResponse(copy(parseDrugRq.getParseDrugRs()));
        //parseRequest.setObjectHashCode(parseDrugRq.hashCode());

        return parseRequest;
    }

    private ParserInput copy(ParseRequest parseRequest) {

        if (parseRequest == null) {
            return null;
        }

        ParserInput parseDrugRq = new ParserInput(parseRequest.getDin(), parseRequest.getDrugName(), parseRequest.getInstruction(), parseRequest.getSource());
        // parseDrugRq.setParseDrugRs(copy(parseRequest.getParseResponse()));

        return parseDrugRq;
    }

    private ParseResponse copy(ParserResponse parserResponse) {

        if (parserResponse == null) {
            return null;
        }

        ParseResponse parseResponse = new ParseResponse();
        parseResponse.setDin(parserResponse.getDin());
        parseResponse.setTrademarkName(parserResponse.getTrademarkName());
        parseResponse.setGenericName(parserResponse.getGenericName());
        parseResponse.setGenCode(parserResponse.getGenCode());
        parseResponse.setFormat(parserResponse.getFormat());
        parseResponse.setStrength(parserResponse.getStrength());
        parseResponse.setDosePerAdmin(parserResponse.getDosePerAdmin());
        parseResponse.setUnitAtAdmin(parserResponse.getUnitAtAdmin());
        parseResponse.setQtyAtAdmin(parserResponse.getQuantityPerAdmin());
        parseResponse.setFormatAtAdmin(parserResponse.getFormatAtAdmin());
        parseResponse.setRoute(parserResponse.getRoute());
        parseResponse.setFrequency(parserResponse.getFrequency());
        parseResponse.setFrequencyCode(parserResponse.getFreqCode());
        parseResponse.setDirective(parserResponse.getDirective());
        parseResponse.setDuration(parserResponse.getDuration());
        parseResponse.setPrn(parserResponse.getPrn());
        parseResponse.setRepeat(parserResponse.getRepeat());
        parseResponse.setCreationTimeStamp(Calendar.getInstance());
        //parseResponse.setError(parserResponse.getError());
        //parseResponse.setWarning(parserResponse.getWarning());

        return parseResponse;
    }


    private ParserResponse copy(ParseResponse parseResponse) {

        if (parseResponse == null) {
            return null;
        }

        ParserResponse parserResponse = new ParserResponse();
        parserResponse.setDin(parseResponse.getDin());
        parserResponse.setTrademarkName(parseResponse.getTrademarkName());
        parserResponse.setGenericName(parseResponse.getGenericName());
        parserResponse.setGenCode(parseResponse.getGenCode());
        parserResponse.setFormat(parseResponse.getFormat());
        parserResponse.setStrength(parseResponse.getStrength());
        parserResponse.setDosePerAdmin(parseResponse.getDosePerAdmin());
        parserResponse.setUnitAtAdmin(parseResponse.getUnitAtAdmin());
        parserResponse.setQuantityPerAdmin(parseResponse.getQtyAtAdmin());
        parserResponse.setFormatAtAdmin(parseResponse.getFormatAtAdmin());
        parserResponse.setFreqCode(parseResponse.getFrequencyCode());
        parserResponse.setRoute(parseResponse.getRoute());
        parserResponse.setFrequency(parseResponse.getFrequency());
        return parserResponse;
    }
}
