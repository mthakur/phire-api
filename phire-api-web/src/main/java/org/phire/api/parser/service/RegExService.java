package org.phire.api.parser.service;

import org.apache.commons.lang3.StringUtils;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Triplet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Stateless
@Named(value = "regExService")
public class RegExService {

    private static final Logger LOG = LoggerFactory.getLogger(ParserRuleService.class);

    private static Map regexList = new Hashtable();
    private static String number = "((\\d+)(\\.|( ?),( ?))?\\d*)";
    private static String numberWOZero = "((\\d+)?(\\.|( ?),( ?))?\\d*)";
    private static String unitSeparator = "( ?)(/)( ?)";
    private static String comboSeparator = "( ?)(\\+|\\-)( ?)";
    private static String unit = "(\\%\\+|\\%|millilitre|milliards|insuline|milliard|millions|billion|million|ampoule|inches|mcgear|mcgrae|pouces|valent|allait|ampule|sachet|saveur|grain|minim|units|unite|bande" +
            "|grain|pouch|strip|sucre|sucre|drop|gtte|mgat|mmol|volt|btle|drop|jour|lact|sach|bau|cmk|cps|kbq|lmk|mcg|mci|mcl|meq|miu|mui|pfu|ppm|xcm|xmk|bag|cap|day|dps" +
            "|gte|low|pck|vap|at|ch|cm|dh|gx|in|iu|lf|mc|mg|ml|mm|mt|mu|po|rx|su|tm|tu|hr|ui|ch|cm|dh|hr|kg|ml|po|tm|b|c|d|e|g|k|l|m|u|x|c|d|g|h|l|m|x)";

    private static String strength_00 = "^" + number + "( ?)" + unit + "?" + "(" + comboSeparator + numberWOZero + "( ?)" + unit + "?" + "( ?))+" + unitSeparator + numberWOZero + "?" + unit + "?" + "(\\s)*(\\.?)";
    private static String strength_01 = "^" + number + "( ?)" + unit + "?" + "(" + comboSeparator + numberWOZero + "( ?)" + unit + "?" + "( ?))+(\\.?)";
    private static String strength_02 = "^" + number + "( ?)" + unit + "?" + unitSeparator + numberWOZero + "( ?)" + unit + "?" + " (\\.?)";
    private static String strength_03 = "^" + number + "( ?)" + unit + "?" + unitSeparator + unit + "(\\.?)";
    private static String strength_04 = "^" + number + "( ?)" + unit + "(\\s)+(\\.?)";

    private static String TOD = "^\\d+( |)(am|pm|clock)";
    private static String NUM_00 = "^(\\d+)( ?)(to|-|a|à|À)( ?)(\\d+)";
    //private static String NUM_01 = "^[-+]?[0-9]*\\.?[0-9]+$";
    //private static String NUM_01 = "^[0-9]*?\\.?[0-9]+ $";
    private static String NUM_01 = "^(\\d+)?(\\.|\\,|/|\\+)?\\d+";
    private static String NUM_02 = "^(one|two|three|four|five|six|seven|eight|nine|ten) to (one|two|three|four|five|six|seven|eight|nine|ten)( |$)";
    private static String NUM_03 = "^(un|deux|trois|quatre|cinq|six|sept|huit|neuf|dix) (à|a) (un|deux|trois|quatre|cinq|six|sept|huit|neuf|dix)( |$)";

    //    private static KnowledgeBase kbaseAmbiguation;
    //    private static KnowledgeBase kbaseTransformOne;
    //    private static KnowledgeBase kbaseTransformTwo;

    static {

        // DOSE
        regexList.put(strength_00, "DOSE");
        regexList.put(strength_01, "DOSE");
        regexList.put(strength_02, "DOSE");
        regexList.put(strength_03, "DOSE");
        regexList.put(strength_04, "DOSE");

        //TOD  Time Of Day
        regexList.put(TOD, "DOD");

        // NUM Number
        regexList.put(NUM_00, "NUM");
        regexList.put(NUM_01, "NUM");
        regexList.put(NUM_02, "NUM");
        regexList.put(NUM_03, "NUM");

        // DRT Duration
        regexList.put("^\\d{1,2}\\/\\d{1,2} \\- \\d{1,2}\\/\\d{1,2}( |$)", "DRT");
        regexList.put("^x\\d+( )(day|days|day\\(s\\)|wk|week|weeks|m|month|months|jour|jours)( |$)", "DRT");
        regexList.put("^(\\d+|one|two|three|four|five|six|seven|eight|nine|ten) (day|days|days\\(s\\))( |$)", "DRT");
        regexList.put("^(\\d+|un|deux|trois|quatre|cinq|six|sept|huit|neuf|dix) (jour|jours)( |$)", "DRT");
        regexList.put("^(\\d+|one|two|three|four|five|six|seven|eight|nine|ten|eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen|twenty|thirty)( more | )(d|day|days|wk|week|weeks|m|month|months)( |$)", "DRT");

        // TUNIT  Time Unit
        regexList.put("^x(min|minute|minutes|d|day|days|wk|week|weeks|m|month|months)( |$)", "TUNIT");

        // FREQ Frequncy
        regexList.put("^q(\\.|)\\d+( |$)", "FREQ");
        regexList.put("^(every|each|per|par|aux) (\\d+) (hour|heure|jour|jours)( |$)", "FREQ");
        regexList.put("^\\d( |)x( |)(daily|weekly|monthly|a day|a week|a month|per day|per week|per month)", "FREQ");
        regexList.put("^(\\d+)( ?)(a|à)( ?)(\\d+) fois (par jour|par semaine)( |$)", "FREQ");
        regexList.put("^(\\d+|un|deux|trois|quatre|cinq|six|sept|huit|neuf|dix) fois (par jour|par semaine)( |$)", "FREQ");
        regexList.put("^(\\d+|one|two|three|four|five|six|seven|eight|nine|ten) times (daily|weekly|monthly|a day|a week|a month|per day|per week|per month)( |$)", "FREQ");
        regexList.put("^(every|q|qdaily) (m|mon|t|tue|tues|w|wed|r|thu|thur|f|fri|sat|sun|monday|tuesday|wednesday|thursday|friday|saturday|sunday|,| |and)+( |$)", "FREQ");
        regexList.put("^(\\d+) (night|nights) (every|each|per|par|aux) (week|weeks)( |$)", "FREQ");
        regexList.put("^(every|each|per|par|aux) (minute|hour|heure|heures|morning|afternoon|evening|day|jour|jours|wk|week|semaine|month|bedtime|breakfast|lunch|dinner)( |$)", "FREQ");
        regexList.put("^(m|mon|t|tue|w|wed|r|thu|f|fri|sat|sun|monday|tuesday|wednesday|thursday|friday|saturday|sunday) and (m|mon|t|tue|w|wed|r|thu|f|fri|sat|sun|monday|tuesday|wednesday|thursday|friday|saturday|sunday)( |$)", "FREQ");
        regexList.put("^(every|aux) (\\d+|\\d+\\-\\d+|\\d+ to \\d+|\\d+ a \\d+|\\d+ \\- \\d+|one|two|three|four|five|six|seven|eight|nine|ten|eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen|twenty|twenty-one|twenty-two|twenty-three|twenty-four) ?(hrs|hr|h|m|min|d|hour|hours|heure|heures|day|days|minute|minutes|wk|week|weeks|month|months)( |$)", "FREQ");

        regexList.put("^(q|every)( )(\\w+(( ?)(\\-)( ?)(\\w+))+)", "FREQ");
        regexList.put("^q( )(\\d+)( ?)([^\\s]+)", "FREQ");
        regexList.put("^q( )([^\\s]+)", "FREQ");
        regexList.put("^q( \\. | |\\.|)( |)(\\d+|\\d\\-\\d|\\d \\- \\d)?( |)(hrs|hr|h|m|min|d|hour|hours|heures|heure|day|days|minute|minutes|wk|week|weeks|month|months)(prn|)(\\.|)( |$)", "FREQ");

        // Repeat
        regexList.put("^(refill( ?)\\(( ?)s( ?)\\)( ?):( ?)( ?)\\d+)$", "REPEAT");
    }


    private Triplet<Integer, String, String> findMatch(int start, String strTokens) throws IOException {

        int end = -1;
        String term = "";
        String tag = "";
        int maxLength = 0;
        Pattern pattern;
        Matcher matcher;
        Iterator it = regexList.entrySet().iterator();

        while (it.hasNext()) {

            Map.Entry pairs = (Map.Entry) it.next();
            String regexp = (String) pairs.getKey();
            String stag = (String) pairs.getValue();
            pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(strTokens);

            if (matcher.find()) {
                String matchString = matcher.group(0).trim();
                int matchLength = matchString.split(" ").length;
                if (matchLength > maxLength) {
                    maxLength = matchLength;
                    end = start + matchLength;
                    term = rTrim(matchString);
                    tag = stag;
                }
            }
        }
        LOG.info("findMatch : " + start + "\t" + strTokens);
        LOG.info("findMatch : " + end + "\t" + term + "\t" + tag);
        Triplet<Integer, String, String> tuple = Triplet.with(end, term, tag);
        return tuple;
    }


    public ArrayList<Quartet<String, String, Integer, Pair<Pair<String, Integer>, Integer>>> parse(
            String[] tokens, String tags[], int[] tagIndex, ArrayList sentTokenMap) throws IOException {

        ArrayList sentTag = new ArrayList();
        int startPos = 0;
        int endPos = -1;
        int idx = 0;

        while (startPos < tokens.length) {
            String term = tokens[startPos];
            String tag = "TK";
            String lookedUpTerm = "";

            if (!tags[startPos].equals("TK")) {
                endPos = startPos;
                int first_tag_index = tagIndex[endPos];
                while (!tags[endPos].equals("TK") && endPos < tokens.length - 1
                        && first_tag_index == tagIndex[endPos]) {
                    lookedUpTerm += tokens[endPos] + " ";
                    endPos++;
                }

                if (endPos == tokens.length - 1) {
                    if (!tags[endPos].equals("TK") && first_tag_index == tagIndex[endPos]) {
                        lookedUpTerm += tokens[endPos] + " ";
                        endPos++;
                    }
                }
                endPos--;
            }

            lookedUpTerm = lookedUpTerm.trim();
            Triplet<Integer, String, String> tempTuple = findMatch(startPos, StringUtils.join(tokens, " ", startPos, tokens.length));
            int endPosReg = tempTuple.getValue0();
            String termReg = tempTuple.getValue1();
            String tagReg = tempTuple.getValue2();

            if (endPosReg > -1) {
                boolean isUpdate = true;
                if (endPosReg < tokens.length) {
                    if (!tags[endPosReg].equals("TK") && !tags[endPosReg - 1].equals("TK")
                            && tagIndex[endPosReg] == (tagIndex[endPosReg - 1])) {
                        isUpdate = false;
                    }
                }
                if (isUpdate) {
                    tag = tagReg;
                    term = termReg;
                    startPos = endPosReg - 1;
                } else {
                    if (endPos > -1) {
                        tag = tags[startPos];
                        startPos = endPos;
                        term = lookedUpTerm;
                    } else {
                        tag = tags[startPos];
                    }
                }
            } else {
                if (endPos > -1) {
                    tag = tags[startPos];
                    startPos = endPos;
                    term = lookedUpTerm;
                } else {
                    tag = tags[startPos];
                }
            }
            Pair<Pair<String, Integer>, Integer> pair = (Pair<Pair<String, Integer>, Integer>) sentTokenMap.get(startPos);
            Quartet<String, String, Integer, Pair<Pair<String, Integer>, Integer>> tagTuple = Quartet.with(term, tag, idx, pair);
            sentTag.add(tagTuple);
            startPos++;
            idx = idx + 1;
            endPos = -1;
        }
        return sentTag;
    }

    private String rTrim(String in) {
        int len = in.length();
        while (len > 0) {
            if (!Character.isWhitespace(in.charAt(--len))) {
                return in.substring(0, len + 1);
            }
        }
        return "";
    }
}
