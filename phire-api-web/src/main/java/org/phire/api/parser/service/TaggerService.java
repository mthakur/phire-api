package org.phire.api.parser.service;

import org.apache.commons.lang3.StringUtils;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.phire.api.parser.dto.DrugInfoDto;
import org.phire.api.parser.dto.ParserResponse;
import org.phire.api.parser.util.*;
import org.phire.api.parser.util.NLPTools.Document;
import org.phire.api.parser.util.NLPTools.Global;
import org.phire.api.parser.util.algorithms.SuffixArray;
import org.phire.api.parser.util.medex.DrugTag;
import org.phire.api.parser.util.medex.Encoder;
import org.phire.api.parser.util.medex.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@Stateless
@Named(value = "medTaggerServive")
public class TaggerService {

    private static final Logger LOG = LoggerFactory.getLogger(TaggerService.class);

    @Inject
    private ParserRuleService parserRuleService;

    @Inject
    private ApiCacheService parserCacheService;

    @Inject
    private DrugInfoServie drugInfoServie;

    public ParserResponse parse(String prescription) {

        ParserResponse parsedDrugInfo = new ParserResponse();
        Document doc = new Document(prescription);

        try {

            Lexicon lex = parserCacheService.getLex();
            Encoder coder = parserCacheService.getCoder();
            ArrayList<Pair<String, String>> tokenTagPairs;
            ArrayList<Pair<String, String>> pairPostTransform;

            // lexicon search
            SuffixArray sa = new SuffixArray(doc, lex, '\0', Global.SuffixArrayMode.WORD, Global.SuffixArrayCaseMode.NON_SENSITIVE);
            //LOG.error("Search Result : " + sa.search());
            doc.drugTag().addAll(sa.getSearchResult());

            String drugTag = "";
            for (DrugTag tag : sa.getSearchResult()) {
                drugTag += tag.semanticTag() + "=" + tag.getText() + ", ";
            }
            LOG.debug("Lexicon token search : " + drugTag);

            // removing overlapping
            doc.filterOverlappedDrugTag();
            doc.print();

            // Applying transformations
            //tokenTags = parserRuleService.transform(doc);
            tokenTagPairs = parserRuleService.transformTagTokens(doc);

            // remove redundant tokens
            pairPostTransform = removeRedundantTokens(tokenTagPairs);
            // removeRedundantTagTokenPairList(tokenTags);

            //int tagIndex = 0;
            int isDrugScore = 0;
            //ArrayList sentSemTerms = new ArrayList();
            ArrayList tagTokenPair = new ArrayList();
            String unParsedTokens = "";
            String token = "", preTag = "", tag = "";

            // calculating drug score and Quintet pair object
            for (Pair<String, String> tokenTag : pairPostTransform) {
                tag = tokenTag.getValue0();
                token = tokenTag.getValue1();

                if (!tokenTag.getValue0().equals(CommonConst.TK)
                        && !tokenTag.getValue0().equals(CommonConst.INDICATION) && !tokenTag.getValue0().equals(CommonConst.TUNIT)
                        && !tokenTag.getValue0().equals(CommonConst.UNIT)) {

                    tagTokenPair.add(new Pair<>(tokenTag.getValue0(), tokenTag.getValue1()));

                    if (token.equals(CommonConst.DIN) || token.equals(CommonConst.DBN) || token.equals(CommonConst.DPN)
                            || token.equals(CommonConst.DSCD) || token.equals(CommonConst.DSCDC) || token.equals(CommonConst.DSCDF)) {
                        isDrugScore = isDrugScore + 2;
                    } else {
                        isDrugScore = isDrugScore + 1;
                    }
                } else {
                    if (preTag.equalsIgnoreCase(tag)) {
                        unParsedTokens += " " + token;
                    } else {
                        unParsedTokens += " / " + token;
                    }
                }
                preTag = tag;
            }

            if (StringUtils.isNotBlank(unParsedTokens)) {
                tagTokenPair.add(new Pair("DIRECTIVE", unParsedTokens));
            }

            LOG.debug("drug Score : " + isDrugScore);
            LOG.debug("sentSemTerms : " + tagTokenPair);

            DrugInfoDto drugInfoDto = null;
            Map<String, String> tagTokenMap = null;
            String tagTerm = new RegexParser().parsePrescriptionFormat(tagTokenPair);
            LOG.debug("tagTerm : " + tagTerm);

            // match parsed info with standard prescription data to get generic code, atc other info
            if (StringUtils.isBlank(tagTerm)) {
                // parsedDrugInfo.setError("Not a standard prescription format.");

            } else if (StringUtils.isBlank(parsedDrugInfo.getTrademarkName()) && (isDrugScore >= 2)) {
                tagTokenMap = mapDruglist(tagTerm);
                drugInfoDto = coder.encode(tagTokenMap);
            }

            //TODO remove
            LOG.debug("drugInfoDto : " + drugInfoDto);
            LOG.debug("tagTokenPair : " + tagTokenPair);
            LOG.debug("parsedDrugInfo : " + parsedDrugInfo);
            LOG.debug("tagTokenMap : " + tagTokenMap);


            if (drugInfoDto == null) {
                updateResponseObject(tagTokenPair, parsedDrugInfo);
            } else {
                parsedDrugInfo = updateResponseInfo(drugInfoDto, tagTokenMap);
            }

        } catch (Exception e) {
            LOG.error("Exception caught while paring : " + e.getMessage());
            // parsedDrugInfo.setError("Application Error");
            e.printStackTrace();
        }
        return parsedDrugInfo;
    }


    private ArrayList<Pair<String, String>> removeRedundantTokens(ArrayList<Pair<String, String>> tokenTags) {

        if (tokenTags == null || tokenTags.size() == 0) {
            return null;
        }

        for (int j = 0; j < tokenTags.size(); j++) {
            int index = j + 1;
            if (index < tokenTags.size()) {
                String currentTokenTag = tokenTags.get(j).getValue0();
                String nextTokenTag = tokenTags.get(j + 1).getValue0();
                if (nextTokenTag.startsWith(currentTokenTag + " ")) {
                    tokenTags.remove(j);
                }
            }
        }

        ArrayList<Pair<String, String>> posTemp = new ArrayList();
        int counter = 0;

        for (int j = 0; j < tokenTags.size(); j++) {
            Pair<String, String> tagTokenPair = tokenTags.get(j);
            posTemp.add(tagTokenPair);
            counter++;
        }
        LOG.debug("posTemp : " + posTemp);
        return posTemp;
    }


    private String formatDruglist(String DList) {

        LOG.info("formatDrugList : DList: " + DList);
        String[] items = DList.split("\t");
        String drug, bdrug, ddf, strength, dose, doseamt, rut, freq, directive, du, nec, repeat;
        drug = bdrug = ddf = strength = dose = doseamt = rut = freq = directive = du = nec = repeat = CommonConst.NA;
        LOG.debug("items length : " + items.length);
        String tag = "", token = "";
        Map<String, String> drugListMap = new LinkedHashMap<String, String>();

        for (int k = 0; k < items.length; k++) {

            String DGListElement = items[k];
            String[] Str = DGListElement.split(" FFF ");

            LOG.debug("\t" + k + " | " + Str[0] + " | " + Str[1]);
            tag = Str[0];
            token = Str[1];

            if (tag.equals(CommonConst.DBN) || ((tag.equals(CommonConst.DBN) || tag.equals(CommonConst.DPN)) && drug.equals(CommonConst.NA))) {
                drug = Str[1];
            }
            if (tag.equals(CommonConst.MDBN) && bdrug.equals(CommonConst.NA)) {
                bdrug = token;
            }
            if (tag.equals(CommonConst.DDF) && ddf.equals(CommonConst.NA)) {
                ddf = token;
            }
            if (tag.equals(CommonConst.DOSE) && strength.equals(CommonConst.NA)) {
                strength = token;
                continue;
            }
            if (tag.equals(CommonConst.DOSE) && !strength.equals(CommonConst.NA)) {
                dose = token;
            }
            if (tag.equals(CommonConst.DOSEAMT) && doseamt.equals(CommonConst.NA)) {
                doseamt = token;
            }
            if (tag.equals(CommonConst.RUT) && rut.equals(CommonConst.NA)) {
                rut = token;
            }
            if (tag.equals(CommonConst.FREQ) && freq.equals(CommonConst.NA)) {
                freq = token;
            }
            if (tag.equals(CommonConst.DRT) && du.equals(CommonConst.NA)) {
                du = token;
            }
            if (tag.equals(CommonConst.NEC) && nec.equals(CommonConst.NA)) {
                nec = token;
            }
            if (tag.equals(CommonConst.DIRECTIVE) && nec.equals(CommonConst.NA)) {
                directive = token;
            }
            if (tag.equals(CommonConst.REPEAT) && nec.equals(CommonConst.NA)) {
                repeat = token;
            }
        }

        if (dose.equals(CommonConst.NA) && doseamt.equals(CommonConst.NA)) {
            dose = strength;
            strength = CommonConst.NA;
        }

        String[] sentenceOne = {drug, bdrug, ddf, strength, dose, doseamt, rut, freq, directive, du, nec, nec};
        ArrayList sentenceOneList = new ArrayList(Arrays.asList(sentenceOne));

        String[] sentenceTwo = {drug, null, ddf, strength, dose, doseamt, rut, freq, directive, du, nec, repeat};
        ArrayList sentenceTwoArray = new ArrayList(Arrays.asList(sentenceTwo));

        drugListMap.put(Util.DRUG_STRING, drug);
        drugListMap.put(Util.BRAND_STRING, bdrug);
        drugListMap.put(Util.FORM_STRING, ddf);
        drugListMap.put(Util.FREQUENCY_STRING, strength);
        drugListMap.put(Util.DOSE_STRING, dose);
        drugListMap.put(Util.AMOUNT_STRING, doseamt);
        drugListMap.put(Util.ROUTE_STRING, rut);
        drugListMap.put(Util.FREQUENCY_STRING, freq);
        drugListMap.put(Util.DIRECTIVE_STRING, directive);
        drugListMap.put(Util.DURATION_STRING, du);
        drugListMap.put(Util.NECESSITY_STRING, nec);
        drugListMap.put(Util.REPEAT_STRING, repeat);

        ArrayList<String> genericList = new ArrayList<String>();
        String[] temps = bdrug.toLowerCase().split("_");
        String bDrugName = temps[0];
        String[] temps2 = drug.split("_");
        String drugName = temps2[0];

        if (!bDrugName.equals("")) {
            if (parserCacheService.getRxNormMap().containsKey(bDrugName)) {
                genericList = parserCacheService.getRxNormMap().get(bDrugName);
            }
        }

        if ("na".equalsIgnoreCase(bDrugName) || genericList.contains(drugName.toLowerCase())) {
            return StringUtils.join(sentenceOneList, "__SEP__");
        } else {
            return StringUtils.join(sentenceTwoArray, "__SEP__");
        }
    }


    private Map<String, String> mapDruglist(String list) {

        String tag = "", token = "";
        String[] items = list.split("\t");
        String drug, bdrug, ddf, strength, dose, doseamt, rut, freq, directive, du, nec, repeat;
        drug = bdrug = ddf = strength = dose = doseamt = rut = freq = directive = du = nec = repeat = StringUtils.EMPTY;
        Map<String, String> drugListMap = new LinkedHashMap<String, String>();

        for (int k = 0; k < items.length; k++) {
            String element = items[k];

            if (StringUtils.isBlank(element)) {
                continue;
            }
            String[] Str = element.split(" FFF ");
            tag = Str[0];
            token = Str[1];

            if (tag.equals(CommonConst.DIN) || ((tag.equals(CommonConst.DBN) || tag.equals(CommonConst.DPN)) && drug.equals(CommonConst.NA))) {
                drug = token;
            } else if (tag.equals(CommonConst.DBN) && StringUtils.isBlank(bdrug)) {
                bdrug = token;
            } else if (tag.equals(CommonConst.DDF) && StringUtils.isBlank(ddf)) {
                ddf = token;
            } else if (tag.equals(CommonConst.DOSE) && StringUtils.isBlank(strength)) {
                strength = token;
            } else if (tag.equals(CommonConst.DOSE) && !StringUtils.isBlank(strength)) {
                dose = token;
            } else if (tag.equals(CommonConst.DOSEAMT) && StringUtils.isBlank(doseamt)) {
                doseamt = token;
            } else if (tag.equals(CommonConst.RUT) && StringUtils.isBlank(rut)) {
                rut = token;
            } else if (tag.equals(CommonConst.FREQ) && StringUtils.isBlank(freq)) {
                freq = token;
            } else if (tag.equals(CommonConst.DRT) && StringUtils.isBlank(du)) {
                du = token;
            } else if (tag.equals(CommonConst.NEC) && StringUtils.isBlank(nec)) {
                nec = token;
            } else if (tag.equals(CommonConst.DIRECTIVE) && StringUtils.isNotBlank(token)) {
                if (StringUtils.isNotBlank(directive)) {
                    directive = directive + "/" + token;
                } else {
                    directive = token;
                }
            } else if (tag.equals(CommonConst.REPEAT) && StringUtils.isBlank(nec)) {
                repeat = token;
            }
        }

        if (StringUtils.isBlank(dose) && StringUtils.isBlank(doseamt) && !isCreamOrLiquid(ddf, strength)) {
            dose = strength;
            strength = StringUtils.EMPTY;
        }
        drugListMap.put(Util.DRUG_STRING, drug);
        drugListMap.put(Util.BRAND_STRING, bdrug);
        drugListMap.put(Util.FORM_STRING, ddf);
        drugListMap.put(Util.STRENGTH_STRING, strength);
        drugListMap.put(Util.DOSE_STRING, dose);
        drugListMap.put(Util.AMOUNT_STRING, doseamt);
        drugListMap.put(Util.ROUTE_STRING, rut);
        drugListMap.put(Util.FREQUENCY_STRING, freq);
        drugListMap.put(Util.DIRECTIVE_STRING, directive);
        drugListMap.put(Util.DURATION_STRING, du);
        drugListMap.put(Util.NECESSITY_STRING, nec);
        drugListMap.put(Util.REPEAT_STRING, repeat);

        ArrayList<String> genericList = new ArrayList<String>();
        LOG.debug("bdrug : " + bdrug);
        String[] temps = bdrug.toLowerCase().split("_");
        String bDrugName = temps[0];
        String[] temps2 = drug.split("_");
        String drugName = temps2[0];

        if (StringUtils.isNotBlank(bDrugName)) {
            if (parserCacheService.getRxNormMap().containsKey(bDrugName)) {
                genericList = parserCacheService.getRxNormMap().get(bDrugName);
            }
            // check if brand has this drug else remove the brand name
            if (!genericList.contains(drugName.toLowerCase()) && StringUtils.isBlank(strength)) {
                drugListMap.put(Util.BRAND_STRING, StringUtils.EMPTY);
            }
        }
        LOG.debug("mapping drug list : " + drugListMap);
        return drugListMap;
    }


    private static ArrayList<ArrayList<Quintet<String, String, Integer, Pair<Pair<String, Integer>, Integer>, Integer>>> breakSentenceByMed(
            ArrayList<Quintet<String, String, Integer, Pair<Pair<String, Integer>, Integer>, Integer>> list) {

        ArrayList taglist = new ArrayList();
        taglist.add(CommonConst.DIN);
        taglist.add(CommonConst.DBN);
        taglist.add(CommonConst.DPN);

        ArrayList tokenlist = new ArrayList();
        tokenlist.add(",");
        tokenlist.add("and");
        tokenlist.add("or");
        tokenlist.add(";");

        ArrayList<ArrayList<Quintet<String, String, Integer, Pair<Pair<String, Integer>, Integer>, Integer>>> medList = new ArrayList();
        int start = list.size() - 1;

        while (rightFindTag(list, taglist, start) >= 0) {
            int currentMedIndex = rightFindTag(list, taglist, start);
            int connectIndex = rightFindToken(list, tokenlist, (currentMedIndex - 1));
            int nextMedIndex = rightFindTag(list, taglist, connectIndex);
            if (connectIndex >= 0 && nextMedIndex >= 0) {
                ArrayList<Quintet<String, String, Integer, Pair<Pair<String, Integer>, Integer>, Integer>> t = new ArrayList();
                for (int i = connectIndex; i < start + 1; i++)
                    t.add(list.get(i));
                medList.add(t);
                start = connectIndex - 1;
            } else {
                ArrayList<Quintet<String, String, Integer, Pair<Pair<String, Integer>, Integer>, Integer>> t = new ArrayList();
                for (int i = 0; i < start + 1; i++)
                    t.add(list.get(i));

                medList.add(t);
                start = -1;
            }
        }
        return medList;
    }

    /**
     * Searches the token-tag list from the right, to find the index of a
     * specified tag.
     *
     * @param list     list of token-tag
     * @param target   tags to be searched
     * @param startpos start offset
     */
    private static int rightFindTag(
            ArrayList<Quintet<String, String, Integer, Pair<Pair<String, Integer>, Integer>, Integer>> list,
            ArrayList target, int startpos) {
        int index = startpos;
        int targetPos = -1;
        while (index >= 0) {
            if (target.contains(list.get(index).getValue1())) {
                targetPos = index;
                break;
            } else
                index = index - 1;
        }

        return targetPos;
    }

    /**
     * Searches the token-tag list from the right, to find the index of a
     * specified token.
     *
     * @param list   list of drug signature
     * @param target tokens to be searched
     * @return array of string contains UMLS code and RXNORM code
     * @see TaggerService
     */
    private static int rightFindToken(
            ArrayList<Quintet<String, String, Integer, Pair<Pair<String, Integer>, Integer>, Integer>> list,
            ArrayList target, int startpos) {
        int index = startpos;
        int targetPos = -1;
        while (index >= 0) {
            if (target.contains(list.get(index).getValue0())) {
                targetPos = index;
                break;
            } else
                index = index - 1;
        }
        return targetPos;
    }

    private ParserResponse updateResponseInfo(DrugInfoDto drugInfoDto, Map<String, String> tokenMap) {

        ParserResponse parserResponse = new ParserResponse();
        LOG.debug("drugInfoDto : " + drugInfoDto);
        LOG.debug("final Tokens : " + Arrays.asList(tokenMap));
        String value;

        for (String token : tokenMap.keySet()) {
            value = tokenMap.get(token);
            if (CommonConst.DRUG.equals(token)) {
                parserResponse.setGenericName(value);
            } else if (CommonConst.FREQ.equals(token)) {
                parserResponse.setFrequency(value);
            } else if (CommonConst.DOSEAMOUNT.equals(token)) {
                updateQtyAtAdminInfo(parserResponse, value);
            } else if (CommonConst.ROUTE.equals(token)) {
                parserResponse.setRoute(value);
            } else if (CommonConst.FORM.equals(token)) {
                parserResponse.setFormat(value);
            } else if (CommonConst.DIRECTIVE.equals(token)) {
                parserResponse.setDirective(value);
            } else if (CommonConst.DURATION.equals(token)) {
                parserResponse.setDuration(value);
            } else if (CommonConst.PRN.equals(token)) {
                parserResponse.setPrn(value);
            } else if (CommonConst.REPEAT.equals(token)) {
                parserResponse.setRepeat(value);
            } else if (CommonConst.NEC.equals(token)) {
                parserResponse.setPrn(value);
            } else if (CommonConst.NECESSITY.equals(token)) {
                parserResponse.setPrn(value);
            } else if (CommonConst.STRENGTH.equals(token)) {
                parserResponse.setStrength(value);
            } else if (CommonConst.DOSE.equals(token)) {
                updateDoseInfo(parserResponse, value);
            } else if (CommonConst.REPEAT.equals(token)) {
                parserResponse.setRepeat(value);
            } else if (CommonConst.FREQUENCY.equals(token)) {
                parserResponse.setFrequency(value);
                updateFrequencyCode(parserResponse, value);
            }
        }

        if (drugInfoDto != null) {
            if (StringUtils.isNotBlank(drugInfoDto.getDin())) {
                parserResponse.setDin(drugInfoDto.getDin());
                parserResponse.setTrademarkName(drugInfoDto.getBrandName());
                parserResponse.setFormat(drugInfoDto.getFormat());
                parserResponse.setRoute(drugInfoDto.getRoute());
            }
            parserResponse.setGenericName(drugInfoDto.getGenericName());
            parserResponse.setGenCode(drugInfoDto.getGenCode());
            parserResponse.setAtcCode(drugInfoDto.getAtcCode());
        }

        calculateDosePerAdmin(parserResponse);
        LOG.debug("parserResponse : " + parserResponse);
        return parserResponse;
    }

    private void updateResponseObject(ArrayList<Pair<String, String>> posTemp, ParserResponse parsedDrugInfo) {

        for (Pair<String, String> pairIntegerQuintet : posTemp) {

            String token = pairIntegerQuintet.getValue1();
            String tag = pairIntegerQuintet.getValue0();

            if (CommonConst.DOSE.equals(tag)) {
                if (StringUtils.isBlank(parsedDrugInfo.getDosePerAdmin())) {
                    updateDoseInfo(parsedDrugInfo, token);
                } else {
                    parsedDrugInfo.setStrength(parsedDrugInfo.getDosePerAdmin() + parsedDrugInfo.getUnitAtAdmin());
                    updateDoseInfo(parsedDrugInfo, token);
                }
            } else if (CommonConst.DBN.equals(tag)) {
                parsedDrugInfo.setTrademarkName(token);
            } else if (CommonConst.DIN.equals(tag)) {
                parsedDrugInfo.setGenericName(token);
                DrugInfoDto drugInfoDto = drugInfoServie.fetchDrugInfo(token);
                if (drugInfoDto != null) {
                    parsedDrugInfo.setGenCode(drugInfoDto.getGenCode());
                    parsedDrugInfo.setAtcCode(drugInfoDto.getAtcCode());
                }
                //parsedDrugInfo.setError("No standard prescription format found.");
            } else if (CommonConst.DOSEAMT.equals(tag)) {
                updateQtyAtAdminInfo(parsedDrugInfo, token);

            } else if (CommonConst.FREQ.equals(tag)) {
                parsedDrugInfo.setFrequency(token);
                updateFrequencyCode(parsedDrugInfo, parsedDrugInfo.getFrequency());

            } else if (CommonConst.RUT.equals(tag)) {
                parsedDrugInfo.setRoute(token);
            } else if (CommonConst.FORMAT.equals(tag)) {
                parsedDrugInfo.setFormat(token);
            } else if (CommonConst.DIRECTIVE.equals(tag)) {
                parsedDrugInfo.setDirective(token);
            } else if (CommonConst.DURATION.equals(tag)) {
                parsedDrugInfo.setDuration(token);
            } else if (CommonConst.PRN.equals(tag)) {
                parsedDrugInfo.setPrn(token);
            } else if (CommonConst.REPEAT.equals(tag)) {
                parsedDrugInfo.setRepeat(token);
            } else if (CommonConst.NEC.equals(tag)) {
                parsedDrugInfo.setPrn(token);
            }
        }


        if (StringUtils.isBlank(parsedDrugInfo.getStrength()) && StringUtils.isNotBlank(parsedDrugInfo.getQuantityPerAdmin())) {
            parsedDrugInfo.setStrength(parsedDrugInfo.getDosePerAdmin());
            parsedDrugInfo.setDosePerAdmin("");
        }

    }

    // Extracting unit and values from dose
    private void updateDoseInfo(ParserResponse parserResponse, String doseValue) {

        if (doseValue != null) {
            Dose dose = DoseCalculator.parseDoseText(doseValue);
            if (dose == null) {
                parserResponse.setDosePerAdmin(doseValue);
            } else {
                parserResponse.setDosePerAdmin(dose.getDoseValue());
                parserResponse.setUnitAtAdmin(dose.getDoseUnit());
            }
        }
    }

    // Extracting unit and values from dose amount
    private void updateQtyAtAdminInfo(ParserResponse parserResponse, String doseAmt) {

        if (doseAmt != null) {
            Dose dose = DoseCalculator.parseDoseText(doseAmt);
            if (dose != null) {
                parserResponse.setQuantityPerAdmin(dose.getDoseValue());
                parserResponse.setFormatAtAdmin(dose.getDoseUnit());
            } else {
                parserResponse.setQuantityPerAdmin(doseAmt);
            }
        }
    }

    // Add frequency code to response from mapping table
    private void updateFrequencyCode(ParserResponse parserResponse, String frequency) {
        if (StringUtils.isNoneBlank(frequency)) {
            frequency = frequency.replaceAll("\\s+", "");
            if (parserCacheService.getFrequencyMap().containsKey(frequency)) {
                parserResponse.setFreqCode(parserCacheService.getFrequencyMap().get(frequency));
            } else if (parserCacheService.getFrequencyMap().containsValue(frequency)) {
                parserResponse.setFreqCode(frequency);
            }
        }
    }

    // calculating dose from intake / dose amount
    private void calculateDosePerAdmin(ParserResponse parserResponse) {
        //converting intake into dose
        //LOG.debug("calculateDosePerAdmin : " + parserResponse.getDosePerAdmin() + " : " + parserResponse.getQuantityPerAdmin() + " : " + parserResponse.getFormatAtAdmin() + " : " + parserResponse.getStrength());
        if (StringUtils.isBlank(parserResponse.getDosePerAdmin())
                && StringUtils.isNotBlank(parserResponse.getQuantityPerAdmin())
                && StringUtils.isNotBlank(parserResponse.getFormatAtAdmin())
                && StringUtils.isNotBlank(parserResponse.getStrength())) {

            try {
                parserResponse.setDosePerAdmin(DoseCalculator.calculateMedicationTotalParsedDoseValue(parserResponse.getStrength(), new BigDecimal(parserResponse.getQuantityPerAdmin())));
                parserResponse.setUnitAtAdmin(DoseCalculator.getMedicationDoseUnit(parserResponse.getStrength()));
            } catch (Exception e) {
                LOG.debug("Error while calculating dose from intake : " + e.getMessage());
            }
        }
    }

    private boolean isCreamOrLiquid(String format, String strength) {
        if ("cream".equalsIgnoreCase(format) || "nasal spray".equalsIgnoreCase(format)
                || (strength != null && strength.contains("%"))) {
            return true;
        }
        return false;
    }
}
