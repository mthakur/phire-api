package org.phire.api.parser.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.phire.api.parser.dto.VigilNomProdPlusJson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Named;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


@Stateless
@Named(value = "vigilanceDrugService")
public class VigilanceDrugService {

    final static GsonBuilder builder = new GsonBuilder();
    final static Gson gson = builder.create();
    static final Logger LOG = LoggerFactory.getLogger(VigilanceDrugService.class);

    public VigilNomProdPlusJson search(String key) {

        try {

            //URL url = new URL("https://adexpert-stag.mchi.mcgill.ca/drug_mgt/ws/drug/din/" + key.toString());
            URL url = new URL("http://132.206.151.177:6080/drug_mgt/ws/drug/din/" + key.toString());
            //URL url = new URL("https://adexpert-stag.mchi.mcgill.ca/drug_mgt/ws/drug/din/" + key.toString()); // stag
            LOG.info("vigilance url : " + url.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("jsse.enableSNIExtension", "false");
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            VigilNomProdPlusJson jsonObject = null;

            if (conn.getResponseCode() == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                StringBuilder builder = new StringBuilder();

                String aux = "";
                while ((aux = br.readLine()) != null) {
                    builder.append(aux);
                }

                jsonObject = gson.fromJson(builder.toString(), VigilNomProdPlusJson.class);
                conn.disconnect();
                return jsonObject;
            }

        } catch (Exception exception) {
            LOG.error("Error while fetching drug info from vigilance.");
            exception.printStackTrace();
        }
        return null;
    }
}