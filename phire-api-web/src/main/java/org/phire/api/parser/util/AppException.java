package org.phire.api.parser.util;

import javax.ws.rs.core.Response;

public class AppException extends Exception {

    private Response.Status code;

    public AppException(Response.Status code, String message) {
        super(message);
        this.code = code;
    }

    public Response.Status getCode() {
        return code;
    }

    public void setCode(Response.Status code) {
        this.code = code;
    }
}
