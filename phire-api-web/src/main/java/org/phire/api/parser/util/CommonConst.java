package org.phire.api.parser.util;

public class CommonConst {

    public static final String MED = "MED";
    public static final String ALLERGY = "ALLERGY";
    public static final String FAMILY_HISTORY = "FAMILY HISTORY";
    public static final String LABS = "LABS";
    public static final String NOMED = "NOMED";
    public static final String INDICATION = "INDICATION";
    public static final String TUNIT = "TUNIT";
    public static final String DOSEUNIT = "DOSEUNIT";
    public static final String UNIT = "UNIT";
    public static final String DBN = "DBN";
    public static final String MDBN = "MDBN";
    public static final String DPN = "DPN";
    public static final String DSCD = "DSCD";
    public static final String DSCDC = "DSCDC";
    public static final String DSCDF = "DSCDF";

    public static final String DIN = "DIN";
    public static final String DOSE = "DOSE";
    public static final String DOSEAMT = "DOSEAMT";
    public static final String FREQ = "FREQ";
    public static final String RUT = "RUT";
    public static final String ROUTE = "ROUTE";
    public static final String FORMAT = "FORMAT";
    public static final String DIRECTIVE = "DIRECTIVE";
    public static final String ADD_DIRECTIVE = "ADD-DIRECTIVE";
    public static final String DURATION = "DURATION";
    public static final String PRN = "PRN";
    public static final String DDF = "DDF";
    public static final String REPEAT = "REPEAT";
    public static final String NEC = "NEC";
    public static final String DRT = "DRT";
    public static final String TK = "TK";
    public static final String NA = "NA";

    public static final String DRUG = "DRUG";
    public static final String DOSEAMOUNT = "DOSEAMOUNT";
    public static final String STRENGTH = "STRENGTH";
    public static final String FORM = "FORM";
    public static final String NECESSITY = "NECESSITY";
    public static final String FREQUENCY = "FREQUENCY";

}
