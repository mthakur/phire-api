package org.phire.api.parser.util;

public class Dose {

    String doseValue;
    String doseUnit;

    public String getDoseValue() {
        return doseValue;
    }

    public void setDoseValue(String doseValue) {
        this.doseValue = doseValue;
    }

    public String getDoseUnit() {
        return doseUnit;
    }

    public void setDoseUnit(String doseUnit) {
        this.doseUnit = doseUnit;
    }

    @Override
    public String toString() {
        return "Dose{" +
                "doseValue='" + doseValue + '\'' +
                ", doseUnit='" + doseUnit + '\'' +
                '}';
    }
}
