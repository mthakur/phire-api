package org.phire.api.parser.util;

import org.phire.api.parser.util.NLPTools.Global;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

public class Lexicon {

    private Vector<String> lexiconList;
    private Vector<String> semanticList;

    public Lexicon(List<org.phire.api.parser.model.Lexicon> lexiconList) throws IOException {

        this.lexiconList = new Vector<String>();
        this.semanticList = new Vector<String>();

        for (org.phire.api.parser.model.Lexicon lexicon : lexiconList) {

            String name = lexicon.getName();
            String type = lexicon.getType();

            if (name.length() == 0) {
                continue;
            }
            name = name.trim() + ' ';
            String normTerm = this.tokenization(name);
            this.lexiconList.add(normTerm);
            this.semanticList.add(type);
            String cTerm = this.removeSpecialSymbol(normTerm);

            if (!cTerm.equals(normTerm)) {
                this.lexiconList.add(cTerm);
                this.semanticList.add(type);
            }
        }
    }

    private String removeSpecialSymbol(String str) {

        int startPos = 0;
        int llen = str.length();

        while (startPos < llen && (this.removableSymbol(str.charAt(startPos)) || str.charAt(startPos) == ' ')) {
            startPos = startPos + 1;
        }

        int endPos = llen - 1;
        while (endPos >= 0 && (this.removableSymbol(str.charAt(endPos)) || str.charAt(endPos) == ' ')) {
            endPos = endPos - 1;
        }

        if (endPos >= startPos) {
            return str.substring(startPos, endPos + 1);
        } else {
            return "";
        }
    }

    private boolean removableSymbol(char ch) {
        return ch == '-' || ch == '+' || ch == '/' || ch == '\\' || ch == '@' || ch == '&';
    }

    private String tokenization(String txt) {

        int length = txt.length();
        int tokenChStart = 0; // set new start only when new token added
        int curPos = 0;

        StringBuffer normTxt = new StringBuffer();

        while (curPos < length & (txt.charAt(curPos) == ' ' || txt.charAt(curPos) == '\n')) {
            curPos = curPos + 1;
        }
        tokenChStart = curPos;

        while (curPos < length) {
            char ch = txt.charAt(curPos);

            // current char is a separator
            if (Global.Util.is_sep(ch)) {
                appendText(curPos, tokenChStart, txt, normTxt);
                curPos = curPos + 1;

                while (curPos < length && txt.charAt(curPos) == ' ') {
                    curPos = curPos + 1;
                }
                tokenChStart = curPos;

            } else if (Global.Util.is_punctuation(ch) || Global.Util.is_braces(ch)) {
                // current token is a punctuation or braces
                appendText(curPos, tokenChStart, txt, normTxt);
                // add current punctuation as token
                normTxt.append(String.valueOf(ch) + ' ');
                curPos = curPos + 1;

                while (curPos < length && txt.charAt(curPos) == ' ') {
                    curPos = curPos + 1;
                }
                tokenChStart = curPos;
            } else {
                // current token is not a token boundary
                curPos = curPos + 1;
            }
        }
        return normTxt.toString().trim();
    }

    private void appendText(int curPos, int tokenChStart, String txt, StringBuffer normTxt) {
        if (curPos > tokenChStart) {
            String str = txt.substring(tokenChStart, curPos);
            normTxt.append(str + ' ');
        }
    }

    public Vector<String> lexList() {
        return this.lexiconList;
    }

    public Vector<String> semList() {
        return this.semanticList;
    }
}
