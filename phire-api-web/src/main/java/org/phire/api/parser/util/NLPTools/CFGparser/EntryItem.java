package org.phire.api.parser.util.NLPTools.CFGparser;

public class EntryItem {

    protected String sign;

    public boolean equals(Object o) {
        if (o instanceof EntryItem) {
            if (sign.equals(((EntryItem) o).getSign())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return 7;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
