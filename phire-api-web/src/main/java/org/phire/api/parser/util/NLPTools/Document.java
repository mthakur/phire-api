package org.phire.api.parser.util.NLPTools;

import org.apache.commons.lang3.StringUtils;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.phire.api.parser.util.medex.DrugTag;

import java.util.*;

public class Document implements Global {

    private boolean sbdFlag;
    private Vector<Token> tokenVct = null;// original token
    private Sentence _sentence = null;// original sentences
    private Vector<Sentence> sentence = null;// original sentences
    private Map<Integer, Integer> startMap = null;
    private Map<Integer, Integer> endMap = null;

    // original token ,sentence and map
    private Vector<Token> originalTokenVct = null;// original token
    private Vector<Sentence> originalSentence = null;// original sentences
    private Map<Integer, Integer> originalStartMap = null;
    private Map<Integer, Integer> originalEndMap = null;
    private Map<Integer, Integer> originalToken2startMap;
    private Map<Integer, Integer> originalToken2endMap;
    private String originalNormStr = null;

    // token, sentence, map after sentence boundary
    private Vector<Token> boundaryTokenVct = null;// Tokens after sentence
    // boundary
    private Vector<Sentence> boundarySentence = null;// after sentence boundary
    // map position in boundary_norm_str to token index
    private Map<Integer, Integer> boundaryStartMap = null;
    private Map<Integer, Integer> boundaryEndMap = null;
    // map token index into position in boundary_norm_str
    private Map<Integer, Integer> boundaryToken2startMap;
    private Map<Integer, Integer> boundaryToken2endMap;
    private String boundary_norm_str = null;

    private String originalTxt;
    private List<DrugTag> drugTags;
    private Vector<DrugTag> filteredDrugTags;
    private HashMap<Integer, Pair<String, Integer>> tokenTags;
    private Vector<Vector<DrugTag>> signature;

    //Added After
    ArrayList<Quartet<String, String, Integer, Integer>> updated_token_info;


    public Document(String itxt) {


        if (StringUtils.isBlank(itxt)) {
            return;
        }

        //Step to de-normalize the frequency, separating time from frequency

        this.sbdFlag = false;
        //this.fname = fname;

        // initiate original
        this.originalTokenVct = new Vector<Token>();
        this.originalSentence = new Vector<Sentence>();
        this.originalStartMap = new HashMap<Integer, Integer>();
        this.originalEndMap = new HashMap<Integer, Integer>();
        this.originalToken2startMap = new HashMap<Integer, Integer>();
        this.originalToken2endMap = new HashMap<Integer, Integer>();
        this.originalNormStr = "";

        // initiate boundary
        this.boundaryTokenVct = new Vector<Token>();
        this.boundarySentence = new Vector<Sentence>();
        this.boundaryStartMap = new HashMap<Integer, Integer>();
        this.boundaryEndMap = new HashMap<Integer, Integer>();
        this.boundaryToken2startMap = new HashMap<Integer, Integer>();
        this.boundaryToken2endMap = new HashMap<Integer, Integer>();
        this.boundary_norm_str = "";

        // initiate tags
        this.drugTags = new ArrayList<DrugTag>();
        this.filteredDrugTags = new Vector<DrugTag>();
        this.tokenTags = new HashMap<Integer, Pair<String, Integer>>();
        this.signature = new Vector<Vector<DrugTag>>();
        this.updated_token_info = new ArrayList<Quartet<String, String, Integer, Integer>>();

        // set default token, sentence and map
        this.set_default_TSM();

        if (itxt == null || itxt.length() == 0) {
            //LOG.error("Empty string");
            return;
        }
        this.originalTxt = itxt;
        String txt = "";
        if (itxt.charAt(itxt.length() - 1) == '\n') {
            txt = itxt;
        } else {
            txt = itxt + '\n';
        }


        int token_ch_start = 0; // set new start only when new token added
        int sent_token_start = 0; // set new sent token start when new sentence
        // added
        int sent_ch_start = 0; // set new sent_ch_start when new sentence added
        int cur_pos = 0;
        int llen = txt.length();
        while (cur_pos < llen
                & (txt.charAt(cur_pos) == ' ' || txt.charAt(cur_pos) == '\n' || txt.charAt(cur_pos) == '\r')) {
            cur_pos = cur_pos + 1;
        }
        token_ch_start = cur_pos;

        while (cur_pos < llen) {

            char ch = txt.charAt(cur_pos);

            if (Util.is_sep(ch)) {

                if (cur_pos > token_ch_start) {
                    int spos = token_ch_start;
                    int epos = cur_pos;
                    add_original_vct(txt, spos, epos);
                }

                if (ch == '\n') {

                    Sentence sent = new Sentence();
                    sent.setAbsStart(sent_ch_start);
                    sent.setStartTokenIndex(sent_token_start);

                    sent.setAbsEnd(this.originalTokenVct.get(this.originalTokenVct.size() - 1).getAbsEndPos());
                    sent.setEndTokenIndex(this.originalTokenVct.size() - 1);
                    this.originalSentence.add(sent);

                    cur_pos = cur_pos + 1;
                    while (cur_pos < llen && (txt.charAt(cur_pos) == ' ' || txt.charAt(cur_pos) == '\n')) {
                        cur_pos = cur_pos + 1;
                    }
                    sent_token_start = this.originalTokenVct.size();
                    sent_ch_start = cur_pos;

                } else {
                    cur_pos = cur_pos + 1;
                    while (cur_pos < llen && txt.charAt(cur_pos) == ' ') {
                        cur_pos = cur_pos + 1;
                    }
                }
                token_ch_start = cur_pos;

            } else if (Util.is_punctuation(ch) || Util.is_braces(ch)) {

                if (cur_pos > token_ch_start) {
                    int spos = token_ch_start;
                    int epos = cur_pos;
                    add_original_vct(txt, spos, epos);
                }

                int spos = cur_pos;
                int epos = cur_pos + 1;
                add_original_vct(txt, spos, epos);
                cur_pos = cur_pos + 1;

                while (cur_pos < llen && txt.charAt(cur_pos) == ' ') {
                    cur_pos = cur_pos + 1;
                }
                token_ch_start = cur_pos;

            } else {
                cur_pos = cur_pos + 1;
            }
        }
        build_original_norm_str_map();
    }

    // set the default token, sentence and map to the original parameters,
    // before sentence boundary
    private void set_default_TSM() {
        this.tokenVct = this.originalTokenVct;
        this.sentence = this.originalSentence;
        this.startMap = this.originalStartMap;
        this.endMap = this.originalEndMap;
    }

    // set the default token, sentence and map to the boundary parameters
    private void set_boundary_TSM() {
        this.tokenVct = this.boundaryTokenVct;
        this.sentence = this.boundarySentence;
        this.startMap = this.boundaryStartMap;
        this.endMap = this.boundaryEndMap;
    }

    // private functions

    /**
     * add token into vector
     */
    private void add_original_vct(String txt, int spos, int epos) {
        String str = txt.substring(spos, epos);

        if (Util.is_dot_word(str)) {
            str = ".";
            Token ttok2 = new Token(spos, spos + 1, TextSectionType.TOKEN, str);
            this.originalTokenVct.add(ttok2);
            str = txt.substring(spos + 1, epos);
            Token ttok3 = new Token(spos + 1, epos, TextSectionType.TOKEN, str);
            this.originalTokenVct.add(ttok3);
        } else if (Util.is_unit(str)) {
            int tpos = str.length() - 1;
            while (!Character.isDigit(str.charAt(tpos))) {
                tpos = tpos - 1;
            }
            str = txt.substring(spos, spos + tpos + 1);
            Token ttok2 = new Token(spos, spos + tpos + 1, TextSectionType.TOKEN, str);
            this.originalTokenVct.add(ttok2);
            str = txt.substring(spos + tpos + 1, epos);
            Token ttok3 = new Token(spos + tpos + 1, epos, TextSectionType.TOKEN, str);
            this.originalTokenVct.add(ttok3);
        } else {
            Token ttok2 = new Token(spos, epos, TextSectionType.TOKEN, str);
            this.originalTokenVct.add(ttok2);
        }
    }

    /**
     * build original normalized string from the token list by join the tokens
     * with a space
     */
    private void build_original_norm_str_map() {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < this.originalSentence.size(); i++) {
            for (int j = this.originalSentence.get(i).startTokenIndex(); j <= this.originalSentence.get(i).endTokenIndex(); j++) {
                Token tok = this.originalTokenVct.get(j);
                this.originalStartMap.put(result.length(), j);
                this.originalEndMap.put(result.length() + tok.getText().length(), j);
                this.originalToken2startMap.put(j, result.length());
                this.originalToken2endMap.put(j, result.length() + tok.getText().length());
                String word = tok.getText();

                if (word.charAt(word.length() - 1) == '.') {
                    this.originalEndMap.put(result.length() + tok.getText().length() - 1, j);
                }
                result.append(this.originalTokenVct.get(j).getText() + ' ');
            }
            int len = result.length();
            result.setCharAt(len - 1, '\n');
        }
        this.originalNormStr = result.toString();
    }

    public String get_boundary_map_str() {
        StringBuffer buf = new StringBuffer();
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < this.boundarySentence.size(); i++) {
            for (int j = this.boundarySentence.get(i).startTokenIndex(); j <= this.boundarySentence.get(i)
                    .endTokenIndex(); j++) {

                buf.append(this.boundaryTokenVct.get(j).getText() + "\t" + result.length());

                result.append(this.boundaryTokenVct.get(j).getText() + ' ');
                buf.append("\t" + (result.length() - 1));
                buf.append("\t" + this.boundaryTokenVct.get(j).getAbsStartPos() + "\t"
                        + this.boundaryTokenVct.get(j).getAbsEndPos() + "\n");

            }
            buf.append("\n");
        }

        return buf.toString();
    }

    public void set_sbd_flag(boolean flag) {
        this.sbdFlag = flag;
        this.set_boundary_TSM();
    }

    public void filterOverlappedDrugTag() {

        if (this.drugTags.size() == 0) {
            return;
        } else {
            Section[] secs = new Section[this.drugTags.size()];

            for (int i = 0; i < this.drugTags.size(); i++) {
                secs[i] = new Section();
                secs[i].start = new Integer(0);
                secs[i].end = new Integer(0);
                secs[i].start = this.drugTags.get(i).getAbsStartPos();
                secs[i].end = this.drugTags.get(i).getAbsEndPos();
                secs[i].str = this.originalTxt.substring(secs[i].start, secs[i].end);
            }
            ArrayIndexComparator cmp = new ArrayIndexComparator(secs);
            Integer[] indices = cmp.createIndexArray();
            Arrays.sort(indices, cmp);

            int pre = -1;
            for (int i = 0; i < indices.length; i++) {

                if (i == 0) {
                    pre = 0;
                } else {
                    DrugTag dt1 = this.drugTags.get(indices[pre]);
                    DrugTag dt2 = this.drugTags.get(indices[i]);
                    if (this.over_lap(dt1, dt2)) {
                        if (dt2.getText().length() > dt1.getText().length()) {
                            pre = i;
                        }
                    } else {
                        DrugTag ndt = new DrugTag(dt1);
                        this.filteredDrugTags.add(ndt);
                        pre = i;
                    }
                }
            }
            // add the last drugtag
            DrugTag dt1 = this.drugTags.get(indices[pre]);
            DrugTag ndt = new DrugTag(dt1);
            this.filteredDrugTags.add(ndt);
        }
    }

    /**
     * Judge if two section overlapped
     */
    public boolean over_lap(DrugTag dt1, DrugTag dt2) {
        boolean flag = false;
        if (dt2.getAbsStartPos() < dt1.getAbsEndPos()) {
            flag = true;
        }
        return flag;
    }

    /**
     * Add DrugTag class into vector 'drug_tags'
     */
    public void addDrugTag(DrugTag dt) {
        this.drugTags.add(dt);
    }

    /**
     * Add the signature to Document class. The signature is generated after
     * rule engine and parser.
     */
    public void add_signature(Vector<DrugTag> dt) {
        this.signature.add(dt);
    }

    public Vector<Vector<DrugTag>> signature() {
        return this.signature;
    }

    public String originalTxt() {
        return this.originalTxt;
    }

    public Vector<Sentence> sentence() {
        return this.sentence;
    }

    public Vector<Sentence> boundary_sentence() {
        return this.boundarySentence;
    }

    public String norm_str() {
        if (this.sbdFlag) {
            return this.boundary_norm_str;
        } else {
            return this.originalNormStr;
        }

    }

    public String boundary_norm_str() {
        return this.boundary_norm_str;

    }

    public void print() {
        for (int i = 0; i < this.sentence.size(); i++) {
            this.sentence.get(i).print();
        }
    }

    public Map startMap() {
        return this.startMap;
    }

    public Map endMap() {
        return this.endMap;
    }

    public Map boundaryStartMap() {
        return this.boundaryStartMap;
    }

    public Map boundaryEndMap() {
        return this.boundaryEndMap;
    }

    public Vector<Token> token_vct() {
        return this.tokenVct;
    }

    public Vector<Token> boundary_token_vct() {
        return this.boundaryTokenVct;
    }

    public String getStrByToken(int start, int end) {
        return this.originalTxt.substring(this.tokenVct.get(start).getAbsStartPos(), this.tokenVct.get(end).getAbsEndPos());
    }

    public List<DrugTag> drugTag() {
        return this.drugTags;
    }

    public Vector<DrugTag> filteredDrugTag() {
        return this.filteredDrugTags;
    }

    public void add_boundary_token(Token token) {
        this.boundaryTokenVct.add(token);
    }

    public void set_boundary_norm_str(String str) {
        this.boundary_norm_str = str;
    }

    public void add_boundary_start_map(int key, int val) {
        this.boundaryStartMap.put(key, val);
    }

    public void add_boundary_token2start_map(int key, int val) {
        this.boundaryToken2startMap.put(key, val);
    }

    public void add_boundary_end_map(int key, int val) {
        this.boundaryEndMap.put(key, val);
    }

    public void add_boundary_token2end_map(int key, int val) {
        this.boundaryToken2endMap.put(key, val);
    }

    public Sentence get_sentence() {
        return _sentence;
    }

    public void set_sentence(Sentence _sentence) {
        this._sentence = _sentence;
    }

    public HashMap<Integer, Pair<String, Integer>> getTokenTags() {
        return tokenTags;
    }

    public void setTokenTags(HashMap<Integer, Pair<String, Integer>> tokenTags) {
        this.tokenTags = tokenTags;
    }

    public ArrayList<Quartet<String, String, Integer, Integer>> getUpdated_token_info() {
        return updated_token_info;
    }

    public void setUpdated_token_info(ArrayList<Quartet<String, String, Integer, Integer>> updated_token_info) {
        this.updated_token_info = updated_token_info;
    }
}
