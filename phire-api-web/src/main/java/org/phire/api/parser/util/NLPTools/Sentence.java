package org.phire.api.parser.util.NLPTools;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import java.util.Vector;

public class Sentence {

//    private static final Logger LOG = LoggerFactory.getLogger(Sentence.class);

    private int start_token_index;
    private int end_token_index;
    private int absStartPos = -1;
    private int absEndPos = -1;
    private Vector<TextSection> tokens;

    public Sentence() {
    }

    public void setTokens(Vector<TextSection> tags) {
        this.tokens = tags;
    }

    public Vector<TextSection> getTokens() {
        return tokens;
    }

    public void setAbsStart(int pos) {
        this.absStartPos = pos;
    }

    public void setAbsEnd(int pos) {
        this.absEndPos = pos;
    }

    public void setStartTokenIndex(int pos) {
        this.start_token_index = pos;
    }

    public int startTokenIndex() {
        return this.start_token_index;
    }

    public int endTokenIndex() {
        return this.end_token_index;
    }

    public void setEndTokenIndex(int pos) {
        this.end_token_index = pos;
    }

    public int absStart() {
        return this.absStartPos;
    }

    public int absEnd() {
        return this.absEndPos;
    }

    public int token_num() {
        return this.end_token_index - this.start_token_index + 1;
    }

    public int str_lenth() {
        return this.absEndPos - this.absStartPos;
    }

    public void print() {
        if (tokens == null) {
            return;
        }
        for (TextSection textSection : tokens) {
       //     LOG.debug("\t" + textSection.str());
        }
        //LOG.debug("--");
    }
}
