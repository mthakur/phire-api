package org.phire.api.parser.util.NLPTools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

public class SentenceBoundary implements Global {

    private final static String WORDS_FILE = "word.txt";
    private final static String ABBR_FILE = "abbr.txt";
    private Document doc;
    private Map<String, Integer> word_map;
    private Map<String, Integer> abbr_map;
    private StringBuffer sbuf;

    public SentenceBoundary() {

        this.word_map = new HashMap<String, Integer>();
        this.abbr_map = new HashMap<String, Integer>();
        this.sbuf = new StringBuffer();

        try {
            // load english words from word_fname

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(WORDS_FILE).getFile());
            BufferedReader br = new BufferedReader(new FileReader(file));
            String strLine;
            StringBuffer lines = new StringBuffer();
            int count = 0;

            while ((strLine = br.readLine()) != null) {
                this.word_map.put(strLine.trim().toLowerCase(), count);
                count = count + 1;
            }
            br.close();

            classLoader = getClass().getClassLoader();
            file = new File(classLoader.getResource(ABBR_FILE).getFile());
            br = new BufferedReader(new FileReader(file));
            lines.setLength(0);

            count = 0;
            while ((strLine = br.readLine()) != null) {
                this.abbr_map.put(strLine.trim().toLowerCase(), count);
                count = count + 1;
            }
            br.close();

        } catch (Exception e) {
            System.out.println("Error : " + e.getMessage());
        }
    }

    // private founctions

    private void add_token(Token tok) {

        this.doc.add_boundary_token(tok);
        int tmp = this.doc.boundary_token_vct().size() - 1;
        this.doc.add_boundary_start_map(this.sbuf.length(), tmp);
        this.doc.add_boundary_end_map(this.sbuf.length() + tok.getText().length(), tmp);
        this.doc.add_boundary_token2start_map(tmp, this.sbuf.length());
        this.doc.add_boundary_token2end_map(tmp, this.sbuf.length() + tok.getText().length());
        // Build map index for partially match. The partially match will
        // consider the partially match correct, when there is only one '.' at
        // the end
        // E.g., 'p.o' match for 'p.o.'
        //

        String word = tok.getText();

        if (word.charAt(word.length() - 1) == '.') {
            this.doc.add_boundary_end_map(this.sbuf.length() + tok.getText().length() - 1, tmp);
        }
        this.sbuf.append(tok.getText() + ' ');

    }

    private void add_boundary() {
        this.sbuf.setCharAt(this.sbuf.length() - 1, '\n');
    }

    // public functions

    /**
     * The main process to detect sentence boundaries.
     */
    public void detect_boundaries(Document doc) {

        //System.out.println("-------- detect_boundaries START ---------");
        // System.out.println("Start detect boundary");
        this.doc = doc;
        //System.out.println("set sbd");

        this.sbuf.setLength(0);
        String word = "";

        Sentence sent = new Sentence();
        sent.setAbsStart(0);
        sent.setStartTokenIndex(0);
        //System.out.println("-------------- this.doc ------------------ " + this.doc);
        //System.out.println("original_txt : " + this.doc.original_txt());
        //System.out.println("# sentence : " + this.doc.sentence().size());

        for (int i = 0; i < this.doc.sentence().size(); i++) {

            Sentence sent2 = this.doc.sentence().get(i);

            //System.out.println("abs start : " + sent2.absStart() + " abs end : " + sent2.absEnd());
            //System.out.println("original : " + this.doc.original_txt().substring(sent2.absStart(), sent2.absEnd()));

            int start = this.doc.sentence().get(i).startTokenIndex();
            int ends = this.doc.sentence().get(i).endTokenIndex();

            for (int j = start; j <= ends; j++) {
                //System.out.println("-------- " + j + "---------");
                boolean sent_flag = false;

                Token token = this.doc.token_vct().get(j);
                //System.out.println("current token : " + token.getText());

                Token next_token = null;
                word = token.getText();
                String next_word = "";

                if (j + 1 < this.doc.token_vct().size()) {
                    next_token = this.doc.token_vct().get(j + 1);
                    next_word = next_token.getText();
                }

                if (next_token != null) {
                    //System.out.println("   next token : " + next_token.getText());
                }

                //System.out.println(word + " list : " + Global.Util.is_list(word));
                if (word == "." || Util.is_list(word)) {
                    // situations that keep the original token
                    //System.out.println("token2 : " + next_token.getText());
                    Token tok = new Token(token);
                    this.add_token(tok);
                    if (word == ".") {
                        sent_flag = true;
                    } else {
                        //System.out.println("list : " + word);
                    }
                } else {
                    // handle the '.' within word
                    int dot_num = Util.dot_num(word);
                    if (dot_num > 0) {
                        //System.out.println(word + "----- has dot num : " + dot_num);

                        if (dot_num == 1) {
                            // dot is the first symbol of current word
                            if (word.charAt(0) == '.') {
                                //System.out.println("1 dot begining:");
                                Token tok = new Token(token);
                                this.add_token(tok);
                            } else if (word.charAt(word.length() - 1) == '.') {
                                //System.out.println("1 dot at end: next word|" + next_word);

                                if ((next_token != null) && !Util.is_sentence_start(next_word)
                                        && (this.abbr_map.containsKey(word.toLowerCase()) || this.abbr_map
                                        .containsKey(word.substring(0, word.length() - 1).toLowerCase()))) {
                                    //System.out.println("part of abbr");
                                    Token tok = new Token(token);
                                    this.add_token(tok);
                                }
                                // dot is period, not part of abbreviation
                                else {
                                    //System.out.println("period dot");
                                    Token tok = new Token(token.getAbsStartPos(), token.getAbsEndPos() - 1,
                                            TextSectionType.TOKEN, word.substring(0, word.length() - 1));
                                    this.add_token(tok);

                                    Token tok2 = new Token(token.getAbsEndPos() - 1, token.getAbsEndPos(),
                                            TextSectionType.TOKEN, ".");
                                    this.add_token(tok2);

                                    sent_flag = true;
                                }
                            } else { // dot is in the center of the token
                                //System.out.println("----- dot in center:" + word);
                                int pos = word.indexOf(".");
                                String lword = word.substring(0, pos);
                                String rword = word.substring(pos + 1);
                                //System.out.println("rword : " + rword);
                                if (Character.isUpperCase(rword.charAt(0))
                                        && (this.word_map.containsKey(rword.toLowerCase()))) {
                                    // split the token by dot
                                    // add left part word
                                    Token tok = new Token(token.getAbsStartPos(), token.getAbsStartPos() + pos,
                                            TextSectionType.TOKEN, lword);
                                    this.add_token(tok);

                                    Token tok2 = new Token(token.getAbsStartPos() + pos, token.getAbsStartPos() + pos + 1,
                                            TextSectionType.TOKEN, ".");
                                    this.add_token(tok2);

                                    // add new sentence
                                    this.add_boundary();
                                    //System.out.println("new sentence added!!");
                                    sent.setAbsEnd(token.getAbsEndPos());
                                    sent.setEndTokenIndex(this.doc.boundary_token_vct().size() - 1);
                                    this.doc.boundary_sentence().add(sent);
                                    sent = new Sentence();
                                    if (next_token != null) {
                                        sent.setAbsStart(next_token.getAbsStartPos());
                                        sent.setStartTokenIndex(this.doc.boundary_token_vct().size());
                                    }

                                    // add right part word
                                    Token tok3 = new Token(token.getAbsStartPos() + pos + 1, token.getAbsEndPos(),
                                            TextSectionType.TOKEN, rword);
                                    this.add_token(tok3);

                                } else {
                                    Token tok = new Token(token);
                                    this.add_token(tok);
                                }

                            }
                        } else {// current token has more than one dot
                            //System.out.println("more than one dot : " + word);
                            if (word.charAt(word.length() - 1) == '.') {
                                String lword = word.substring(0, word.length() - 1);
                                if ((Util.is_digit(lword))
                                        || (next_word.length() > 0 && Character.isUpperCase(next_word.charAt(0)))) {
                                    Token tok = new Token(token.getAbsStartPos(), token.getAbsStartPos() + lword.length(),
                                            TextSectionType.TOKEN, lword);
                                    this.add_token(tok);

                                    Token tok2 = new Token(token.getAbsStartPos() + lword.length(),
                                            token.getAbsStartPos() + lword.length() + 1, TextSectionType.TOKEN, ".");
                                    this.add_token(tok2);
                                    sent_flag = true;
                                } else {
                                    Token tok = new Token(token);
                                    this.add_token(tok);
                                }

                            } else {
                                Token tok = new Token(token);
                                this.add_token(tok);
                            }
                        }

                    } else {
                        //System.out.println("normal word ,just add in : "+ token.getText());
                        Token tok = new Token(token);
                        this.add_token(tok);
                    }
                }

                if (sent_flag) {
                    this.add_boundary();
                    //System.out.println("new sentence added!!");
                    sent.setAbsEnd(token.getAbsEndPos());
                    sent.setEndTokenIndex(this.doc.boundary_token_vct().size() - 1);
                    this.doc.boundary_sentence().add(sent);
                    sent = new Sentence();
                    if (next_token != null) {
                        sent.setAbsStart(next_token.getAbsStartPos());
                        sent.setStartTokenIndex(this.doc.boundary_token_vct().size());
                    }

                }

            }
            // Tokens in current original sentence is finished, handle the hard
            // '\n' at the end of line
            // get the first word for next line
            //System.out.println("current setence ended. start:" + sent.absStart());
            if (this.doc.boundary_token_vct().get(this.doc.boundary_token_vct().size() - 1).getAbsEndPos() > sent.absStart()
                    && sent.absStart() >= 0) {
                //
                Token next_token = null;
                String next_word = "";
                if (i + 1 < this.doc.sentence().size()) {
                    next_token = this.doc.token_vct().get(this.doc.sentence().get(i + 1).startTokenIndex());
                    next_word = next_token.getText();
                }

                if (next_word.length() > 0
                        && (Character.isUpperCase(next_word.charAt(0)) || Util.is_list(next_word))) {
                    this.add_boundary();
                    sent.setAbsEnd(
                            this.doc.boundary_token_vct().get(this.doc.boundary_token_vct().size() - 1).getAbsEndPos());
                    sent.setEndTokenIndex(this.doc.boundary_token_vct().size() - 1);
                    //System.out.println("handle '/n' new sentence added!! " + sent.absStart() + " : " + sent.absEnd());
                    this.doc.boundary_sentence().add(sent);
                    sent = new Sentence();
                    if (next_token != null) {
                        sent.setAbsStart(next_token.getAbsStartPos());
                        sent.setStartTokenIndex(this.doc.boundary_token_vct().size());
                    }
                }
            }

        }
        if (this.doc.boundary_token_vct().get(this.doc.boundary_token_vct().size() - 1).getAbsStartPos() > sent.absStart()
                && sent.absStart() >= 0) {
            //System.out.println("last sentence added!!");
            sent.setAbsEnd(this.doc.boundary_token_vct().get(this.doc.boundary_token_vct().size() - 1).getAbsEndPos());
            sent.setEndTokenIndex(this.doc.boundary_token_vct().size() - 1);
            this.doc.boundary_sentence().add(sent);

        }
        this.doc.set_boundary_norm_str(this.sbuf.toString());
        //System.out.println("boundary detect end");

        this.doc.set_sbd_flag(true);
        //System.out.println("------------ detect_boundaries ENDS ---------");
    }

}
