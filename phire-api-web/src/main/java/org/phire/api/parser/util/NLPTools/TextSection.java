package org.phire.api.parser.util.NLPTools;

public abstract class TextSection implements Global {

    public TextSection(int spos, int epos, TextSectionType type, String text) {
        this.absStartPos = spos;
        this.absEndPos = epos;
        this.type = type;
        this.text = text;
    }

    //members
    private int absStartPos = -1;
    private int absEndPos = -1;
    private String text = "";
    private TextSectionType type = TextSectionType.NA;

    public int getAbsStartPos() {
        return absStartPos;
    }

    public int getAbsEndPos() {
        return absEndPos;
    }

    public String getText() {
        return text;
    }

    public TextSectionType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "TextSection{" +
                "absStartPos=" + absStartPos +
                ", absEndPos=" + absEndPos +
                ", text='" + text + '\'' +
                ", type=" + type +
                '}';
    }
}
