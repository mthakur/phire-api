package org.phire.api.parser.util.NLPTools;

public class Token extends TextSection implements Global {

    public Token(int spos, int epos, TextSectionType type, String str) {
        super(spos, epos, type, str);
    }

    public Token(Token token) {
        super(token.getAbsStartPos(), token.getAbsEndPos(), token.getType(), token.getText());
    }
}
