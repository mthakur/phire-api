package org.phire.api.parser.util;


import org.apache.commons.lang3.StringUtils;
import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class implements a regular expression based semantic parser
 */
public class RegexParser {

    private static final Logger LOG = LoggerFactory.getLogger(RegexParser.class);
    private static final String pattern = "((((DIN|DBN|DPN|DOSE|DDF| )+)|DPN|DSCDC|DSCDF|DSCD| )(DOSEAMT|DOSE|DDF|FREQ|RUT|TOD|DISA|REFL|DIRECTIVE|DRT|REPEAT|NEC|MDBN| )+)" +
            "|((DOSEAMT|DOSE|DDF|FREQ|RUT|REPEAT|TOD|DISA|REFL|DIRECTIVE|DRT|NEC|MDBN| )+ (DIN|DBN|DPN|DSCDC|DSCDF|DSCD))" +
            "|(DOSEAMT|DOSE|DDF|FREQ|RUT|REPEAT|TOD|DISA|REFL|DIRECTIVE|DRT|NEC|MDBN| )+" +
            "|((DOSEAMT|DOSE|DDF|FREQ|RUT|REPEAT|TOD|DISA|REFL|DIRECTIVE|DRT|NEC|MDBN| )+ (DIN|DBN|DPN|DSCDC|DSCDF|DSCD) (DOSEAMT|DOSE|DDF|FREQ|RUT|TOD|DISA|REFL|DIRECTIVE|DRT|REPEAT|NEC|MDBN| )+) +";

    /**
     * This method contains the main logic of the parser
     *
     * @param sentSemterms signature list need to be parsed
     * @return parsed signature structure
     * @see RegexParser
     */
    public String parsePrescriptionFormat(ArrayList<Pair<String, String>> sentSemterms) {

        String tagConcat = "";
        ArrayList<String> tagList = new ArrayList<String>();
        ArrayList<Pair<String, String>> tagTermPairs = new ArrayList<Pair<String, String>>();

        for (Pair<String, String> stringPair : sentSemterms) {
            String tag = stringPair.getValue0();
            String term = stringPair.getValue1();
            tagList.add(tag);
            tagTermPairs.add(Pair.with(tag, term));
        }

        for (int i = 0; i < tagList.size(); i++) {
            tagConcat = tagConcat + " " + tagList.get(i);
        }

        tagConcat = tagConcat.trim();
        Pattern patternOne = Pattern.compile(pattern);
        LOG.debug("matcher tag : " + tagConcat + " = " + tagTermPairs + " |\t" + tagTermPairs.size());
        Matcher matcherOne = patternOne.matcher(tagConcat);

        if (matcherOne.find()) {
            String match = matcherOne.group();
            tagConcat = getTagTermPairs(match, tagTermPairs);
        }
        return tagConcat;
    }

    public List<Pair<String, String>> parsePrescriptionFormats(ArrayList<Pair<String, String>> sentSemterms) {

        List<Pair<String, String>> pairList = null;
        String tagConcat = "";
        List<Pair<String, String>> returnTagTerm = null;
        ArrayList<String> tagList = new ArrayList<String>();
        ArrayList<Pair<String, String>> tagTermPairs = new ArrayList<Pair<String, String>>();

        for (Pair<String, String> stringPair : sentSemterms) {
            String tag = stringPair.getValue0();
            String term = stringPair.getValue1();
            tagList.add(tag);
            tagTermPairs.add(Pair.with(tag, term));
        }

        for (int i = 0; i < tagList.size(); i++) {
            tagConcat = tagConcat + " " + tagList.get(i);
        }

        tagConcat = tagConcat.trim();
        Pattern patternOne = Pattern.compile(pattern);
        LOG.debug("matcher tag : " + tagConcat + " = " + tagTermPairs + " |\t" + tagTermPairs.size());
        Matcher matcherOne = patternOne.matcher(tagConcat);

        if (matcherOne.find()) {
            String match = matcherOne.group();
            pairList = getTagTermPairList(match, tagTermPairs);
        }
        return pairList;
    }

    /**
     * @param match        i.e. "RUT DBN"
     * @param tagTermPairs "[[RUT, Pt], [DBN, Asacol], [TOD, 9am], [TOD, morning]]"
     * @return "RUT FFF Pt	DBN FFF Asacol"
     */
    private static String getTagTermPairs(String match, ArrayList<Pair<String, String>> tagTermPairs) {


        if (StringUtils.isBlank(match) || tagTermPairs == null || tagTermPairs.isEmpty()) {
            return StringUtils.EMPTY;
        }

        String returnTagTerm = StringUtils.EMPTY;
        ArrayList<Integer> tagPos = new ArrayList<Integer>();
        String[] matchTagArray = match.split(" ");
        ArrayList<String> matchTagList = new ArrayList<String>();

        for (String matchStr : matchTagArray) {
            if (StringUtils.isNoneBlank(matchStr)) {
                matchTagList.add(matchStr);
            }
        }

        for (int i = 0; i < tagTermPairs.size(); i++) {
            if (tagTermPairs.get(i).getValue0().equals(matchTagList.get(0))) {
                tagPos.add(i);
            }
        }

        for (int i = 0; i < tagPos.size(); i++) {

            int start = tagPos.get(i);
            int matchLen = 0;
            String tagTerm = "";
            ArrayList<String> tempTagTermList = new ArrayList<String>();

            if ((start + matchTagList.size()) <= tagTermPairs.size()) {
                int loop_ctr = 0;

                while (loop_ctr < matchTagList.size()) {
                    if (matchTagList.get(loop_ctr).equals(tagTermPairs.get(start).getValue0())) {
                        String tag = tagTermPairs.get(start).getValue0();
                        String term = tagTermPairs.get(start).getValue1();
                        tagTerm = tag + " " + "FFF" + " " + term;
                        tempTagTermList.add(tagTerm);
                        matchLen++;
                    }
                    start++;
                    loop_ctr++;
                }
                    }

            if (matchLen == matchTagList.size()) {
                String str = "";
                for (int j = 0; j < tempTagTermList.size(); j++) {
                    str = str + tempTagTermList.get(j) + "\t";
                }
                returnTagTerm = str;
                break;
            }
        }
        return returnTagTerm;
    }

    private static List<Pair<String, String>> getTagTermPairList(String match, ArrayList<Pair<String, String>> tagTermPairs) {


        if (StringUtils.isBlank(match) || tagTermPairs == null || tagTermPairs.isEmpty()) {
            return null;
        }

        List<Pair<String, String>> pairList = new ArrayList<>();
        ArrayList<Integer> tagPos = new ArrayList<Integer>();
        String[] matchTagArray = match.split(" ");
        ArrayList<String> matchTagList = new ArrayList<String>();

        for (String matchStr : matchTagArray) {
            if (StringUtils.isNoneBlank(matchStr)) {
                matchTagList.add(matchStr);
            }
        }

        for (int i = 0; i < tagTermPairs.size(); i++) {
            if (tagTermPairs.get(i).getValue0().equals(matchTagList.get(0))) {
                tagPos.add(i);
            }
        }

        for (int i = 0; i < tagPos.size(); i++) {

            int start = tagPos.get(i);
            int matchLen = 0;
            Pair<String, String> stringPair;

            if ((start + matchTagList.size()) <= tagTermPairs.size()) {
                int loopCtr = 0;

                while (loopCtr < matchTagList.size()) {
                    if (matchTagList.get(loopCtr).equals(tagTermPairs.get(start).getValue0())) {
                        String tag = tagTermPairs.get(start).getValue0();
                        String term = tagTermPairs.get(start).getValue1();
                        stringPair = new Pair<>(tag, term);
                        pairList.add(stringPair);
                        matchLen++;
                    }
                    start++;
                    loopCtr++;
                }
            }

            if (matchLen == matchTagList.size()) {
                break;
            }
        }
        return pairList;
    }
}
