package org.phire.api.parser.util.algorithms;

import org.phire.api.parser.util.NLPTools.Document;

/**
 * An interface of classifier
 * 
 * @author Min Jiang
 *
 */
public interface Classifier {
	
	public String generatefeature(Document doc);
	
	public void train(String trainingFile);
	
	public void predict(String testFile, String outputFile);
	
}
