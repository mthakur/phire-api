package org.phire.api.parser.util.algorithms;

import org.phire.api.parser.util.Lexicon;
import org.phire.api.parser.util.NLPTools.Document;
import org.phire.api.parser.util.NLPTools.Global;
import org.phire.api.parser.util.NLPTools.Token;
import org.phire.api.parser.util.medex.DrugTag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SuffixArray implements Global {

    private static final Logger LOG = LoggerFactory.getLogger(SuffixArray.class);

    Document doc;
    Lexicon lex;
    char endChar;
    SuffixArrayMode mode;
    SuffixArrayCaseMode cmode;
    String otext;
    int N;
    private Map<Character, Integer> text2int = new HashMap<Character, Integer>();
    private Vector<Character> int2text = new Vector<Character>();
    int[] intText;
    int[] SA;
    Vector<SuffixArrayNode> ST;
    int ST_num;

    public SuffixArray(Document doc, Lexicon lex, char endc,
                       SuffixArrayMode mode, SuffixArrayCaseMode cmode) {
        this.doc = doc;
        this.lex = lex;
        this.endChar = endc;
        this.mode = mode;
        this.cmode = cmode;
        this.reBuild();
    }

    private void DC3_SA() {

        this.intText = new int[this.otext.length() + 3];
        int count = 1;

        for (int i = 0; i < this.otext.length(); i++) {
            char ch = otext.charAt(i);
            int code = 0;
            if (!this.text2int.containsKey(ch)) {
                this.text2int.put(ch, count);
                code = count;
                this.int2text.add(ch);
                count = count + 1;
            } else {
                code = this.text2int.get(ch);
            }
            this.intText[i] = code;
        }
        int n = this.otext.length();
        int K = n;
        this.SA = new int[n + 3];
        this.suffixArray(this.intText, SA, n, K);
    }

    public void reBuild() {

        this.otext = this.doc.norm_str().trim();

        if (this.otext.charAt(this.otext.length() - 1) == '.') {
            this.otext = this.otext.substring(0, this.otext.length() - 1);
        }

        this.otext = this.otext.toLowerCase();
        this.otext = this.otext.replace(this.endChar, ' ');
        this.otext = this.otext + this.endChar;
        this.N = this.otext.length();
        this.DC3_SA();
        // this.show_suffix_str();

        if (this.mode == SuffixArrayMode.ALL) {
            this.construct_tree();
        } else if (this.mode == SuffixArrayMode.WORD) {
            this.construct_tree_word();
        }
    }

    public void suffixArray(int[] s, int[] SA, int n, int K) {
        int n0 = (n + 2) / 3;// # number of positions MOD 3 = 0, eg, for string:
        // y a b b a d a b b a d o , n0 = 4 = {0, 3, 6,
        // 9}
        int n1 = (n + 1) / 3;// # positions MOD 3 = 1, n1 = 4 = {1, 4, 7, 10}
        int n2 = n / 3;// # positions MOD 3 = 2 n2 = 4 = {2, 5, 8, 11}
        int n02 = n0 + n2;//

        int[] s12 = new int[n02 + 3];// # s12 is all the suffix positions MOD 3
        // = 1 or 2
        int[] SA12 = new int[n02 + 3];// # SA12 is the ranked suffix array of
        // s12

        int[] s0 = new int[n0];// # all the suffix positions MOD 3 = 0
        int[] SA0 = new int[n0];// # the ranked suffix array of s0

        // # find all suffixs with position MOD 3 = 1 or 2 0 1 2 3 4 5 6
        // # Notice, when n % 3 = 1, there is a dummy pos MOD 3 = 1 added. eg, y
        // a b c 0 0 0, s1 = {1, 4}, where suffix[4] is dummy(000)

        int j = 0;
        for (int i = 0; i < n + (n0 - n1); i++) {
            if (i % 3 != 0) {
                s12[j] = i;
                j = j + 1;
            }
        }
        this.radix_pass(s12, SA12, s, 2, n02, K);

        this.radix_pass(SA12, s12, s, 1, n02, K);
        this.radix_pass(s12, SA12, s, 0, n02, K);

        // # find lexicographic names of triples
        int name = 0;
        int c0 = -1;
        int c1 = -1;
        int c2 = -1;
        for (int i = 0; i < n02; i++) {
            if (s[SA12[i]] != c0 || s[SA12[i] + 1] != c1
                    || s[SA12[i] + 2] != c2) {
                name = name + 1;
                c0 = s[SA12[i]];
                c1 = s[SA12[i] + 1];
                c2 = s[SA12[i] + 2];
            }
            if (SA12[i] % 3 == 1) {// # left half
                s12[SA12[i] / 3] = name;// # SA12[0] is the position ranked in
                // top 0, SA12[0] / 3 = the triple
                // number of this position
            } else {// # right half, s12 is now the ranking of tripples
                s12[SA12[i] / 3 + n0] = name;
            }
        }

        // # recurse if names are not yet unique
        if (name < n02) {
            this.suffixArray(s12, SA12, n02, name);
            // # store unique names in s12 using the suffix array
            for (int i = 0; i < n02; i++) {
                s12[SA12[i]] = i + 1;
            }
        } else {// # generate the suffix array of s12 directly,
            // SA12[tripple_rank]=tripple_no
            for (int i = 0; i < n02; i++) {
                SA12[s12[i] - 1] = i;
            }
        }

        // #sort positions MOD 3 == 0, by the first letter
        j = 0;
        for (int i = 0; i < n02; i++) {
            if (SA12[i] < n0) {
                s0[j] = 3 * SA12[i];
                j = j + 1;
            }
        }

        this.radix_pass(s0, SA0, s, 0, n0, K);

        // # merge sorted SA0 suffixes and sorted SA12 suffixes
        int p = 0;
        int t = n0 - n1;
        int k = 0;

        while (k < n) {
            int i = this.GetI(t, SA12, n0);
            j = SA0[p];
            boolean flag = false;

            if (SA12[t] < n0) {
                flag = this.leq2(s[i], s12[SA12[t] + n0], s[j], s12[j / 3]);
            } else {
                flag = this.leq3(s[i], s[i + 1], s12[SA12[t] - n0 + 1], s[j],
                        s[j + 1], s12[j / 3 + n0]);
            }

            if (flag) {
                SA[k] = i;
                k = k + 1;
                t = t + 1;
                if (t == n02) {
                    for (int pp = p; pp < n0; pp++) {
                        SA[k] = SA0[pp];
                        k = k + 1;
                    }
                }
            } else {
                SA[k] = j;
                k = k + 1;
                p = p + 1;
                if (p == n0) {
                    for (int tt = t; tt < n02; tt++) {
                        SA[k] = this.GetI(tt, SA12, n0);
                        k = k + 1;
                    }
                }
            }
        }
    }

    private boolean leq2(int a1, int a2, int b1, int b2) {
        return (a1 < b1 || (a1 == b1 && a2 <= b2));
    }

    private boolean leq3(int a1, int a2, int a3, int b1, int b2, int b3) {
        return (a1 < b1 || (a1 == b1 && this.leq2(a2, a3, b2, b3)));
    }

    private int GetI(int t, int[] SA12, int n0) {
        if (SA12[t] < n0) {
            return SA12[t] * 3 + 1;
        } else {
            return (SA12[t] - n0) * 3 + 2;
        }
    }

    private void radix_pass(int[] a, int[] b, int[] r, int index, int n, int K) {

        int[] c = new int[K + 1];
        for (int i = 0; i < n; i++) {
            c[r[a[i] + index]] = c[r[index + a[i]]] + 1;
        }

        int summ = 0;
        for (int i = 0; i < K + 1; i++) {
            int tmp = c[i];
            c[i] = summ;
            summ = summ + tmp;
        }

        for (int i = 0; i < n; i++) {
            b[c[r[index + a[i]]]] = a[i];
            c[r[index + a[i]]] = c[r[index + a[i]]] + 1;
        }

    }

    private void construct_tree() {
        this.ST = new Vector<SuffixArrayNode>();
        SuffixArrayNode node = new SuffixArrayNode();
        this.ST.add(node);
        this.ST_num = 1;

        for (int i = 0; i < this.N; i++) {
            this.insert_SF_tree(this.SA[i], 0, 0); // # 0 denote the root in
        }
    }

    private void construct_tree_word() {

        this.ST = new Vector<SuffixArrayNode>();
        SuffixArrayNode node = new SuffixArrayNode();
        this.ST.add(node);
        this.ST_num = 1;

        for (int i = 0; i < this.N; i++) {
            int pos = this.SA[i];
            if (this.otext.charAt(pos) != ' '
                    && this.otext.charAt(pos) != '\n'
                    && this.otext.charAt(pos) != this.endChar
                    && (pos == 0 || (this.otext.charAt(pos - 1) == ' ' || this.otext
                    .charAt(pos - 1) == '\n'))) {
                this.insert_SF_tree(this.SA[i], 0, 0); // # 0 denote the root in
            }
        }
    }

    private void insert(int poss, int lcs, int node) {
        int pos = poss + lcs;
        SuffixArrayNode SAnode = new SuffixArrayNode();
        SAnode.father = node;
        SAnode.start = pos;
        SAnode.end = this.N;
        SAnode.pos.add(poss);
        this.ST.add(SAnode);// add leaf node
        this.ST.get(node).son.add(this.ST_num);
        if (node != 0) {
            this.ST.get(node).pos.add(poss);
        }
        this.ST_num = this.ST_num + 1;
    }

    private int get_lcs(int pos, int start, int end) {
        if (start == -1) {
            return 0;
        }
        int poss = pos;
        int tpos = start;
        int lcd = 0;
        while (poss < this.N && tpos < end) {
            if (this.otext.charAt(poss) == this.otext.charAt(tpos)) {
                lcd = lcd + 1;
                poss = poss + 1;
                tpos = tpos + 1;
            } else {
                break;
            }
        }
        return lcd;
    }

    private void insert_inter(int poss, int lcs, int node) {
        int pos = poss + lcs;
        SuffixArrayNode SAnode = new SuffixArrayNode();
        SAnode.father = node;
        SAnode.start = pos;
        SAnode.end = this.N;
        SAnode.pos.add(poss);
        this.ST.add(SAnode);// # leaf node
        this.ST.get(node).son.add(this.ST_num);
        this.ST_num = this.ST_num + 1;
    }

    private void insert_SF_tree(int poss, int d, int node) {

        int pos = poss + d;
        if (this.ST.get(node).son.size() == 0) {
            this.insert(poss, d, node);
        } else {
            int rnode = this.ST.get(node).son
                    .get(this.ST.get(node).son.size() - 1);// #
            int start = this.ST.get(rnode).start;
            int end = this.ST.get(rnode).end;
            int lcs = this.get_lcs(pos, start, end);// # compare with right most
            // child

            int nstart = start + lcs;
            if (nstart == start) {// # no match, insert into node as right most
                this.insert_inter(poss, lcs + d, node);
            } else if (nstart == end) {// # the current node is matched over,
                if (this.ST.get(rnode).son.size() > 0) {
                    this.ST.get(rnode).pos.add(poss);
                    this.insert_SF_tree(poss, lcs + d, rnode);
                } else {
                    this.insert_inter(poss, lcs + d, rnode);
                }
            } else if (nstart > start && nstart < end) {// # matched in the
                SuffixArrayNode SAnode = new SuffixArrayNode();
                SAnode.father = rnode;
                SAnode.start = nstart;
                SAnode.end = end;
                SAnode.son.addAll(this.ST.get(rnode).son);
                SAnode.pos.addAll(this.ST.get(rnode).pos);
                for (int i = 0; i < this.ST.get(rnode).son.size(); i++) {
                    int child = this.ST.get(rnode).son.get(i);
                    this.ST.get(child).father = this.ST_num;
                }
                this.ST.get(rnode).end = nstart;
                this.ST.get(rnode).son.clear();// # clear the children
                this.ST.get(rnode).son.add(this.ST_num);
                this.ST.get(rnode).pos.add(poss);
                this.ST.add(SAnode);
                this.ST_num = this.ST_num + 1;
                this.insert_inter(poss, lcs + d, rnode);
            }
        }
    }

    public Vector<Integer> SAsearch(String query) {

        Vector<Integer> result = new Vector<Integer>();
        if (this.otext.length() == 0 || this.ST.get(0).son.size() == 0) {
            return result;
        }

        int qpos = 0;
        int llen = query.length();
        int start_node_id = 0;
        int match_pos = 0;
        while (qpos < llen) {
            if (!this.text2int.containsKey(query.charAt(qpos))) {// the query
                start_node_id = -1;
                break;
            }
            int node = start_node_id;
            int pos = match_pos;
            int start = this.ST.get(node).start;
            int end = this.ST.get(node).end;

            if (node == 0 || start + pos == end) {
                if (this.ST.get(node).son.size() == 0) {
                    start_node_id = -1;
                    break;
                }
                for (int i = 0; i < this.ST.get(node).son.size(); i++) {
                    int child = this.ST.get(node).son.get(i);
                    int tmp = this.ST.get(child).start;

                    if (this.text2int.get(this.otext.charAt(tmp)) > this.text2int
                            .get(query.charAt(qpos))) {
                        start_node_id = -1;
                        break;
                    }

                    if (this.otext.charAt(tmp) == query.charAt(qpos)) {
                        start_node_id = child;
                        match_pos = 1;
                        break;
                    } else {// # no match
                        start_node_id = -1;
                    }
                }
            } else {
                if (this.otext.charAt(start + pos) == query.charAt(qpos)) {
                    match_pos = pos + 1;
                } else {
                    start_node_id = -1;
                }
            }
            if (start_node_id == -1) {
                break;
            }
            qpos = qpos + 1;
        }

        if (start_node_id >= 0) {
            if (this.mode == SuffixArrayMode.ALL) {// # all suffix mode
                for (int i = 0; i < this.ST.get(start_node_id).pos.size(); i++) {
                    int val = this.ST.get(start_node_id).pos.get(i);
                    result.add(val);
                }
            } else if (this.mode == SuffixArrayMode.WORD) { // #word
                for (int i = 0; i < this.ST.get(start_node_id).pos.size(); i++) {
                    int val = this.ST.get(start_node_id).pos.get(i);
                    char w = this.otext.charAt(val + llen);
                    int tmpos = val + llen + 1;
                    if (w == ' '
                            || w == '\n'
                            || w == this.endChar
                            || (w == '.' && (tmpos == this.otext.length() || (tmpos < this.otext
                            .length() && (this.otext.charAt(tmpos) == ' ' || this.otext
                            .charAt(tmpos) == '\n'))))) {
                        result.add(val);
                    }
                }
            }
        }
        return result;
    }

    public List<DrugTag> getSearchResult() {

        List<SuffixArrayResult> result = search();
        List<DrugTag> drugTags = new ArrayList<DrugTag>();

        // building drug tags
        for (int i = 0; i < result.size(); i++) {
            String str = doc.getStrByToken(result.get(i).startToken, result.get(i).endToken);
            DrugTag dt = new DrugTag(result.get(i).startPos, result.get(i).endPos, TextSectionType.DRUG, str,
                    result.get(i).startToken, result.get(i).endToken, result.get(i).semanticType);
            drugTags.add(dt);
        }
        return drugTags;
    }


    public List<SuffixArrayResult> search() {

        List<SuffixArrayResult> result = new ArrayList<>();
        int len = this.lex.lexList().size();

        int pos = -1;
        int pos2 = -1;
        String lowerKey = "";
        String semanticType = "";
        Map<Integer, Integer> startMap;
        Map<Integer, Integer> endMap;
        Vector<Token> tokenVct;

        startMap = this.doc.startMap();
        endMap = this.doc.endMap();
        tokenVct = this.doc.token_vct();

        for (int i = 0; i < len; i++) {

            lowerKey = this.lex.lexList().get(i).toLowerCase();
            semanticType = this.lex.semList().get(i);
            Vector<Integer> tresult = this.SAsearch(lowerKey);

            for (int j = 0; j < tresult.size(); j++) {
                pos = tresult.get(j);
                pos2 = pos + lowerKey.length();
                SuffixArrayResult re = new SuffixArrayResult();
                int tmp = startMap.get(pos);

                re.startToken = tmp;
                re.startPos = tokenVct.get(tmp).getAbsStartPos();
                tmp = endMap.get(pos2);
                re.endToken = tmp;
                re.endPos = tokenVct.get(tmp).getAbsEndPos();
                re.semanticType = semanticType;
                result.add(re);
            }
        }
        return result;
    }

    public Vector<SuffixArrayResult> simple_search() {

        Vector<SuffixArrayResult> result = new Vector<SuffixArrayResult>();
        String lower_norm_str = doc.boundary_norm_str().toLowerCase();
        int text_len = lower_norm_str.length();
        int tokenLen = this.doc.boundary_token_vct().size();
        int len = this.lex.lexList().size();

        int pos = -1;
        int pos2 = -1;

        for (int i = 0; i < len; i++) {
            String lower_key = this.lex.lexList().get(i).toLowerCase();
            int startp = 0;
            pos = lower_norm_str.indexOf(lower_key, startp);

            while (pos >= 0) {
                pos2 = pos + lower_key.length();
                if ((pos == 0
                        || this.doc.boundary_norm_str().charAt(pos - 1) == ' ' || this.doc
                        .boundary_norm_str().charAt(pos - 1) == '\n')
                        && (pos2 == lower_norm_str.length()
                        || this.doc.boundary_norm_str().charAt(pos2) == ' '
                        || this.doc.boundary_norm_str().charAt(pos2) == '\n' || (this.doc
                        .boundary_norm_str().charAt(pos2) == '.' && (pos2 + 1 == lower_norm_str
                        .length()
                        || lower_norm_str.charAt(pos2 + 1) == ' ' || lower_norm_str
                        .charAt(pos2 + 1) == '\n')))) {

                    SuffixArrayResult re = new SuffixArrayResult();
                    int tmp = (Integer) this.doc.boundaryStartMap().get(pos);
                    re.startToken = tmp;
                    re.startPos = this.doc.boundary_token_vct().get(tmp).getAbsStartPos();
                    tmp = (Integer) this.doc.boundaryEndMap().get(pos2);
                    re.endToken = tmp;
                    re.endPos = this.doc.boundary_token_vct().get(tmp).getAbsEndPos();
                    re.semanticType = this.lex.semList().get(i);
                    result.add(re);
                }
                startp = pos + 1;
                // find next search point
                while (startp < text_len
                        && lower_norm_str.charAt(startp) != ' '
                        && lower_norm_str.charAt(startp) != '\n') {
                    startp = startp + 1;
                }
                pos = lower_norm_str.indexOf(lower_key, startp);
            }
        }
        return result;
    }

    private String getNodeStr(int node) {
        String result = "";
        int start = this.ST.get(node).start;
        int end = this.ST.get(node).end;
        if (start >= 0 && end >= 0) {
            result = this.otext.substring(start, end);
        } else {
            result = "";
        }
        return result;
    }

    public void traverse() {
        Stack<String> path = new Stack<String>();
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(0);
        Set<Integer> seen = new HashSet<Integer>();

        while (stack.size() > 0) {
            int node = stack.lastElement();

            if (seen.contains(node)) {
                stack.pop();
                path.pop();
                continue;
            }
            seen.add(node);
            String strr = this.getNodeStr(node);
            path.push(strr);
            int llen = this.ST.get(node).son.size();

            if (llen == 0) {
                this.print_path(path);
                stack.pop();
                path.pop();
                continue;
            }
            llen = this.ST.get(node).son.size();
            int i = llen - 1;

            while (i >= 0) {
                stack.add(this.ST.get(node).son.get(i));
                i = i - 1;
            }
        }
    }

    private void print_path(Stack<String> path) {
        String pp = "";
        for (int i = 0; i < path.size(); i++) {
            pp = pp + path.elementAt(i);
        }
    }
}
