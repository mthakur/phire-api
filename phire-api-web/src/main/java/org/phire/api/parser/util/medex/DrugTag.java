package org.phire.api.parser.util.medex;

import org.phire.api.parser.util.NLPTools.Global;
import org.phire.api.parser.util.NLPTools.TextSection;

public class DrugTag extends TextSection implements Global {


    private int startToken = -1;
    private int endToken = -1;
    private String semanticTag;

    public DrugTag(int sPos, int ePos, TextSectionType type, String str, int startToken, int endToken, String stype) {
        super(sPos, ePos, type, str);
        this.startToken = startToken;
        this.endToken = endToken;
        this.semanticTag = stype;
    }

    public DrugTag(DrugTag dt) {
        super(dt.getAbsStartPos(), dt.getAbsEndPos(), dt.getType(), dt.getText());
        this.startToken = dt.startToken;
        this.endToken = dt.endToken;
        this.semanticTag = dt.semanticTag;
    }

    public int startToken() {
        return this.startToken;
    }

    public int endToken() {
        return this.endToken;
    }

    public String semanticTag() {
        return this.semanticTag;
    }

    @Override
    public String toString() {
        return "DrugTag{" +
                ", semanticTag='" + semanticTag + '\'' +
                ", token='" + getText() + '\'' +
                '}';
    }
}
