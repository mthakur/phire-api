package org.phire.api.parser.util.medex;

import org.apache.commons.lang3.StringUtils;
import org.phire.api.parser.dto.DrugInfoDto;
import org.phire.api.parser.util.algorithms.VectorSpaceModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Encoder {

    private static final Logger LOG = LoggerFactory.getLogger(Encoder.class);
    private static final String NULL_STRING = "NULL";

    public HashMap<String, ArrayList<String>> rxCodeMap;
    public HashMap<String, ArrayList<DrugInfoDto>> drugInfoDtoList;
    public HashMap<String, String> normalizationMap;
    public Util utility;

    public Encoder(HashMap<String, ArrayList<String>> rx_code_map, HashMap<String, ArrayList<DrugInfoDto>> drugInfoDtoList,
                   HashMap<String, String> normalizationMap, HashMap<String, String> genericMap) {

        this.rxCodeMap = rx_code_map;
        this.drugInfoDtoList = drugInfoDtoList;
        this.normalizationMap = normalizationMap;
        this.utility = new Util(normalizationMap);
    }

    /**
     * Search UMLS code and RXNORM code for the given drug signature
     *
     * @param drugListMap list of drug signature e.g. "{"Warfarin FFF DRUG", "1mg",
     *                    "DOSE"}
     * @return array of string contains UMLS code and RXNORM code
     * e.g.{"C1123825", "328830"}
     * @see Encoder
     */
    public DrugInfoDto encode(Map<String, String> drugListMap) {

        String INGRE = "";
        String STRENGTH = "";
        String FORM = "";
        String ROUTE = "";
        String DOSE = "";
        String BRAND = "";
        Iterable<String> siglist = drugListMap.keySet();

        for (String tag : siglist) {

            if (StringUtils.isBlank(tag)) {
                continue;
            }
            String token = drugListMap.get(tag);

            if (tag.equals(Util.DRUG_STRING)) {
                INGRE = token.toLowerCase();
            } else if (tag.equals(Util.ROUTE_STRING)) {
                ROUTE = token.toLowerCase();
            } else if (tag.equals(Util.FORM_STRING)) {
                FORM = token.toLowerCase();
            } else if (tag.equals(Util.STRENGTH_STRING)) {
                STRENGTH = token.toLowerCase();
            } else if (tag.equals(Util.AMOUNT_STRING)) {
                DOSE = token.toLowerCase();
            } else if (tag.equals(Util.BRAND_STRING)) {
                BRAND = token.toLowerCase();
            }
            if (STRENGTH.trim().equals(StringUtils.EMPTY)) {
                STRENGTH = DOSE;
            }
        }

        // normalize drug form
        INGRE = utility.normalizeDrugNameForMatch(INGRE);
        FORM = utility.normalizeForm(FORM.trim().toLowerCase());
        ROUTE = utility.normalizeForm(ROUTE.trim().toLowerCase());

        // normalize the drug name
        // INGRE = utility.normalizeDrugName(INGRE);
        // normalize the drug strength
        STRENGTH = utility.normalizeDose(STRENGTH);

        // make inference on form based on strength
        if (STRENGTH.indexOf("tablet") >= 0) {
            if (FORM.equals("")) {
                FORM = "tablet";
            }
        }

        // if drug name contain form
        for (String str : Util.DRUG_FORMS) {
            if (INGRE.endsWith(" " + str) && FORM.indexOf(str) < 0) {
                FORM = str + " " + FORM;
                INGRE = INGRE.substring(0, INGRE.length() - str.length() - 1);
            }
        }

        INGRE = INGRE.trim();
        STRENGTH = STRENGTH.trim();
        FORM = FORM.trim();
        ROUTE = ROUTE.trim();

        LOG.debug("INGRE/STRENGTH/FORM/ROUTE/BRAND : " + INGRE + "/" + STRENGTH + "/" + FORM + "/" + ROUTE + "/" + BRAND);

        // check if generic name is in the form like "generic(brand)" or
        // "brand(generic)"
        int brandStartOffset = INGRE.indexOf('(');
        int brandEndOffset = INGRE.indexOf(')');

        if (StringUtils.isBlank(BRAND) && brandStartOffset >= 0 && brandEndOffset >= 0 && brandEndOffset > brandStartOffset) {
            String generic = INGRE.substring(0, brandStartOffset).trim();
            BRAND = INGRE.substring(brandStartOffset + 1, brandEndOffset).trim();
            return getCodeNew(generic, STRENGTH, FORM, ROUTE, BRAND);
        } else {
            return getCodeNew(INGRE, STRENGTH, FORM, ROUTE, BRAND);
        }
    }

    private DrugInfoDto getCodeNew(String drugName, String strength, String format, String route, String brand) {

        LOG.debug("getCodeNew : " + "drugName=" + drugName + ",strength=" + strength + ",format=" + format + ",route=" + route + ",brand=" + brand);
        DrugInfoDto drugInfoDto = null;


        if (normalizationMap.containsKey(drugName)) {
            drugName = normalizationMap.get(drugName);
        }
        String[] codes = {"", "", "", ""};
        ArrayList<DrugInfoDto> candidateObjects = new ArrayList<DrugInfoDto>();

        try {
            ArrayList<String> candidates = new ArrayList<String>();
            drugName = Util.transformDrugName(drugName);

            if (drugInfoDtoList.containsKey(drugName)) {
                LOG.debug("drugInfo by name :" + candidateObjects.size());
                candidateObjects = drugInfoDtoList.get(drugName);
            } else if (drugInfoDtoList.containsKey(brand)) {
                LOG.debug("drugInfo by brand : " + candidateObjects.size());
                candidateObjects = drugInfoDtoList.get(brand);
            } else {
                LOG.warn("druInfo not found : /" + drugName + " / " + brand);
            }

            for (DrugInfoDto drugInfo : candidateObjects) {
                candidates.add(Util.transformPercentage(getMatchString(drugInfo)));
            }

            String items[] = drugName.split(StringUtils.SPACE);

            if (items.length >= 2) {
                if (items.length == 2) {
                    String nDrugName = drugName.replaceAll(StringUtils.SPACE, "-");
                    addDrugInfo(nDrugName, candidateObjects, candidates);
                }
                for (String word : items) {
                    addDrugInfo(word, candidateObjects, candidates);
                }
            }

            if (candidates.size() > 0) {
                int topIndex = compareSimilarity(candidates, drugName, strength, format, route, brand);
                //LOG.debug("candidateObjects.size() / topIndex : " + candidateObjects.size() + " / " + topIndex);
                if (candidateObjects.size() > topIndex) {
                    drugInfoDto = candidateObjects.get(topIndex);
                }
            }
        } catch (Exception e) {
            LOG.error("Error while getting code Codes : " + Arrays.toString(codes));
            e.printStackTrace();
        }
        LOG.debug("Most matched obj : " + drugInfoDto);

        if (StringUtils.isBlank(brand) && drugInfoDto != null && StringUtils.isNotBlank(drugInfoDto.getDin())) {
            drugInfoDto.setDin(StringUtils.EMPTY);
            drugInfoDto.setBrandName(StringUtils.EMPTY);
        }
        return drugInfoDto;
    }


    private void addDrugInfo(String nDrugName, ArrayList<DrugInfoDto> candidateObjects, ArrayList<String> candidates) {

        if (drugInfoDtoList.containsKey(nDrugName)) {
            ArrayList<DrugInfoDto> drugInfoList = drugInfoDtoList.get(nDrugName);

            for (DrugInfoDto drugInfo : drugInfoList) {
                candidateObjects.add(drugInfo);
                candidates.add(Util.transformPercentage(getMatchString(drugInfo)));
            }
        }
    }

    public Encoder() {
        super();
    }

    private String getMatchString(DrugInfoDto drugInfoDto) {

        if (drugInfoDto == null) {
            return null;
        }

        String value = StringUtils.EMPTY;
        String brandName = drugInfoDto.getBrandName() == null ? NULL_STRING : drugInfoDto.getBrandName();
        String genericName = drugInfoDto.getGenericName() == null ? NULL_STRING : drugInfoDto.getGenericName();
        String format = drugInfoDto.getFormat() == null ? NULL_STRING : drugInfoDto.getFormat();
        String strength = drugInfoDto.getStrength() == null ? NULL_STRING : drugInfoDto.getStrength();
        String sent = drugInfoDto.getSent() == null ? NULL_STRING : drugInfoDto.getSent();

        if (!genericName.equals("NULL")) {
            value = genericName + "\t" + strength + "\t" + format + "\t" + brandName + "\t" + sent;
        }

        if (!brandName.equals("NULL")) {
            value = genericName + "\t" + strength + "\t" + format + "\t" + brandName + "\t" + sent;
            if (genericName.equals("NULL"))
                value = brandName + "\t" + strength + "\t" + format + "\t" + "NULL" + "\t" + sent;
        }
        return value;

    }

    private int compareSimilarity(ArrayList<String> candidateList, String drugName, String strength, String form,
                                  String route, String brand) {

        int maxIndex = 0;

        if (candidateList == null || candidateList.size() == 0 || (drugName == null && brand == null)) {
            LOG.debug("Invalid method input : " + candidateList + ", " + drugName + ", " + strength + ", " + form + ", " + route + ", " + brand);
            return maxIndex;
        }
        ArrayList<String> sentList = new ArrayList<String>();

        for (String sent : candidateList) {
            //LOG.debug("\t " + sent);
            String[] items = sent.split("\t");
            String sentText = items[4].trim();
            String item = utility.stem(sentText.replace("[", "").replace("]", "").
                    replace("(", "").replace(")", "").replace(" %", "%").split(" "));
            sentList.add(item);
        }

        // vector space model for whole sent
        VectorSpaceModel vsm = new VectorSpaceModel();
        try {
            String[] doseUnit = utility.getdoseNumberUnit(strength);
            String queryString = drugName + " " + doseUnit[0] + "" + doseUnit[1].replace(" ", "") + " " + form + " " + route + " " + brand;
            //LOG.debug("queryString : " + queryString);
            maxIndex = vsm.getRankingTopN(utility.stem(queryString.split(" ")), sentList, 10).get(0);
            //LOG.debug("most matched string: " + maxIndex + "/" + sentList.get(maxIndex));
        } catch (Exception e) {
            LOG.error("Exception caught while comparing similarity : " + e.getMessage());
            e.printStackTrace();
        }
        //LOG.debug("maxIndex : " + maxIndex);
        return maxIndex;
    }
}
