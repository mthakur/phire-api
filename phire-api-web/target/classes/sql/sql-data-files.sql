-- sql for generating generic name and generic code
SELECT
    DISTINCT * FROM (
    SELECT lower(GENERIC_NAME_EN) AS GENERIC_NAME, GENCODE from FGENPLUS
    UNION
    SELECT lower(GENERIC_NAME_EN_SIMPLE) as GENERIC_NAME , GENCODE from FGENPLUS
    UNION
    SELECT lower(GENERIC_NAME_FR) as GENERIC_NAME, GENCODE from FGENPLUS
    UNION
    SELECT lower(GENERIC_NAME_FR_SIMPLE) as GENERIC_NAME, GENCODE from FGENPLUS
);

-- sql for generating data file for rx norm
